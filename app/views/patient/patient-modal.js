const observableModule = require("tns-core-modules/data/observable");
const appSettings = require("tns-core-modules/application-settings");

let page;
let closeCallback;
let curDate;
let tabindex;

exports.onShownModally = (args) => {
    page = args.object.page;
    closeCallback = args.closeCallback;
    const patientBody = args.context;
    curDate = patientBody.date;
    tabindex = patientBody.tab;
    // closeCallback = args.closeCallback;
    // const page = args.object;
    // console.log(clinicBody);
    // let clinicdata = [];
    // let doctorPerid = clinicBody.cliniclist[0].c_doct;
    // let date_d = clinicBody.cliniclist[0].date_d;

    page.bindingContext = observableModule.fromObject({
        patientname: patientBody.name,
        patienthn: patientBody.hn.split("HN")[1],
        queuetime: patientBody.time,
    });
};

exports.onClicked = (args) => {
    let service = args.object.service;
    let hnText = page.getViewById("patienthn");
    let hn = hnText.text;

    let result = {
        statusCode: 200,
        service: service,
        action: "",
        date: curDate,
        hn: hn,
        code_b: appSettings.getString("cliniccode" + tabindex),
        c_doct: appSettings.getString("doctor" + tabindex),
    };

    closeCallback(result);
};
