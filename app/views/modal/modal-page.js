const observableModule = require("tns-core-modules/data/observable");
const MainPageModel = require("../main/main-page-model");
const appSettings = require("tns-core-modules/application-settings");

let apiservice = MainPageModel.APIService();
let closeCallback;
let tab;

exports.onShownModally = (args) => {
    const clinicBody = args.context;
    tab = clinicBody.tab;
    closeCallback = args.closeCallback;
    const page = args.object;
    let clinicdata = [];
    let doctorPerid = clinicBody.cliniclist[0].c_doct;
    let doctorName = clinicBody.doctorname;
    let date_d = clinicBody.cliniclist[0].date_d;
    clinicBody.cliniclist.forEach((element) => {
        clinicdata.push({
            cliniccode: element.clinic_code,
            clinicname: element.clinic_name,
        });
    });
    page.bindingContext = observableModule.fromObject({
        doctorperid: doctorPerid,
        doctorname: doctorName,
        currentdate: date_d,
        clinicdata: clinicdata,
    });

    // const bindingContext = page.page.bindingContext;
    // let id = bindingContext.get("id");
    // let name = bindingContext.get("name");
    // closeCallback(id, name);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    let doctorPerid = page.getViewById("doctorperid").text;
    let currentdate = page.getViewById("currentdate").text;
    let listView = page.getViewById("listView");
    var selectedItems = listView.getSelectedItems();
    let clinicCode = selectedItems[0].cliniccode;
    let clinicName = selectedItems[0].clinicname;
    appSettings.setString("cliniccode"+tab, clinicCode);
    appSettings.setString("clinicname"+tab, clinicName);
    // console.log("Doctor Perid: " + doctorPerid.text);
    // console.log("Date: " + currentdate.text);
    // console.log(selectedItems[0]);
    apiservice
        .getDoctorQueue(currentdate, clinicCode, doctorPerid)
        .catch((err) => {
            console.log(err);
            reject(err);
        })
        .then((clinicqueueresponse) => {
            let clinicqueuelist = clinicqueueresponse.data;
            let clinicData;
            if (clinicqueuelist.length != 0) {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: clinicqueuelist,
                };
            } else {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: [],
                };
            }
            closeCallback(clinicData);
        });
};
