const observableModule = require("tns-core-modules/data/observable");
const appSettings = require("tns-core-modules/application-settings");

let closeCallback;
let tab;
exports.onShownModally = (args) => {
   closeCallback = args.closeCallback;
};

exports.onAddNewDoctor = (args) => {
    let page = args.object.page;
    let perid = page.getViewById("doctorperid");
    closeCallback(perid.text);
};
