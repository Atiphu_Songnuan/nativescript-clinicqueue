const Observable = require("tns-core-modules/data/observable").Observable;
const http = require("tns-core-modules/http");

function APIService() {
    let viewModel = new Observable();
    let result = new Array();
    // console.log(global.conf.api.doctorclinic);
    viewModel.getDoctorInfo = (perid) => {
        return http
            .request({
                url: global.conf.api.isdoctor,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getClinic = (perid, curdate) => {
        return http
            .request({
                url: global.conf.api.doctorclinic,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                    date_d: curdate,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getDoctorQueue = (curdate, cliniccode, doctorperid) => {
        return http
            .request({
                url: global.conf.api.doctorqueue,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    date_d: curdate,
                    code_b: cliniccode,
                    c_doct: doctorperid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getPatientInfo = (hn) => {
        return http
            .request({
                url: global.conf.api.patientinfo,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    hn: hn,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.checkPatientTime = (patientinfo) => {
        let currentdate = patientinfo.date;
        let clinicCode = patientinfo.code_b;
        let doctorPerid = patientinfo.c_doct;
        let hn = patientinfo.hn;
        return http
            .request({
                url: global.conf.api.patienttime,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    date_d: currentdate,
                    code_b: clinicCode,
                    c_doct: doctorPerid,
                    hn: hn,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    result = {
                        statusCode: 200,
                        action: res.action,
                    };
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        action: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.updatePatientTime = (patientinfo) => {
        let updateTimeURL = "";
        switch (patientinfo.action) {
            case "update":
                console.log("updated");
                updateTimeURL = global.conf.api.updatepatienttime;
                break;
            case "insert":
                console.log("insert");
                updateTimeURL = global.conf.api.insertpatienttime;
                break;
            default:
                break;
        }

        let hn = patientinfo.hn;
        let currentdate = patientinfo.date;
        let clinicCode = patientinfo.code_b;
        let doctorPerid = patientinfo.c_doct;

        return http
            .request({
                url: updateTimeURL,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    hn: hn,
                    date_d: currentdate,
                    code_b: clinicCode,
                    c_doct: doctorPerid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    result = {
                        statusCode: 200,
                        status: res.data,
                    };
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };
    return viewModel;
}

module.exports = {
    APIService: APIService,
};
