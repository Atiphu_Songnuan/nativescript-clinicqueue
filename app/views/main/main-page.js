const view = require("tns-core-modules/ui/core/view");
const MainPageModel = require("./main-page-model");
const fromObject = require("tns-core-modules/data/observable").fromObject;
const appSettings = require("tns-core-modules/application-settings");
const platform = require("tns-core-modules/platform");
const dialogs = require("tns-core-modules/ui/dialogs");
const nstoasts = require("nativescript-toasts");

const modalViewModule = "/views/modal/modal-page.js";
const doctorViewModule = "/views/doctor/doctor-modal.js";
const patientViewModule = "/views/patient/patient-modal.js";

let apiservice = MainPageModel.APIService();
let page;
let modalView;

// Doctor1
let data1 = [];
let btnDoctor1;
let doctor1;

// Doctor2
let data2 = [];
let btnDoctor2;
let doctor2;

// Doctor3
let data3 = [];
let btnDoctor3;
let doctor3;

// Doctor4
let data4 = [];
let btnDoctor4;
let doctor4;

// Doctor5
let data5 = [];
let btnDoctor5;
let doctor5;

let curdate = getDateString(new Date());
// let curDateStr = curdate.year + "-" + curdate.month + "-" + curdate.day;
let curDateStr = "2020-07-05";

exports.onPageLoaded = (args) => {
    page = args.object.page;
    modalView = args.object;
    // newDoctorBtn = page.getViewById("newdoctorbtn");
    // if (platform.isIOS) {
    //     newDoctorBtn.style = "font-size: 18em;";
    // } else {
    //     newDoctorBtn.style = "font-size: 14em;";
    // }
};

exports.addDoctor1 = () => {
    btnDoctor1 = page.getViewById("btnDoctor1");
    doctor1 = page.getViewById("doctor1");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "1", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated1 = () => {
    let doctorperid = appSettings.getString("doctor1");
    let listView1 = page.getViewById("listview1");
    getDoctorInfo("reload", "1", doctorperid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView1.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor2 = () => {
    btnDoctor2 = page.getViewById("btnDoctor2");
    doctor2 = page.getViewById("doctor2");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "2", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated2 = () => {
    let perid = appSettings.getString("doctor2");
    let listView2 = page.getViewById("listview2");
    getDoctorInfo("reload", "2", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView2.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor3 = () => {
    btnDoctor3 = page.getViewById("btnDoctor3");
    doctor3 = page.getViewById("doctor3");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "3", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated3 = () => {
    let perid = appSettings.getString("doctor3");
    let listView3 = page.getViewById("listview3");
    getDoctorInfo("reload", "3", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView3.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor4 = () => {
    btnDoctor4 = page.getViewById("btnDoctor4");
    doctor4 = page.getViewById("doctor4");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "4", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated4 = () => {
    let perid = appSettings.getString("doctor4");
    let listView4 = page.getViewById("listview4");
    getDoctorInfo("reload", "4", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView4.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor5 = () => {
    btnDoctor5 = page.getViewById("btnDoctor5");
    doctor5 = page.getViewById("doctor5");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "5", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated5 = () => {
    let perid = appSettings.getString("doctor5");
    let listView5 = page.getViewById("listview5");
    getDoctorInfo("reload", "5", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView5.notifyPullToRefreshFinished();
    }, 500);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    let tabindex = args.object.tabindex;
    let listView = page.getViewById("listview" + tabindex);
    let selectedItems = listView.getSelectedItems();

    let patientdata = selectedItems[0];
    // console.log(patientdata);
    // let doctorperid = appSettings.getString("doctor" + tabindex);
    // let clinicCode = appSettings.getString("cliniccode" + tabindex);
    let pHN = patientdata["hn" + tabindex];
    let pName = patientdata["name" + tabindex];
    let qTime = patientdata["qtime" + tabindex];

    const option = {
        context: {
            tab: tabindex,
            date: "2020-07-05", //อย่าลืมแก้กลับเป็นวันตรวจปัจจุบัน Curdate
            hn: pHN,
            name: pName,
            time: qTime,
        },
        closeCallback: (patientinfo) => {
            if (typeof patientinfo == "object") {
                let doctorperid = patientinfo.c_doct;
                let date = patientinfo.date;
                apiservice
                    .checkPatientTime(patientinfo)
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
                    .then((response) => {
                        patientinfo["action"] = response.action;
                        console.log(patientinfo);
                        apiservice
                            .updatePatientTime(patientinfo)
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        getDoctorInfo("reload", tabindex, doctorperid, date);
                    });
            } else {
                console.log("close patient modal");
                listView.deselectItemAt(args.index);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(patientViewModule, option);
};

exports.onDoctorClosed = (args) => {
    const page = args.object.page;
    let tabindex = args.object.tabindex;
    let perid = appSettings.getString("doctor" + tabindex);
    dialogs
        .confirm({
            title: "นำแพทย์ออกจากคลินิก",
            message: "",
            okButtonText: "ตกลง",
            cancelButtonText: "ยกเลิก",
            cancelable: false,
        })
        .then((r) => {
            if (r) {
                clearData(tabindex);
                setRadListView("hidelistview", tabindex);
                let options = {
                    text: "นำแพทย์ออกจากคลินิกสำเร็จ",
                    duration: nstoasts.DURATION.SHORT,
                    position: nstoasts.POSITION.BOTTOM, //optional
                };
                nstoasts.show(options);
            }
        });
};

// Step 1 ตรวจสอบข้อมูลแพทย์
function getDoctorInfo(action, tab, perid, curdate) {
    let doctorInfoProms = new Promise((resolve, reject) => {
        apiservice
            .getDoctorInfo(perid)
            .catch((err) => {
                console.log(err);
                reject(err);
            })
            .then((inforesponse) => {
                let info = inforesponse.data;
                if (info.length != 0) {
                    appSettings.remove("doctor" + tab);
                    appSettings.setString("doctor" + tab, perid);
                    // let doctorname = doctorinfo[0].doctorname;
                    // let doctorsex = doctorinfo[0].sex;
                    let doctorinfo = {
                        doctorname: info[0].doctorname,
                        sex: info[0].sex,
                    };
                    resolve(doctorinfo);
                } else {
                    console.log("ไม่พบข้อมูลแพทย์");
                }
            });
    });
    doctorInfoProms.then((doctorinfo) => {
        getDoctorClinicInfo(action, tab, perid, curdate, doctorinfo);
    });
}

// Step 2 ตรวจสอบข้อมูลคลินิกที่เลือก
function getDoctorClinicInfo(action, tab, perid, curdate, doctorinfo) {
    let clinicInfoProms = new Promise((resolve, reject) => {
        apiservice
            .getClinic(perid, curdate)
            .catch((err) => {
                console.log(err);
                reject(err);
                return Promise.reject();
            })
            .then((response) => {
                let clinicList = response.data;
                if (clinicList.length != 0) {
                    resolve(clinicList);
                } else {
                    resolve([]);
                    console.log("ไม่มีคลินิก");
                }
            });
    });
    clinicInfoProms.then((clinicinfo) => {
        getDoctorQueue(action, tab, clinicinfo, doctorinfo, curdate);
    });
}

// Step 3 ดึงข้อมูลคิวตรวจของแพทย์ในคลินิกนั้นๆ
function getDoctorQueue(action, tab, clinicinfo, doctorinfo, curdate) {
    if (clinicinfo.length != 0) {
        // const mainView = args.object;
        switch (action) {
            case "new":
                const option = {
                    context: {
                        tab: tab,
                        doctorname: doctorinfo.doctorname,
                        cliniclist: clinicinfo,
                    },
                    closeCallback: (doctorqueueinfo) => {
                        if (typeof doctorqueueinfo == "object") {
                            // appSettings.setString("");
                            let queuelist = doctorqueueinfo.queueList;
                            setQueueList(
                                tab,
                                doctorinfo,
                                doctorqueueinfo,
                                queuelist
                            );
                        } else {
                            console.log("close modal");
                        }
                    },
                    fullscreen: false,
                    animated: true,
                };
                modalView.showModal(modalViewModule, option);
                break;
            case "reload":
                let doctorPerid = appSettings.getString("doctor" + tab);
                let clinicCode = appSettings.getString("cliniccode" + tab);
                let clinicName = appSettings.getString("clinicname" + tab);
                apiservice
                    .getDoctorQueue(curdate, clinicCode, doctorPerid)
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
                    .then((clinicqueueresponse) => {
                        let queuelist = clinicqueueresponse.data;
                        let doctorqueueinfo;
                        if (queuelist.length != 0) {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: clinicName,
                                queueList: queuelist,
                            };
                        } else {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: "",
                                queueList: [],
                            };
                        }
                        setQueueList(
                            tab,
                            doctorinfo,
                            doctorqueueinfo,
                            queuelist
                        );
                    });
                break;
            default:
                break;
        }
    } else {
        console.log("ไม่พบข้อมูลคลินิก");
    }
}

// Step 4 เช็ค action ว่าเป็น new หรือ reload
function setQueueList(tab, doctorinfo, doctorqueueinfo, queuelist) {
    let data = [];
    if (queuelist.length != 0) {
        // console.log(queuelist);
        queuelist.forEach((element) => {
            let name = "name" + tab;
            let hn = "hn" + tab;
            let qtime = "qtime" + tab;
            let qStatus = "";
            let statuscolor = "statuscolor" + tab;
            let time;
            let status = "status" + tab;
            let patientimg = "patientimg" + tab;
            let patientimgPath = "";

            switch (element.sex) {
                case "1":
                    patientimgPath = "~/src/img/man-patient.png";
                    break;
                case "2":
                    patientimgPath = "~/src/img/woman-patient.png";
                    break;
                default:
                    break;
            }

            // ตรวจสอบผู้ป่วยที่ยังตรวจไม่เสร็จ หรือ กำลังรอพบแพทย์ รอพบพยาบาล
            // nurse_time = เวลาที่ผู้ป่วยพบพยาบาลเรียบร้อยแล้ว หรือ ออกจากคลินิกแล้ว
            // if (element.nurse_time == "") {
            let backgroundColor = "";
            if (element.q_exit_nurse != "" && element.q_exit_nurse != "00:00") {
                qStatus = "รอพบพยาบาล";
                time = element.q_exit_nurse;
                backgroundColor = "#4BAEA0";
            } else if (
                element.q_doct_time != "" &&
                element.q_doct_time != "00:00"
            ) {
                qStatus = "รอพบแพทย์";
                time = element.q_doct_time;
                backgroundColor = "#D45D79";
            } else {
                qStatus = "รอเรียก";
                time = element.q_time;
                backgroundColor = "#30475E";
            }
            data.push({
                [name]: element.patientname,
                [hn]: "HN" + element.hn,
                [status]: qStatus,
                [statuscolor]: backgroundColor,
                [qtime]: time,
                [patientimg]: patientimgPath,
            });
            // }
        });
        let cName = doctorqueueinfo.clinicName;
        let profileimg = page.getViewById("profileimg" + tab);
        let doctorname = page.getViewById("doctorname" + tab);
        let clinicname = page.getViewById("clinicname" + tab);
        let patientamount = page.getViewById("patientamount" + tab);

        let dName = doctorinfo.doctorname;
        let dSex = doctorinfo.sex;
        // Check doctor sex to set default profile image
        switch (dSex) {
            case "1":
                profileimg.src = "~/src/img/woman-doctor.png";
                break;
            case "2":
                profileimg.src = "~/src/img/man-doctor.png";
                break;
            default:
                break;
        }

        doctorname.text = dName;
        clinicname.text = "คลินิก" + cName.trim();
        patientamount.text = "จำนวนผู้ป่วย: " + data.length + " คน";

        switch (tab) {
            case "1":
                data1 = data;
                break;
            case "2":
                data2 = data;
                break;
            case "3":
                data3 = data;
            case "4":
                data4 = data;
            case "5":
                data5 = data;
            default:
                break;
        }
        setRadListView("showlistview", tab);
    } else {
        console.log("ไม่พบข้อมูลคิวตรวจ");
    }
}

// Step 5 นำข้อมูลคิวใส่ใน list
function setRadListView(action, tab) {
    const vm = fromObject({
        dataItems1: data1,
        dataItems2: data2,
        dataItems3: data3,
        dataItems4: data4,
        dataItems5: data5,
    });
    page.bindingContext = vm;
    switchUI(action, tab);
}

function getDateString(date) {
    let monthStr;
    let dayStr;
    if (parseInt(date.getMonth() + 1) <= 9) {
        monthStr = "0" + (date.getMonth() + 1);
    } else {
        monthStr = date.getMonth() + 1;
    }

    if (parseInt(date.getDate()) <= 9) {
        dayStr = "0" + date.getDate();
    } else {
        dayStr = date.getDate();
    }

    let newDateFormat = {
        year: date.getFullYear(),
        month: monthStr,
        day: dayStr,
    };

    return newDateFormat;
}

function clearData(tab) {
    appSettings.remove("doctor" + tab);
    switch (tab) {
        case "1":
            data1 = [];
            break;
        case "2":
            data2 = [];
            break;
        case "3":
            data3 = [];
            break;
        case "4":
            data4 = [];
            break;
        case "5":
            data5 = [];
            break;
        default:
            break;
    }
}

function switchUI(action, tab) {
    // console.log(action);
    // console.log(tab);
    switch (action) {
        case "showlistview":
            switch (tab) {
                case "1":
                    btnDoctor1.visibility = "collapse";
                    btnDoctor1.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor1.visibility = "visible";
                    doctor1.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "2":
                    btnDoctor2.visibility = "collapse";
                    btnDoctor2.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor2.visibility = "visible";
                    doctor2.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "3":
                    btnDoctor3.visibility = "collapse";
                    btnDoctor3.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor3.visibility = "visible";
                    doctor3.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "4":
                    btnDoctor4.visibility = "collapse";
                    btnDoctor4.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor4.visibility = "visible";
                    doctor4.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "5":
                    btnDoctor5.visibility = "collapse";
                    btnDoctor5.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor5.visibility = "visible";
                    doctor5.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                default:
                    break;
            }
            break;
        case "hidelistview":
            switch (tab) {
                case "1":
                    btnDoctor1.visibility = "visible";
                    btnDoctor1.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor1.visibility = "collapse";
                    doctor1.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "2":
                    btnDoctor2.visibility = "visible";
                    btnDoctor2.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor2.visibility = "collapse";
                    doctor2.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "3":
                    btnDoctor3.visibility = "visible";
                    btnDoctor3.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor3.visibility = "collapse";
                    doctor3.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "4":
                    btnDoctor4.visibility = "visible";
                    btnDoctor4.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor4.visibility = "collapse";
                    doctor4.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "5":
                    btnDoctor5.visibility = "visible";
                    btnDoctor5.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor5.visibility = "collapse";
                    doctor5.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
}
