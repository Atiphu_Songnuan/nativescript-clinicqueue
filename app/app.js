let application = require("tns-core-modules/application");
const appSettings = require("tns-core-modules/application-settings");
const LiveUpdate = require("./liveUpdate");
global.conf = require("./configs");

new LiveUpdate(global.conf.updateKey);

let token = appSettings.getString("token", "tokenexpired");
// console.log("Root-User Token: " + token);
if (token != "tokenexpired") {
    application.run({ moduleName: "app-root-main" });
} else {
    application.run({ moduleName: "app-root" });
}
