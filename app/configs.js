config = {
    name: "Clinic Queue",
    ver: "Version 1.0 (build 010720)",

    api: {
        isdoctor: "http://61.19.201.20:19539/clinicqueue/doctor/isdoctor",
        doctorclinic: "http://61.19.201.20:19539/clinicqueue/doctor/clinic",
        doctorqueue: "http://61.19.201.20:19539/clinicqueue/doctor/queue",
        patientinfo: "http://61.19.201.20:19539/clinicqueue/patient/info",
        patienttime: "http://61.19.201.20:19539/clinicqueue/patient/time",
        updatepatienttime: "http://61.19.201.20:19539/clinicqueue/patient/timeupdate",
        insertpatienttime: "http://61.19.201.20:19539/clinicqueue/patient/timeinsert",

    },

    updateKey: {
        android: "c0pxsa4DBl1ipZJHG6hjhz94LSwZpbVYic0w5"
    }
};

module.exports = config;
