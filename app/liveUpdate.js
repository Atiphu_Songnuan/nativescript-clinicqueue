const application = require("tns-core-modules/application");
const AppSync = require("nativescript-app-sync").AppSync;
const InstallMode = require("nativescript-app-sync").InstallMode;
const SyncStatus  = require("nativescript-app-sync").SyncStatus ;
const platform = require("tns-core-modules/platform");

function liveUpdate(config) {
    if (platform.isIOS) {
        console.log("IOS device token: " + config.ios);
    } else {
        console.log("Android device token: " + config.android);
    }
    application.on(application.resumeEvent, function() {
        AppSync.sync({
            enabledWhenUsingHmr: false,
            deploymentKey: platform.isIOS ? config.ios : config.android,
            installMode: InstallMode.IMMEDIATE,
            mandatoryInstallMode: platform.isIOS
                ? InstallMode.ON_NEXT_RESUME
                : InstallMode.IMMEDIATE,
            updateDialog: {
                optionalUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                updateTitle: "อัพเดทเวอร์ชันใหม่",
                mandatoryUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                optionalIgnoreButtonLabel: "ยกเลิก",
                mandatoryContinueButtonLabel: platform.isIOS
                    ? "ปิด"
                    : "รีสตาร์ท",
                appendReleaseDescription: true
            }
        },
         (syncStatus) => {
            if (syncStatus === SyncStatus.UP_TO_DATE) {
              console.log("No pending updates; you’re running the latest version.");
            } else if (syncStatus === SyncStatus.UPDATE_INSTALLED) {
              console.log("Update found and installed. It will be activated upon next cold reboot");
            }
         });
    });
}

module.exports = liveUpdate;
