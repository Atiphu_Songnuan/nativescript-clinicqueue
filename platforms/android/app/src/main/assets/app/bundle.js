require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app-root.xml": "./app-root.xml",
	"./app.css": "./app.css",
	"./app.js": "./app.js",
	"./configs.js": "./configs.js",
	"./liveUpdate.js": "./liveUpdate.js",
	"./views/doctor/doctor-modal.css": "./views/doctor/doctor-modal.css",
	"./views/doctor/doctor-modal.js": "./views/doctor/doctor-modal.js",
	"./views/doctor/doctor-modal.xml": "./views/doctor/doctor-modal.xml",
	"./views/main/main-page-model.js": "./views/main/main-page-model.js",
	"./views/main/main-page.css": "./views/main/main-page.css",
	"./views/main/main-page.js": "./views/main/main-page.js",
	"./views/main/main-page.xml": "./views/main/main-page.xml",
	"./views/modal/modal-page.css": "./views/modal/modal-page.css",
	"./views/modal/modal-page.js": "./views/modal/modal-page.js",
	"./views/modal/modal-page.xml": "./views/modal/modal-page.xml",
	"./views/patient/patient-modal.css": "./views/patient/patient-modal.css",
	"./views/patient/patient-modal.js": "./views/patient/patient-modal.js",
	"./views/patient/patient-modal.xml": "./views/patient/patient-modal.xml"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$";

/***/ }),

/***/ "./app-root.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Frame defaultPage=\"/views/main/main-page\">\n</Frame>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app-root.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./app-root.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("~@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("~@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));
global.registerModule("@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"comment","comment":"\nIn NativeScript, the app.css file is where you place CSS rules that\nyou would like to apply to your entire application. Check out\nhttp://docs.nativescript.org/ui/styling for a full list of the CSS\nselectors and properties you can use to style UI components.\n\n/*\nIn many cases you may want to use the NativeScript core theme instead\nof writing your own CSS rules. You can learn more about the \nNativeScript core theme at https://github.com/nativescript/theme\nThe imported CSS rules must precede all other types of rules.\n"},{"type":"import","import":"\"~@nativescript/theme/css/core.css\""},{"type":"import","import":"\"~@nativescript/theme/css/default.css\""},{"type":"comment","comment":" Place any CSS rules you want to apply on both iOS and Android here.\nThis is where the vast majority of your CSS code goes. "},{"type":"comment","comment":"\nThe following CSS rule changes the font size of all Buttons that have the\n\"-primary\" class modifier.\n"},{"type":"rule","selectors":[".btn"],"declarations":[{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".text-font"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Medium\""}]},{"type":"rule","selectors":[".fas"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-solid-900\""},{"type":"declaration","property":"font-weight","value":"900"},{"type":"comment","comment":" font-size: 10; "}]},{"type":"rule","selectors":[".far"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-regular-400\""},{"type":"declaration","property":"font-weight","value":"400"},{"type":"comment","comment":" font-size: 10; "}]},{"type":"rule","selectors":[".header"],"declarations":[{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"text-align","value":"center"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".nodata-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"22"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./app.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
        let applicationCheckPlatform = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/@nativescript/core/ui/frame/frame.js");
__webpack_require__("../node_modules/@nativescript/core/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__coreModulesLiveSync = global.__onLiveSync;

            global.__onLiveSync = function () {
                // handle hot updated on LiveSync
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                // the hot updates are applied, ask the modules to apply the changes
                setTimeout(() => {
                    global.__coreModulesLiveSync({ type, path });
                });
            };

            // handle hot updated on initial app start
            hmrUpdate();
        }
        
            const context = __webpack_require__("./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$");
            global.registerWebpackModules(context);
            if (true) {
                module.hot.accept(context.id, () => { 
                    console.log("HMR: Accept module '" + context.id + "' from '" + module.i + "'"); 
                });
            }
            
        __webpack_require__("../node_modules/@nativescript/core/bundle-entry-points.js");
        let application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const LiveUpdate = __webpack_require__("./liveUpdate.js");
global.conf = __webpack_require__("./configs.js");

new LiveUpdate(global.conf.updateKey);

let token = appSettings.getString("token", "tokenexpired");
// console.log("Root-User Token: " + token);
if (token != "tokenexpired") {
    application.run({ moduleName: "app-root-main" });
} else {
    application.run({ moduleName: "app-root" });
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./app.js" });
    });
} 
    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./configs.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {config = {
    name: "Clinic Queue",
    ver: "Version 1.0 (build 010720)",

    api: {
        isdoctor: "http://61.19.201.20:19539/clinicqueue/doctor/isdoctor",
        doctorclinic: "http://61.19.201.20:19539/clinicqueue/doctor/clinic",
        doctorqueue: "http://61.19.201.20:19539/clinicqueue/doctor/queue",
        patientinfo: "http://61.19.201.20:19539/clinicqueue/patient/info",
        patienttime: "http://61.19.201.20:19539/clinicqueue/patient/time",
        updatepatienttime: "http://61.19.201.20:19539/clinicqueue/patient/timeupdate",
        insertpatienttime: "http://61.19.201.20:19539/clinicqueue/patient/timeinsert",

    },

    updateKey: {
        android: "c0pxsa4DBl1ipZJHG6hjhz94LSwZpbVYic0w5"
    }
};

module.exports = config;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./configs.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./configs.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./liveUpdate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const AppSync = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").AppSync;
const InstallMode = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").InstallMode;
const SyncStatus  = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").SyncStatus ;
const platform = __webpack_require__("../node_modules/@nativescript/core/platform/platform.js");

function liveUpdate(config) {
    if (platform.isIOS) {
        console.log("IOS device token: " + config.ios);
    } else {
        console.log("Android device token: " + config.android);
    }
    application.on(application.resumeEvent, function() {
        AppSync.sync({
            enabledWhenUsingHmr: false,
            deploymentKey: platform.isIOS ? config.ios : config.android,
            installMode: InstallMode.IMMEDIATE,
            mandatoryInstallMode: platform.isIOS
                ? InstallMode.ON_NEXT_RESUME
                : InstallMode.IMMEDIATE,
            updateDialog: {
                optionalUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                updateTitle: "อัพเดทเวอร์ชันใหม่",
                mandatoryUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                optionalIgnoreButtonLabel: "ยกเลิก",
                mandatoryContinueButtonLabel: platform.isIOS
                    ? "ปิด"
                    : "รีสตาร์ท",
                appendReleaseDescription: true
            }
        },
         (syncStatus) => {
            if (syncStatus === SyncStatus.UP_TO_DATE) {
              console.log("No pending updates; you’re running the latest version.");
            } else if (syncStatus === SyncStatus.UPDATE_INSTALLED) {
              console.log("Update found and installed. It will be activated upon next cold reboot");
            }
         });
    });
}

module.exports = liveUpdate;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./liveUpdate.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./liveUpdate.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"main":"app.js","android":{"v8Flags":"--expose_gc","markingMode":"none"}};

/***/ }),

/***/ "./views/doctor/doctor-modal.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/doctor/doctor-modal.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/doctor/doctor-modal.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/doctor/doctor-modal.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const observableModule = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");

let closeCallback;
let tab;
exports.onShownModally = (args) => {
   closeCallback = args.closeCallback;
};

exports.onAddNewDoctor = (args) => {
    let page = args.object.page;
    let perid = page.getViewById("doctorperid");
    closeCallback(perid.text);
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/doctor/doctor-modal.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/doctor/doctor-modal.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/doctor/doctor-modal.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page shownModally=\"onShownModally\"\n    xmlns:lv=\"nativescript-ui-listview\">\n    <StackLayout width=\"100%\">\n        <GridLayout columns=\"auto, auto\" rows=\"auto\">\n            <Image class=\"fas m-l-16 m-t-16\" row=\"0\" col=\"0\" src=\"font://&#xf0f0;\" width=\"20\" height=\"20\" color=\"#4a774e\"/>\n            <Label class=\"text-font m-t-16 m-l-8\" row=\"0\" col=\"1\" fontSize=\"20\" text=\"ระบุรหัสแพทย์\"></Label>\n        </GridLayout>\n        <TextField id=\"doctorperid\" class=\"text-font text-center m-b-16 m-t-8\" hint=\"รหัสแพทย์\" fontSize=\"16\" style=\"placeholder-color: #BBBBBB\" keyboardType=\"phone\"></TextField>\n        <button id=\"btnadddoctor\" class=\"text-font p-l-16 p-r-16 m-b-16 m-t-8\" col=\"0\" row=\"0\" text=\"ยืนยัน\" borderRadius=\"8\" tap=\"onAddNewDoctor\" backgroundColor=\"#1e4b25\" color=\"white\"></button>\n    </StackLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/doctor/doctor-modal.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/doctor/doctor-modal.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page-model.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const Observable = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").Observable;
const http = __webpack_require__("../node_modules/@nativescript/core/http/http.js");

function APIService() {
    let viewModel = new Observable();
    let result = new Array();
    // console.log(global.conf.api.doctorclinic);
    viewModel.getDoctorInfo = (perid) => {
        return http
            .request({
                url: global.conf.api.isdoctor,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getClinic = (perid, curdate) => {
        return http
            .request({
                url: global.conf.api.doctorclinic,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                    date_d: curdate,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getDoctorQueue = (curdate, cliniccode, doctorperid) => {
        return http
            .request({
                url: global.conf.api.doctorqueue,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    date_d: curdate,
                    code_b: cliniccode,
                    c_doct: doctorperid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getPatientInfo = (hn) => {
        return http
            .request({
                url: global.conf.api.patientinfo,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    hn: hn,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.statusCode != 200) {
                        result = {
                            statusCode: 400,
                            data: [],
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.checkPatientTime = (patientinfo) => {
        let currentdate = patientinfo.date;
        let clinicCode = patientinfo.code_b;
        let doctorPerid = patientinfo.c_doct;
        let hn = patientinfo.hn;
        return http
            .request({
                url: global.conf.api.patienttime,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    date_d: currentdate,
                    code_b: clinicCode,
                    c_doct: doctorPerid,
                    hn: hn,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    result = {
                        statusCode: 200,
                        action: res.action,
                    };
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        action: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.updatePatientTime = (patientinfo) => {
        let updateTimeURL = "";
        switch (patientinfo.action) {
            case "update":
                console.log("updated");
                updateTimeURL = global.conf.api.updatepatienttime;
                break;
            case "insert":
                console.log("insert");
                updateTimeURL = global.conf.api.insertpatienttime;
                break;
            default:
                break;
        }

        let hn = patientinfo.hn;
        let currentdate = patientinfo.date;
        let clinicCode = patientinfo.code_b;
        let doctorPerid = patientinfo.c_doct;

        return http
            .request({
                url: updateTimeURL,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    hn: hn,
                    date_d: currentdate,
                    code_b: clinicCode,
                    c_doct: doctorPerid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    result = {
                        statusCode: 200,
                        status: res.data,
                    };
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };
    return viewModel;
}

module.exports = {
    APIService: APIService,
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page-model.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main/main-page-model.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":["TabStrip"],"declarations":[{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"color","value":"darkgray"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem"],"declarations":[{"type":"declaration","property":"color","value":"darkgray"},{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"margin-top","value":"4"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem:active"],"declarations":[{"type":"declaration","property":"color","value":"#1c8445"},{"type":"declaration","property":"background-color","value":"mintcream"},{"type":"declaration","property":"margin-top","value":"4"}]},{"type":"rule","selectors":[".text-name-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Bold\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".text-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"14"}]},{"type":"rule","selectors":[".card-doctor-profile"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"comment","comment":" background-color: white; "},{"type":"declaration","property":"background-image","value":"url(\"~/src/img/doctor-card.png\")"},{"type":"declaration","property":"background-repeat","value":"no-repeat"},{"type":"declaration","property":"background-size","value":"cover"},{"type":"declaration","property":"background-position","value":"top"},{"type":"declaration","property":"color","value":"#454058"}]},{"type":"rule","selectors":[".listview-patient"],"declarations":[{"type":"comment","comment":" margin-left: 8;\n  margin-right: 8;\n  margin-bottom: 8; "},{"type":"declaration","property":"border-radius","value":"30 30 0 0"},{"type":"declaration","property":"background-color","value":"#fee6d2"},{"type":"comment","comment":" color: white; "}]},{"type":"rule","selectors":[".listview-item"],"declarations":[{"type":"comment","comment":" margin-top: 16;\n  margin-left: 16;\n  margin-right: 16;\n  margin-bottom: 16; "},{"type":"comment","comment":" margin-top:8 ;\n  margin-left: 16;\n  margin-right: 16; "},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"background-color","value":"white"}]},{"type":"rule","selectors":[".patient-image"],"declarations":[{"type":"declaration","property":"margin-left","value":"8"},{"type":"declaration","property":"border-radius","value":"50%"},{"type":"declaration","property":"background-color","value":"crimson"}]},{"type":"rule","selectors":[".add-doctor-btn"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"padding","value":"16"},{"type":"declaration","property":"background-color","value":"darkorange"},{"type":"declaration","property":"color","value":"white"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/main/main-page.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const view = __webpack_require__("../node_modules/@nativescript/core/ui/core/view/view.js");
const MainPageModel = __webpack_require__("./views/main/main-page-model.js");
const fromObject = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").fromObject;
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const platform = __webpack_require__("../node_modules/@nativescript/core/platform/platform.js");
const dialogs = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");
const nstoasts = __webpack_require__("../node_modules/nativescript-toasts/index.js");

const modalViewModule = "/views/modal/modal-page.js";
const doctorViewModule = "/views/doctor/doctor-modal.js";
const patientViewModule = "/views/patient/patient-modal.js";

let apiservice = MainPageModel.APIService();
let page;
let modalView;

// Doctor1
let data1 = [];
let btnDoctor1;
let doctor1;

// Doctor2
let data2 = [];
let btnDoctor2;
let doctor2;

// Doctor3
let data3 = [];
let btnDoctor3;
let doctor3;

// Doctor4
let data4 = [];
let btnDoctor4;
let doctor4;

// Doctor5
let data5 = [];
let btnDoctor5;
let doctor5;

let curdate = getDateString(new Date());
// let curDateStr = curdate.year + "-" + curdate.month + "-" + curdate.day;
let curDateStr = "2020-07-05";

exports.onPageLoaded = (args) => {
    page = args.object.page;
    modalView = args.object;
    // newDoctorBtn = page.getViewById("newdoctorbtn");
    // if (platform.isIOS) {
    //     newDoctorBtn.style = "font-size: 18em;";
    // } else {
    //     newDoctorBtn.style = "font-size: 14em;";
    // }
};

exports.addDoctor1 = () => {
    btnDoctor1 = page.getViewById("btnDoctor1");
    doctor1 = page.getViewById("doctor1");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "1", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated1 = () => {
    let doctorperid = appSettings.getString("doctor1");
    let listView1 = page.getViewById("listview1");
    getDoctorInfo("reload", "1", doctorperid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView1.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor2 = () => {
    btnDoctor2 = page.getViewById("btnDoctor2");
    doctor2 = page.getViewById("doctor2");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "2", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated2 = () => {
    let perid = appSettings.getString("doctor2");
    let listView2 = page.getViewById("listview2");
    getDoctorInfo("reload", "2", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView2.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor3 = () => {
    btnDoctor3 = page.getViewById("btnDoctor3");
    doctor3 = page.getViewById("doctor3");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "3", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated3 = () => {
    let perid = appSettings.getString("doctor3");
    let listView3 = page.getViewById("listview3");
    getDoctorInfo("reload", "3", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView3.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor4 = () => {
    btnDoctor4 = page.getViewById("btnDoctor4");
    doctor4 = page.getViewById("doctor4");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "4", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated4 = () => {
    let perid = appSettings.getString("doctor4");
    let listView4 = page.getViewById("listview4");
    getDoctorInfo("reload", "4", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView4.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor5 = () => {
    btnDoctor5 = page.getViewById("btnDoctor5");
    doctor5 = page.getViewById("doctor5");
    const option = {
        closeCallback: (doctorperid) => {
            if (doctorperid != "") {
                getDoctorInfo("new", "5", doctorperid, curDateStr);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(doctorViewModule, option);
};

exports.onPullToRefreshInitiated5 = () => {
    let perid = appSettings.getString("doctor5");
    let listView5 = page.getViewById("listview5");
    getDoctorInfo("reload", "5", perid, curDateStr);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView5.notifyPullToRefreshFinished();
    }, 500);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    let tabindex = args.object.tabindex;
    let listView = page.getViewById("listview" + tabindex);
    let selectedItems = listView.getSelectedItems();

    let patientdata = selectedItems[0];
    // console.log(patientdata);
    // let doctorperid = appSettings.getString("doctor" + tabindex);
    // let clinicCode = appSettings.getString("cliniccode" + tabindex);
    let pHN = patientdata["hn" + tabindex];
    let pName = patientdata["name" + tabindex];
    let qTime = patientdata["qtime" + tabindex];

    const option = {
        context: {
            tab: tabindex,
            date: "2020-07-05", //อย่าลืมแก้กลับเป็นวันตรวจปัจจุบัน Curdate
            hn: pHN,
            name: pName,
            time: qTime,
        },
        closeCallback: (patientinfo) => {
            if (typeof patientinfo == "object") {
                let doctorperid = patientinfo.c_doct;
                let date = patientinfo.date;
                apiservice
                    .checkPatientTime(patientinfo)
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
                    .then((response) => {
                        patientinfo["action"] = response.action;
                        console.log(patientinfo);
                        apiservice
                            .updatePatientTime(patientinfo)
                            .catch((err) => {
                                console.log(err);
                                reject(err);
                            });
                        getDoctorInfo("reload", tabindex, doctorperid, date);
                    });
            } else {
                console.log("close patient modal");
                listView.deselectItemAt(args.index);
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(patientViewModule, option);
};

exports.onDoctorClosed = (args) => {
    const page = args.object.page;
    let tabindex = args.object.tabindex;
    let perid = appSettings.getString("doctor" + tabindex);
    dialogs
        .confirm({
            title: "นำแพทย์ออกจากคลินิก",
            message: "",
            okButtonText: "ตกลง",
            cancelButtonText: "ยกเลิก",
            cancelable: false,
        })
        .then((r) => {
            if (r) {
                clearData(tabindex);
                setRadListView("hidelistview", tabindex);
                let options = {
                    text: "นำแพทย์ออกจากคลินิกสำเร็จ",
                    duration: nstoasts.DURATION.SHORT,
                    position: nstoasts.POSITION.BOTTOM, //optional
                };
                nstoasts.show(options);
            }
        });
};

// Step 1 ตรวจสอบข้อมูลแพทย์
function getDoctorInfo(action, tab, perid, curdate) {
    let doctorInfoProms = new Promise((resolve, reject) => {
        apiservice
            .getDoctorInfo(perid)
            .catch((err) => {
                console.log(err);
                reject(err);
            })
            .then((inforesponse) => {
                let info = inforesponse.data;
                if (info.length != 0) {
                    appSettings.remove("doctor" + tab);
                    appSettings.setString("doctor" + tab, perid);
                    // let doctorname = doctorinfo[0].doctorname;
                    // let doctorsex = doctorinfo[0].sex;
                    let doctorinfo = {
                        doctorname: info[0].doctorname,
                        sex: info[0].sex,
                    };
                    resolve(doctorinfo);
                } else {
                    console.log("ไม่พบข้อมูลแพทย์");
                }
            });
    });
    doctorInfoProms.then((doctorinfo) => {
        getDoctorClinicInfo(action, tab, perid, curdate, doctorinfo);
    });
}

// Step 2 ตรวจสอบข้อมูลคลินิกที่เลือก
function getDoctorClinicInfo(action, tab, perid, curdate, doctorinfo) {
    let clinicInfoProms = new Promise((resolve, reject) => {
        apiservice
            .getClinic(perid, curdate)
            .catch((err) => {
                console.log(err);
                reject(err);
                return Promise.reject();
            })
            .then((response) => {
                let clinicList = response.data;
                if (clinicList.length != 0) {
                    resolve(clinicList);
                } else {
                    resolve([]);
                    console.log("ไม่มีคลินิก");
                }
            });
    });
    clinicInfoProms.then((clinicinfo) => {
        getDoctorQueue(action, tab, clinicinfo, doctorinfo, curdate);
    });
}

// Step 3 ดึงข้อมูลคิวตรวจของแพทย์ในคลินิกนั้นๆ
function getDoctorQueue(action, tab, clinicinfo, doctorinfo, curdate) {
    if (clinicinfo.length != 0) {
        // const mainView = args.object;
        switch (action) {
            case "new":
                const option = {
                    context: {
                        tab: tab,
                        doctorname: doctorinfo.doctorname,
                        cliniclist: clinicinfo,
                    },
                    closeCallback: (doctorqueueinfo) => {
                        if (typeof doctorqueueinfo == "object") {
                            // appSettings.setString("");
                            let queuelist = doctorqueueinfo.queueList;
                            setQueueList(
                                tab,
                                doctorinfo,
                                doctorqueueinfo,
                                queuelist
                            );
                        } else {
                            console.log("close modal");
                        }
                    },
                    fullscreen: false,
                    animated: true,
                };
                modalView.showModal(modalViewModule, option);
                break;
            case "reload":
                let doctorPerid = appSettings.getString("doctor" + tab);
                let clinicCode = appSettings.getString("cliniccode" + tab);
                let clinicName = appSettings.getString("clinicname" + tab);
                apiservice
                    .getDoctorQueue(curdate, clinicCode, doctorPerid)
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
                    .then((clinicqueueresponse) => {
                        let queuelist = clinicqueueresponse.data;
                        let doctorqueueinfo;
                        if (queuelist.length != 0) {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: clinicName,
                                queueList: queuelist,
                            };
                        } else {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: "",
                                queueList: [],
                            };
                        }
                        setQueueList(
                            tab,
                            doctorinfo,
                            doctorqueueinfo,
                            queuelist
                        );
                    });
                break;
            default:
                break;
        }
    } else {
        console.log("ไม่พบข้อมูลคลินิก");
    }
}

// Step 4 เช็ค action ว่าเป็น new หรือ reload
function setQueueList(tab, doctorinfo, doctorqueueinfo, queuelist) {
    let data = [];
    if (queuelist.length != 0) {
        // console.log(queuelist);
        queuelist.forEach((element) => {
            let name = "name" + tab;
            let hn = "hn" + tab;
            let qtime = "qtime" + tab;
            let qStatus = "";
            let statuscolor = "statuscolor" + tab;
            let time;
            let status = "status" + tab;
            let patientimg = "patientimg" + tab;
            let patientimgPath = "";

            switch (element.sex) {
                case "1":
                    patientimgPath = "~/src/img/man-patient.png";
                    break;
                case "2":
                    patientimgPath = "~/src/img/woman-patient.png";
                    break;
                default:
                    break;
            }

            // ตรวจสอบผู้ป่วยที่ยังตรวจไม่เสร็จ หรือ กำลังรอพบแพทย์ รอพบพยาบาล
            // nurse_time = เวลาที่ผู้ป่วยพบพยาบาลเรียบร้อยแล้ว หรือ ออกจากคลินิกแล้ว
            // if (element.nurse_time == "") {
            let backgroundColor = "";
            if (element.q_exit_nurse != "" && element.q_exit_nurse != "00:00") {
                qStatus = "รอพบพยาบาล";
                time = element.q_exit_nurse;
                backgroundColor = "#4BAEA0";
            } else if (
                element.q_doct_time != "" &&
                element.q_doct_time != "00:00"
            ) {
                qStatus = "รอพบแพทย์";
                time = element.q_doct_time;
                backgroundColor = "#D45D79";
            } else {
                qStatus = "รอเรียก";
                time = element.q_time;
                backgroundColor = "#30475E";
            }
            data.push({
                [name]: element.patientname,
                [hn]: "HN" + element.hn,
                [status]: qStatus,
                [statuscolor]: backgroundColor,
                [qtime]: time,
                [patientimg]: patientimgPath,
            });
            // }
        });
        let cName = doctorqueueinfo.clinicName;
        let profileimg = page.getViewById("profileimg" + tab);
        let doctorname = page.getViewById("doctorname" + tab);
        let clinicname = page.getViewById("clinicname" + tab);
        let patientamount = page.getViewById("patientamount" + tab);

        let dName = doctorinfo.doctorname;
        let dSex = doctorinfo.sex;
        // Check doctor sex to set default profile image
        switch (dSex) {
            case "1":
                profileimg.src = "~/src/img/woman-doctor.png";
                break;
            case "2":
                profileimg.src = "~/src/img/man-doctor.png";
                break;
            default:
                break;
        }

        doctorname.text = dName;
        clinicname.text = "คลินิก" + cName.trim();
        patientamount.text = "จำนวนผู้ป่วย: " + data.length + " คน";

        switch (tab) {
            case "1":
                data1 = data;
                break;
            case "2":
                data2 = data;
                break;
            case "3":
                data3 = data;
            case "4":
                data4 = data;
            case "5":
                data5 = data;
            default:
                break;
        }
        setRadListView("showlistview", tab);
    } else {
        console.log("ไม่พบข้อมูลคิวตรวจ");
    }
}

// Step 5 นำข้อมูลคิวใส่ใน list
function setRadListView(action, tab) {
    const vm = fromObject({
        dataItems1: data1,
        dataItems2: data2,
        dataItems3: data3,
        dataItems4: data4,
        dataItems5: data5,
    });
    page.bindingContext = vm;
    switchUI(action, tab);
}

function getDateString(date) {
    let monthStr;
    let dayStr;
    if (parseInt(date.getMonth() + 1) <= 9) {
        monthStr = "0" + (date.getMonth() + 1);
    } else {
        monthStr = date.getMonth() + 1;
    }

    if (parseInt(date.getDate()) <= 9) {
        dayStr = "0" + date.getDate();
    } else {
        dayStr = date.getDate();
    }

    let newDateFormat = {
        year: date.getFullYear(),
        month: monthStr,
        day: dayStr,
    };

    return newDateFormat;
}

function clearData(tab) {
    appSettings.remove("doctor" + tab);
    switch (tab) {
        case "1":
            data1 = [];
            break;
        case "2":
            data2 = [];
            break;
        case "3":
            data3 = [];
            break;
        case "4":
            data4 = [];
            break;
        case "5":
            data5 = [];
            break;
        default:
            break;
    }
}

function switchUI(action, tab) {
    // console.log(action);
    // console.log(tab);
    switch (action) {
        case "showlistview":
            switch (tab) {
                case "1":
                    btnDoctor1.visibility = "collapse";
                    btnDoctor1.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor1.visibility = "visible";
                    doctor1.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "2":
                    btnDoctor2.visibility = "collapse";
                    btnDoctor2.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor2.visibility = "visible";
                    doctor2.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "3":
                    btnDoctor3.visibility = "collapse";
                    btnDoctor3.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor3.visibility = "visible";
                    doctor3.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "4":
                    btnDoctor4.visibility = "collapse";
                    btnDoctor4.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor4.visibility = "visible";
                    doctor4.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                case "5":
                    btnDoctor5.visibility = "collapse";
                    btnDoctor5.animate({
                        opacity: 0,
                        duration: 300,
                    });

                    doctor5.visibility = "visible";
                    doctor5.animate({
                        opacity: 1,
                        duration: 300,
                    });
                    break;
                default:
                    break;
            }
            break;
        case "hidelistview":
            switch (tab) {
                case "1":
                    btnDoctor1.visibility = "visible";
                    btnDoctor1.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor1.visibility = "collapse";
                    doctor1.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "2":
                    btnDoctor2.visibility = "visible";
                    btnDoctor2.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor2.visibility = "collapse";
                    doctor2.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "3":
                    btnDoctor3.visibility = "visible";
                    btnDoctor3.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor3.visibility = "collapse";
                    doctor3.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "4":
                    btnDoctor4.visibility = "visible";
                    btnDoctor4.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor4.visibility = "collapse";
                    doctor4.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                case "5":
                    btnDoctor5.visibility = "visible";
                    btnDoctor5.animate({
                        opacity: 1,
                        duration: 300,
                    });

                    doctor5.visibility = "collapse";
                    doctor5.animate({
                        opacity: 0,
                        duration: 300,
                    });
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main/main-page.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplate", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });

module.exports = "<Page xmlns=\"http://schemas.nativescript.org/tns.xsd\"\n    xmlns:lv=\"nativescript-ui-listview\" loaded=\"onPageLoaded\">\n\n    <Page.actionBar>\n        <ActionBar backgroundColor=\"#1e4b25\" color=\"white\">\n            <Label class=\"header\" id=\"header\" text=\"Clinic Queue\"></Label>\n            <!-- <ActionItem class=\"fas\" id=\"newdoctorbtn\" icon=\"font://&#xf055;\" ios.position=\"right\" /> -->\n        </ActionBar>\n    </Page.actionBar>\n\n    <GridLayout>\n        <Tabs id=\"doctortabs\" selectedIndex=\"0\" selectedIndexChanged=\"onIndexSelected\">\n            <TabStrip>\n                <TabStripItem>\n                    <Label class=\"text-label\" text=\"แพทย์ 1\"></Label>\n                </TabStripItem>\n                <TabStripItem>\n                    <Label class=\"text-label\" text=\"แพทย์ 2\"></Label>\n                </TabStripItem>\n                <TabStripItem>\n                    <Label class=\"text-label\" text=\"แพทย์ 3\"></Label>\n                </TabStripItem>\n                <TabStripItem>\n                    <Label class=\"text-label\" text=\"แพทย์ 4\"></Label>\n                </TabStripItem>\n                <TabStripItem>\n                    <Label class=\"text-label\" text=\"แพทย์ 5\"></Label>\n                </TabStripItem>\n            </TabStrip>\n\n            <TabContentItem>\n                <StackLayout>\n                    <FlexboxLayout id=\"btnDoctor1\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                        <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                            <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 1\" tap=\"addDoctor1\"></Button>\n                        </GridLayout>\n                    </FlexboxLayout>\n                    <StackLayout id=\"doctor1\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                        <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                            <GridLayout columns=\"auto, *, auto\" rows=\"auto\">\n                                <Image col=\"0\" row=\"0\" id=\"profileimg1\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                    <Label class=\"text-name-label text-left\" id=\"doctorname1\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"clinicname1\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"patientamount1\"></Label>\n                                </StackLayout>\n                                <StackLayout class=\"p-8\" col=\"2\" row=\"0\" horizontalAlignment=\"right\">\n                                    <button class=\"text-center\" text=\"x\" borderRadius=\"50%\" width=\"24\" height=\"24\" backgroundColor=\"crimson\" color=\"white\" tap=\"onDoctorClosed\" tabindex=\"1\"></button>\n                                </StackLayout>\n                            </GridLayout>\n                        </StackLayout>\n\n                        <lv:RadListView id=\"listview1\" class=\"listview-patient p-b-16\" items=\"{{dataItems1}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated1\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" tabindex=\"1\">\n                            <lv:RadListView.itemTemplate>\n                                <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *\" rows=\"auto\">\n                                    <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg1}}\" width=\"60\" height=\"60\"/>\n                                    <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                        <Label class=\"text-font p-l-8 p-r-8\" fontSize=\"20\" text=\"{{name1}}\"></Label>\n                                        <GridLayout columns=\"2*, *, *\" rows=\"*\">\n                                            <Label col=\"0\" row=\"0\" class=\"text-font p-l-8 p-r-8\" fontSize=\"16\" text=\"{{hn1}}\"></Label>\n                                            <Label col=\"1\" row=\"0\" class=\"text-font text-center p-l-8 p-r-8\" fontSize=\"14\" text=\"{{status1}}\" backgroundColor=\"{{statuscolor1}}\" borderRadius=\"50%\" color=\"yellow\"></Label>\n                                            <Label col=\"2\" row=\"0\" class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime1}}\"></Label>\n                                        </GridLayout>\n                                    </StackLayout>\n                                </GridLayout>\n                            </lv:RadListView.itemTemplate>\n                        </lv:RadListView>\n                    </StackLayout>\n                </StackLayout>\n            </TabContentItem>\n\n            <TabContentItem>\n                <StackLayout>\n                    <FlexboxLayout id=\"btnDoctor2\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                        <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                            <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 2\" tap=\"addDoctor2\"></Button>\n                        </GridLayout>\n                    </FlexboxLayout>\n                    <StackLayout id=\"doctor2\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                        <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                            <GridLayout columns=\"auto, *, auto\" rows=\"auto\">\n                                <Image col=\"0\" row=\"0\" id=\"profileimg2\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                    <Label class=\"text-name-label text-left\" id=\"doctorname2\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"clinicname2\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"patientamount2\"></Label>\n                                </StackLayout>\n                                <StackLayout class=\"p-8\" col=\"2\" row=\"0\" horizontalAlignment=\"right\">\n                                    <button class=\"text-center\" text=\"x\" borderRadius=\"50%\" width=\"24\" height=\"24\" backgroundColor=\"crimson\" color=\"white\" tap=\"onDoctorClosed\" tabindex=\"2\"></button>\n                                </StackLayout>\n                            </GridLayout>\n                        </StackLayout>\n\n                        <lv:RadListView id=\"listview2\" class=\"listview-patient p-b-16\" items=\"{{dataItems2}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated2\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" tabindex=\"2\">\n                            <lv:RadListView.itemTemplate>\n                                <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *\" rows=\"auto\">\n                                    <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg2}}\" width=\"60\" height=\"60\"/>\n                                    <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                        <Label class=\"text-font p-l-8 p-r-8\" fontSize=\"20\" text=\"{{name2}}\"></Label>\n                                        <GridLayout columns=\"2*, *, *\" rows=\"*\">\n                                            <Label col=\"0\" row=\"0\" class=\"text-font p-l-8 p-r-8\" fontSize=\"16\" text=\"{{hn2}}\"></Label>\n                                            <Label col=\"1\" row=\"0\" class=\"text-font text-center p-l-8 p-r-8\" fontSize=\"14\" text=\"{{status2}}\" backgroundColor=\"{{statuscolor2}}\" borderRadius=\"50%\" color=\"yellow\"></Label>\n                                            <Label col=\"2\" row=\"0\" class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime2}}\"></Label>\n                                        </GridLayout>\n                                    </StackLayout>\n                                </GridLayout>\n                            </lv:RadListView.itemTemplate>\n                        </lv:RadListView>\n                    </StackLayout>\n                </StackLayout>\n            </TabContentItem>\n\n            <TabContentItem>\n                <StackLayout>\n                    <FlexboxLayout id=\"btnDoctor3\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                        <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                            <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 3\" tap=\"addDoctor3\"></Button>\n                        </GridLayout>\n                    </FlexboxLayout>\n                    <StackLayout id=\"doctor3\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                        <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                            <GridLayout columns=\"auto, *, auto\" rows=\"auto\">\n                                <Image col=\"0\" row=\"0\" id=\"profileimg3\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                    <Label class=\"text-name-label text-left\" id=\"doctorname3\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"clinicname3\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"patientamount3\"></Label> \n                                </StackLayout>\n                                <StackLayout class=\"p-8\" col=\"2\" row=\"0\" horizontalAlignment=\"right\">\n                                    <button class=\"text-center\" text=\"x\" borderRadius=\"50%\" width=\"24\" height=\"24\" backgroundColor=\"crimson\" color=\"white\" tap=\"onDoctorClosed\" tabindex=\"3\"></button>\n                                </StackLayout>\n                            </GridLayout>\n                        </StackLayout>\n\n                        <lv:RadListView id=\"listview3\" class=\"listview-patient p-b-16\" items=\"{{dataItems3}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated3\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" tabindex=\"3\">\n                            <lv:RadListView.itemTemplate>\n                                 <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *\" rows=\"auto\">\n                                    <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg3}}\" width=\"60\" height=\"60\"/>\n                                    <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                        <Label class=\"text-font p-l-8 p-r-8\" fontSize=\"20\" text=\"{{name3}}\"></Label>\n                                        <GridLayout columns=\"2*, *, *\" rows=\"*\">\n                                            <Label col=\"0\" row=\"0\" class=\"text-font p-l-8 p-r-8\" fontSize=\"16\" text=\"{{hn3}}\"></Label>\n                                            <Label col=\"1\" row=\"0\" class=\"text-font text-center p-l-8 p-r-8\" fontSize=\"14\" text=\"{{status3}}\" backgroundColor=\"{{statuscolor3}}\" borderRadius=\"50%\" color=\"yellow\"></Label>\n                                            <Label col=\"2\" row=\"0\" class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime3}}\"></Label>\n                                        </GridLayout>\n                                    </StackLayout>\n                                </GridLayout>\n                            </lv:RadListView.itemTemplate>\n                        </lv:RadListView>\n                    </StackLayout>\n                </StackLayout>\n            </TabContentItem>\n\n            <TabContentItem>\n                <StackLayout>\n                    <FlexboxLayout id=\"btnDoctor4\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                        <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                            <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 4\" tap=\"addDoctor4\"></Button>\n                        </GridLayout>\n                    </FlexboxLayout>\n                    <StackLayout id=\"doctor4\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                        <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                            <GridLayout columns=\"auto, *, auto\" rows=\"auto\">\n                                <Image col=\"0\" row=\"0\" id=\"profileimg4\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                    <Label class=\"text-name-label text-left\" id=\"doctorname4\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"clinicname4\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"patientamount4\"></Label>\n                                </StackLayout>\n                                <StackLayout class=\"p-8\" col=\"2\" row=\"0\" horizontalAlignment=\"right\">\n                                    <button class=\"text-center\" text=\"x\" borderRadius=\"50%\" width=\"24\" height=\"24\" backgroundColor=\"crimson\" color=\"white\" tap=\"onDoctorClosed\" tabindex=\"4\"></button>\n                                </StackLayout>\n                            </GridLayout>\n                        </StackLayout>\n\n                        <lv:RadListView id=\"listview4\" class=\"listview-patient p-b-16\" items=\"{{dataItems4}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated4\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" tabindex=\"4\">\n                            <lv:RadListView.itemTemplate>\n                                 <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *\" rows=\"auto\">\n                                    <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg4}}\" width=\"60\" height=\"60\"/>\n                                    <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                        <Label class=\"text-font p-l-8 p-r-8\" fontSize=\"20\" text=\"{{name4}}\"></Label>\n                                        <GridLayout columns=\"2*, *, *\" rows=\"*\">\n                                            <Label col=\"0\" row=\"0\" class=\"text-font p-l-8 p-r-8\" fontSize=\"16\" text=\"{{hn4}}\"></Label>\n                                            <Label col=\"1\" row=\"0\" class=\"text-font text-center p-l-8 p-r-8\" fontSize=\"14\" text=\"{{status4}}\" backgroundColor=\"{{statuscolor4}}\" borderRadius=\"50%\" color=\"yellow\"></Label>\n                                            <Label col=\"2\" row=\"0\" class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime4}}\"></Label>\n                                        </GridLayout>\n                                    </StackLayout>\n                                </GridLayout>\n                            </lv:RadListView.itemTemplate>\n                        </lv:RadListView>\n                    </StackLayout>\n                </StackLayout>\n            </TabContentItem>\n\n            <TabContentItem>\n                <StackLayout>\n                    <FlexboxLayout id=\"btnDoctor5\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                        <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                            <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 5\" tap=\"addDoctor5\"></Button>\n                        </GridLayout>\n                    </FlexboxLayout>\n                    <StackLayout id=\"doctor5\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                        <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                            <GridLayout columns=\"auto, *, auto\" rows=\"auto\">\n                                <Image col=\"0\" row=\"0\" id=\"profileimg5\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                    <Label class=\"text-name-label text-left\" id=\"doctorname5\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"clinicname5\"></Label>\n                                    <Label class=\"text-label text-left\" id=\"patientamount5\"></Label>\n                                </StackLayout>\n                                <StackLayout class=\"p-8\" col=\"2\" row=\"0\" horizontalAlignment=\"right\">\n                                    <button class=\"text-center\" text=\"x\" borderRadius=\"50%\" width=\"24\" height=\"24\" backgroundColor=\"crimson\" color=\"white\" tap=\"onDoctorClosed\" tabindex=\"5\"></button>\n                                </StackLayout>\n                            </GridLayout>\n                        </StackLayout>\n\n                        <lv:RadListView id=\"listview5\" class=\"listview-patient p-b-16\" items=\"{{dataItems5}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated5\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" tabindex=\"5\">\n                            <lv:RadListView.itemTemplate>\n                                 <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *\" rows=\"auto\">\n                                    <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg5}}\" width=\"60\" height=\"60\"/>\n                                    <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                        <Label class=\"text-font p-l-8 p-r-8\" fontSize=\"20\" text=\"{{name5}}\"></Label>\n                                        <GridLayout columns=\"2*, *, *\" rows=\"*\">\n                                            <Label col=\"0\" row=\"0\" class=\"text-font p-l-8 p-r-8\" fontSize=\"16\" text=\"{{hn5}}\"></Label>\n                                            <Label col=\"1\" row=\"0\" class=\"text-font text-center p-l-8 p-r-8\" fontSize=\"14\" text=\"{{status5}}\" backgroundColor=\"{{statuscolor5}}\" borderRadius=\"50%\" color=\"yellow\"></Label>\n                                            <Label col=\"2\" row=\"0\" class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime5}}\"></Label>\n                                        </GridLayout>\n                                    </StackLayout>\n                                </GridLayout>\n                            </lv:RadListView.itemTemplate>\n                        </lv:RadListView>\n                    </StackLayout>\n                </StackLayout>\n            </TabContentItem>\n\n        </Tabs>\n    </GridLayout>\n\n    <!-- <BottomNavigation id=\"bottomNav\" selectedIndex=\"0\">\n        <TabStrip>\n            <TabStripItem class=\"fas\" tap=\"doctorqueue\">\n                <Image src=\"font://&#xf0f0;\"></Image>\n                <Label class=\"text-font\" text=\"แพทย์\" fontSize=\"14\"></Label>\n            </TabStripItem>\n\n            <TabStripItem class=\"tabstripitem fas\" tap=\"settings\">\n                <Image src=\"font://&#xf085;\"></Image>\n                <Label class=\"text-font\" text=\"ตั้งค่า\" fontSize=\"14\"></Label>\n            </TabStripItem>\n        </TabStrip>\n\n\n\n        <TabContentItem backgroundColor=\"#eee\">\n            <ScrollView orientation=\"vertical\">\n                <FlexboxLayout flexDirection=\"column\">\n                    <GridLayout backgroundColor=\"#fff\" columns=\"auto,auto\" rows=\"*\" borderBottomColor=\"lightgray\">\n                        <Image id=\"settingprofileimg\" class=\"m-8\" row=\"0\" col=\"0\" src=\"{{profile}}\" width=\"65\" height=\"65\" stretch=\"aspectFill\" horizontalAlignment=\"left\"/>\n                        <StackLayout row=\"0\" col=\"1\" verticalAlignment=\"center\">\n                            <Label class=\"text-name-label\" text=\"ชื่อ-นามสกุล\" colSpan=\"2\" />\n                            <Label class=\"text-label\" text=\"หน่วยงาน\" colSpan=\"2\" />\n                        </StackLayout>\n                    </GridLayout>\n                    <GridLayout class=\"m-t-8\" height=\"60\" columns=\"auto,auto,*\" rows=\"*\" backgroundColor=\"#fff\">\n                        <Image class=\"fas m-l-28\" col=\"0\" src=\"font://&#xf2f5;\" width=\"22\" height=\"22\" color=\"#757575\"/>\n                        <Label class=\"text-label m-l-16\" col=\"1\" text=\"ออกจากระบบ\" verticalAlignment=\"center\"></Label>\n                    </GridLayout>\n                </FlexboxLayout>\n            </ScrollView>\n        </TabContentItem>\n    </BottomNavigation> -->\n</Page>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/main/main-page.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".listview-content"],"declarations":[{"type":"comment","comment":" background-image: url(\"app/views/main-page/src/checkin.jpg\") no-repeat; "},{"type":"comment","comment":" width: 95%; "},{"type":"comment","comment":" height: 100%; "},{"type":"declaration","property":"margin-top","value":"8"},{"type":"declaration","property":"margin-left","value":"8"},{"type":"declaration","property":"margin-right","value":"8"},{"type":"comment","comment":" margin-left: 8;\n    margin-right: 8; "},{"type":"comment","comment":" margin-bottom: 8; "},{"type":"declaration","property":"border-radius","value":"8"},{"type":"declaration","property":"background-color","value":"white"},{"type":"comment","comment":" color: white; "}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/modal/modal-page.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const observableModule = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");
const MainPageModel = __webpack_require__("./views/main/main-page-model.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");

let apiservice = MainPageModel.APIService();
let closeCallback;
let tab;

exports.onShownModally = (args) => {
    const clinicBody = args.context;
    tab = clinicBody.tab;
    closeCallback = args.closeCallback;
    const page = args.object;
    let clinicdata = [];
    let doctorPerid = clinicBody.cliniclist[0].c_doct;
    let doctorName = clinicBody.doctorname;
    let date_d = clinicBody.cliniclist[0].date_d;
    clinicBody.cliniclist.forEach((element) => {
        clinicdata.push({
            cliniccode: element.clinic_code,
            clinicname: element.clinic_name,
        });
    });
    page.bindingContext = observableModule.fromObject({
        doctorperid: doctorPerid,
        doctorname: doctorName,
        currentdate: date_d,
        clinicdata: clinicdata,
    });

    // const bindingContext = page.page.bindingContext;
    // let id = bindingContext.get("id");
    // let name = bindingContext.get("name");
    // closeCallback(id, name);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    let doctorPerid = page.getViewById("doctorperid").text;
    let currentdate = page.getViewById("currentdate").text;
    let listView = page.getViewById("listView");
    var selectedItems = listView.getSelectedItems();
    let clinicCode = selectedItems[0].cliniccode;
    let clinicName = selectedItems[0].clinicname;
    appSettings.setString("cliniccode"+tab, clinicCode);
    appSettings.setString("clinicname"+tab, clinicName);
    // console.log("Doctor Perid: " + doctorPerid.text);
    // console.log("Date: " + currentdate.text);
    // console.log(selectedItems[0]);
    apiservice
        .getDoctorQueue(currentdate, clinicCode, doctorPerid)
        .catch((err) => {
            console.log(err);
            reject(err);
        })
        .then((clinicqueueresponse) => {
            let clinicqueuelist = clinicqueueresponse.data;
            let clinicData;
            if (clinicqueuelist.length != 0) {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: clinicqueuelist,
                };
            } else {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: [],
                };
            }
            closeCallback(clinicData);
        });
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/modal/modal-page.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplate", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.pullToRefreshStyle", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/PullToRefreshStyle", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });

module.exports = "<Page shownModally=\"onShownModally\"\n    xmlns:lv=\"nativescript-ui-listview\">\n    <StackLayout backgroundColor=\"#1e4b25\" color=\"white\">\n        <GridLayout columns=\"auto, auto\" rows=\"auto\">\n            <Image class=\"fas m-l-16 m-t-8\" col=\"0\" row=\"0\" src=\"font://&#xf0f0;\" width=\"24\" height=\"24\"/>\n            <Label col=\"1\" row=\"0\" class=\"text-font m-l-8 m-t-8\" id=\"doctorname\" text=\"{{doctorname}}\" style=\"font-size: 20\"></Label>\n        </GridLayout>\n        <GridLayout columns=\"auto, auto, auto, auto\" rows=\"auto\">\n            <Image class=\"far m-l-16 m-t-8 m-b-8\" col=\"0\" row=\"0\" src=\"font://&#xf2c2;\" width=\"24\" height=\"24\"/>\n            <Label col=\"1\" row=\"0\" class=\"text-font m-l-8 m-t-8 m-b-8\" id=\"doctorperid\" text=\"{{doctorperid}}\" style=\"font-size: 20\"></Label>\n\n            <Image class=\"fas m-l-16 m-t-8 m-b-8\" col=\"2\" row=\"0\" src=\"font://&#xf073;\" width=\"24\" height=\"24\"/>\n            <Label col=\"3\" row=\"0\" class=\"text-font m-l-8 m-t-8 m-b-8\" id=\"currentdate\" text=\"{{currentdate}}\" style=\"font-size: 20\"></Label>\n        </GridLayout>\n\n        <lv:RadListView id=\"listView\" class=\"m-l-8 m-r-8\" items=\"{{ clinicdata }}\" backgroundColor=\"#eee\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" text=\"transferdetail\" style=\"height:50%; width:100%\">\n            <lv:RadListView.pullToRefreshStyle>\n                <lv:PullToRefreshStyle indicatorColor=\"#4a774e\" indicatorBackgroundColor=\"white\"/>\n            </lv:RadListView.pullToRefreshStyle>\n            <lv:RadListView.itemTemplate>\n                <StackLayout class=\"listview-content\" orientation=\"vertical\" padding=\"8\">\n                    <GridLayout columns=\"auto, auto, auto\" rows=\"auto, auto\">\n                        <Image class=\"fas\" row=\"0\" col=\"0\" src=\"font://&#xf0f1;\" width=\"20\" height=\"20\" color=\"#4a774e\"/>\n                        <Label class=\"text-font m-l-8\" row=\"0\" col=\"1\" text=\"{{cliniccode}}\" color=\"#4a774e\" style=\"font-size:18;\"></Label>\n                        <Label class=\"text-font\" row=\"0\" col=\"2\" text=\"{{clinicname}}\" color=\"#4a774e\" style=\"font-size:18;\"></Label>\n                    </GridLayout>\n                </StackLayout>\n            </lv:RadListView.itemTemplate>\n        </lv:RadListView>\n    </StackLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/modal/modal-page.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/patient/patient-modal.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const observableModule = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");

let page;
let closeCallback;
let curDate;
let tabindex;

exports.onShownModally = (args) => {
    page = args.object.page;
    closeCallback = args.closeCallback;
    const patientBody = args.context;
    curDate = patientBody.date;
    tabindex = patientBody.tab;
    // closeCallback = args.closeCallback;
    // const page = args.object;
    // console.log(clinicBody);
    // let clinicdata = [];
    // let doctorPerid = clinicBody.cliniclist[0].c_doct;
    // let date_d = clinicBody.cliniclist[0].date_d;

    page.bindingContext = observableModule.fromObject({
        patientname: patientBody.name,
        patienthn: patientBody.hn.split("HN")[1],
        queuetime: patientBody.time,
    });
};

exports.onClicked = (args) => {
    let service = args.object.service;
    let hnText = page.getViewById("patienthn");
    let hn = hnText.text;

    let result = {
        statusCode: 200,
        service: service,
        action: "",
        date: curDate,
        hn: hn,
        code_b: appSettings.getString("cliniccode" + tabindex),
        c_doct: appSettings.getString("doctor" + tabindex),
    };

    closeCallback(result);
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/patient/patient-modal.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page shownModally=\"onShownModally\"\n    xmlns:lv=\"nativescript-ui-listview\">\n    <StackLayout width=\"100%\">\n        <!-- <GridLayout rows=\"100 100\" columns=\"* 2*\">\n            <Label row=\"0\" col=\"0\" text=\"{{id}}\"></Label>\n            <Label row=\"0\" col=\"1\" text=\"{{name}}\"></Label>\n        </GridLayout> -->\n        <GridLayout columns=\"auto, auto\" rows=\"auto\">\n            <!-- <Image class=\"fas m-t-8 m-l-16 m-r-8\" col=\"0\" row=\"0\" src=\"font://&#xf2c2;\" width=\"24\" height=\"24\" color=\"#4a774e\"/> -->\n            <Label class=\"text-font m-t-8 m-l-16\" text=\"ชื่อ-สกุล: \" col=\"0\" row=\"0\" style=\"font-size: 20\"></Label>\n            <Label col=\"1\" row=\"0\" class=\"text-font m-t-8\" id=\"patientname\" text=\"{{patientname}}\" style=\"font-size: 20\" color=\"#4a774e\"></Label>\n        </GridLayout>\n\n        <GridLayout class=\"m-b-16\" columns=\"auto, auto, auto, auto\" rows=\"auto\">\n            <!-- <Image class=\"fas m-t-8 m-l-16 m-r-8\" col=\"0\" row=\"0\" src=\"font://&#xf0f8;\" width=\"24\" height=\"24\" color=\"#4a774e\"/> -->\n            <Label class=\"text-font m-t-8 m-l-16\" text=\"HN: \" col=\"0\" row=\"0\" style=\"font-size: 20\"></Label>\n            <Label col=\"1\" row=\"0\" class=\"text-font m-t-8\" id=\"patienthn\" text=\"{{patienthn}}\" style=\"font-size: 20\" color=\"#4a774e\"></Label>\n\n            <Label class=\"text-font m-t-8 m-l-16\" text=\"เวลา: \" col=\"2\" row=\"0\" style=\"font-size: 20\"></Label>\n            <!-- <Image class=\"fas m-t-8 m-l-16 m-r-8\" col=\"2\" row=\"0\" src=\"font://&#xf017;\" width=\"24\" height=\"24\" color=\"#4a774e\"/> -->\n            <Label col=\"3\" row=\"0\" class=\"text-font m-t-8\" id=\"queuetime\" text=\"{{queuetime}}\" style=\"font-size: 20\" color=\"#4a774e\"></Label>\n        </GridLayout>\n\n        <GridLayout class=\"m-b-8\" columns=\"*\" rows=\"auto, auto\">\n            <button class=\"text-font p-l-16 p-r-16\" col=\"0\" row=\"0\" text=\"รอพบแพทย์\" borderRadius=\"8\" backgroundColor=\"#D45D79\" color=\"white\" tap=\"onClicked\" service=\"doctorclosetime\" fontSize=\"18\"></button>\n            <button class=\"text-font p-l-16 p-r-16\" col=\"0\" row=\"1\" text=\"รอพบพยาบาล\" borderRadius=\"8\" backgroundColor=\"#4BAEA0\" color=\"white\" tap=\"onClicked\" service=\"nurseclosetime\" fontSize=\"18\"></button>\n        </GridLayout>\n    </StackLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/patient/patient-modal.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ })

},[["./app.js","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLiBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvYXBwXFwuKGNzc3xzY3NzfGxlc3N8c2FzcykkIiwid2VicGFjazovLy9cXGJfW1xcdy1dKlxcLilzY3NzKSQiLCJ3ZWJwYWNrOi8vLy4vYXBwLXJvb3QueG1sIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLmpzIiwid2VicGFjazovLy8uL2NvbmZpZ3MuanMiLCJ3ZWJwYWNrOi8vLy4vbGl2ZVVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9kb2N0b3IvZG9jdG9yLW1vZGFsLmNzcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9kb2N0b3IvZG9jdG9yLW1vZGFsLmpzIiwid2VicGFjazovLy8uL3ZpZXdzL2RvY3Rvci9kb2N0b3ItbW9kYWwueG1sIiwid2VicGFjazovLy8uL3ZpZXdzL21haW4vbWFpbi1wYWdlLW1vZGVsLmpzIiwid2VicGFjazovLy8uL3ZpZXdzL21haW4vbWFpbi1wYWdlLmNzcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9tYWluL21haW4tcGFnZS5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9tYWluL21haW4tcGFnZS54bWwiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS5jc3MiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLnhtbCIsIndlYnBhY2s6Ly8vLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuY3NzIiwid2VicGFjazovLy8uL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwueG1sIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpRTs7Ozs7OztBQ3ZCQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxnQkFBZ0I7QUFDaEI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlKOzs7Ozs7OztBQ3ZDQSw2RTtBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHlDQUF5QztBQUNwRSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNSQSwrR0FBaUUsbUJBQU8sQ0FBQyxrREFBa0M7QUFDM0csZ0VBQWdFLG1CQUFPLENBQUMsa0RBQWtDO0FBQzFHLG9FQUFvRSxtQkFBTyxDQUFDLHFEQUFxQztBQUNqSCxtRUFBbUUsbUJBQU8sQ0FBQyxxREFBcUMsR0FBRyxrQkFBa0Isa0NBQWtDLFVBQVUsa2pCQUFrakIsRUFBRSxpRUFBaUUsRUFBRSxvRUFBb0UsRUFBRSwySkFBMkosRUFBRSx5SUFBeUksRUFBRSxvREFBb0QseURBQXlELEVBQUUsRUFBRSwwREFBMEQsb0ZBQW9GLEVBQUUsRUFBRSxvREFBb0Qsa0dBQWtHLEVBQUUsNERBQTRELEVBQUUsMkNBQTJDLEdBQUcsRUFBRSxFQUFFLG9EQUFvRCxvR0FBb0csRUFBRSw0REFBNEQsRUFBRSwyQ0FBMkMsR0FBRyxFQUFFLEVBQUUsdURBQXVELHdEQUF3RCxFQUFFLDhEQUE4RCxFQUFFLG1GQUFtRixFQUFFLHlEQUF5RCxFQUFFLEVBQUUsNkRBQTZELG1GQUFtRixFQUFFLHlEQUF5RCxFQUFFLHdCO0FBQzM3RSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixtQ0FBbUM7QUFDOUQsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ1RBLHVDQUF1QyxtQkFBTyxDQUFDLCtEQUE4QjtBQUM3RTtBQUNBLFlBQVksbUJBQU8sQ0FBQyxzREFBMkI7QUFDL0MsbUJBQU8sQ0FBQyx5REFBb0M7QUFDNUM7OztBQUdBLFlBQVksbUJBQU8sQ0FBQywwRUFBdUQ7OztBQUczRSxZQUFZLElBQVU7QUFDdEIsOEJBQThCLG1CQUFPLENBQUMsdURBQThCO0FBQ3BFOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLDBDQUEwQyxhQUFhLEtBQUs7QUFDNUQ7QUFDQTtBQUNBLGtEQUFrRCxhQUFhO0FBQy9ELGlCQUFpQjtBQUNqQjs7QUFFQTtBQUNBO0FBQ0E7O0FBRUEsNEJBQTRCLHlKQUFrSTtBQUM5SjtBQUNBLGdCQUFnQixJQUFVO0FBQzFCLHFEO0FBQ0EsbUZBQW1GLFFBQVMsUTtBQUM1RixpQkFBaUI7QUFDakI7O0FBRUEsUUFBUSxtQkFBTyxDQUFDLDJEQUFzQztBQUN0RCwwQkFBMEIsbUJBQU8sQ0FBQywrREFBOEI7QUFDaEUsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDO0FBQ25FLG1CQUFtQixtQkFBTyxDQUFDLGlCQUFjO0FBQ3pDLGNBQWMsbUJBQU8sQ0FBQyxjQUFXOztBQUVqQzs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUIsOEJBQThCO0FBQ25ELENBQUM7QUFDRCxxQkFBcUIseUJBQXlCO0FBQzlDO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLG1DQUFtQztBQUM5RCxLQUFLO0FBQ0wsQzs7Ozs7Ozs7Ozs7O0FDN0RBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsdUNBQXVDO0FBQ2xFLEtBQUs7QUFDTCxDOzs7Ozs7OztBQzVCQSxrRUFBb0IsbUJBQU8sQ0FBQywrREFBOEI7QUFDMUQsZ0JBQWdCLG1CQUFPLENBQUMsbURBQXVCO0FBQy9DLG9CQUFvQixtQkFBTyxDQUFDLG1EQUF1QjtBQUNuRCxvQkFBb0IsbUJBQU8sQ0FBQyxtREFBdUI7QUFDbkQsaUJBQWlCLG1CQUFPLENBQUMseURBQTJCOztBQUVwRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsOENBQThDO0FBQzlDLGFBQWE7QUFDYjtBQUNBO0FBQ0EsVUFBVTtBQUNWLEtBQUs7QUFDTDs7QUFFQTtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwwQ0FBMEM7QUFDckUsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7Ozs7OztBQ2pEQSxnRUFBa0Isa0NBQWtDLGlDO0FBQ3BELElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHlEQUF5RDtBQUNwRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNQQSx1RUFBeUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDbkUsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDOztBQUVuRTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIseURBQXlEO0FBQ3BGLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7QUNwQkEsMFJBQTBSLHduQjtBQUMxUixJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwwREFBMEQ7QUFDckYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDUkEsaUVBQW1CLG1CQUFPLENBQUMsa0VBQWtDO0FBQzdELGFBQWEsbUJBQU8sQ0FBQyxpREFBdUI7O0FBRTVDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwwREFBMEQ7QUFDckYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDL1FBLGdFQUFrQixrQ0FBa0MsVUFBVSx3REFBd0QsbUVBQW1FLEVBQUUsMkRBQTJELEVBQUUsRUFBRSx5RUFBeUUsMkRBQTJELEVBQUUsbUVBQW1FLEVBQUUseURBQXlELEVBQUUsRUFBRSxnRkFBZ0YsMERBQTBELEVBQUUsdUVBQXVFLEVBQUUseURBQXlELEVBQUUsRUFBRSxnRUFBZ0Usa0ZBQWtGLEVBQUUseURBQXlELEVBQUUsRUFBRSwyREFBMkQsbUZBQW1GLEVBQUUseURBQXlELEVBQUUsRUFBRSxvRUFBb0UsNkRBQTZELEVBQUUscURBQXFELEdBQUcsRUFBRSxnR0FBZ0csRUFBRSx3RUFBd0UsRUFBRSxrRUFBa0UsRUFBRSxvRUFBb0UsRUFBRSwwREFBMEQsRUFBRSxFQUFFLGlFQUFpRSw0Q0FBNEMsb0JBQW9CLHFCQUFxQixHQUFHLEVBQUUsb0VBQW9FLEVBQUUscUVBQXFFLEVBQUUsMENBQTBDLEdBQUcsRUFBRSxFQUFFLDhEQUE4RCw0Q0FBNEMsb0JBQW9CLHFCQUFxQixzQkFBc0IsR0FBRyxFQUFFLDJDQUEyQyxvQkFBb0IscUJBQXFCLEdBQUcsRUFBRSw2REFBNkQsRUFBRSxtRUFBbUUsRUFBRSxFQUFFLDhEQUE4RCwwREFBMEQsRUFBRSw4REFBOEQsRUFBRSxxRUFBcUUsRUFBRSxFQUFFLCtEQUErRCw2REFBNkQsRUFBRSx1REFBdUQsRUFBRSx3RUFBd0UsRUFBRSx3REFBd0QsRUFBRSx3QjtBQUN2Z0csSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsb0RBQW9EO0FBQy9FLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1BBLDJEQUFhLG1CQUFPLENBQUMseURBQStCO0FBQ3BELHNCQUFzQixtQkFBTyxDQUFDLGlDQUFtQjtBQUNqRCxtQkFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0Qsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDO0FBQ25FLGlCQUFpQixtQkFBTyxDQUFDLHlEQUEyQjtBQUNwRCxnQkFBZ0IsbUJBQU8sQ0FBQywwREFBNkI7QUFDckQsaUJBQWlCLG1CQUFPLENBQUMsOENBQXFCOztBQUU5QztBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaURBQWlEO0FBQ2pELFFBQVE7QUFDUixpREFBaUQ7QUFDakQ7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLDZCQUE2QjtBQUM3QjtBQUNBLHFCQUFxQjtBQUNyQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYixLQUFLO0FBQ0w7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EseUJBQXlCO0FBQ3pCO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCOztBQUVyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7O0FBRXJCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjs7QUFFckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCOztBQUVyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7O0FBRXJCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjs7QUFFckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCOztBQUVyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7O0FBRXJCO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjs7QUFFckI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCOztBQUVyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsb0RBQW9EO0FBQy9FLEtBQUs7QUFDTCxDOzs7Ozs7OztBQy90QkEsNEdBQThELFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFO0FBQzVHLDBFQUEwRSxRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTtBQUN4SCx1RkFBdUYsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7O0FBRXJJLCtZQUErWSxrMkZBQWsyRixZQUFZLG1jQUFtYyxhQUFhLHlPQUF5TyxPQUFPLG1PQUFtTyxLQUFLLHlKQUF5SixTQUFTLHVCQUF1QixjQUFjLHlMQUF5TCxRQUFRLG95RUFBb3lFLFlBQVksbWNBQW1jLGFBQWEseU9BQXlPLE9BQU8sbU9BQW1PLEtBQUsseUpBQXlKLFNBQVMsdUJBQXVCLGNBQWMseUxBQXlMLFFBQVEscXlFQUFxeUUsWUFBWSxvY0FBb2MsYUFBYSx5T0FBeU8sT0FBTyxtT0FBbU8sS0FBSyx5SkFBeUosU0FBUyx1QkFBdUIsY0FBYyx5TEFBeUwsUUFBUSxveUVBQW95RSxZQUFZLG9jQUFvYyxhQUFhLHlPQUF5TyxPQUFPLG1PQUFtTyxLQUFLLHlKQUF5SixTQUFTLHVCQUF1QixjQUFjLHlMQUF5TCxRQUFRLG95RUFBb3lFLFlBQVksb2NBQW9jLGFBQWEseU9BQXlPLE9BQU8sbU9BQW1PLEtBQUsseUpBQXlKLFNBQVMsdUJBQXVCLGNBQWMseUxBQXlMLFFBQVEseWxCQUF5bEIsb1BBQW9QLHloQkFBeWhCLFNBQVMsd29CQUF3b0Isc1Y7QUFDeDZwQixJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixxREFBcUQ7QUFDaEYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDWEEsZ0VBQWtCLGtDQUFrQyxVQUFVLGlFQUFpRSxzR0FBc0csR0FBRyxFQUFFLHdDQUF3QyxHQUFHLEVBQUUsMENBQTBDLEdBQUcsRUFBRSx5REFBeUQsRUFBRSwwREFBMEQsRUFBRSwyREFBMkQsRUFBRSw0Q0FBNEMsc0JBQXNCLEdBQUcsRUFBRSw4Q0FBOEMsR0FBRyxFQUFFLDREQUE0RCxFQUFFLG1FQUFtRSxFQUFFLDBDQUEwQyxHQUFHLEVBQUUsd0I7QUFDdHlCLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHNEQUFzRDtBQUNqRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNQQSx1RUFBeUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDbkUsc0JBQXNCLG1CQUFPLENBQUMsaUNBQXlCO0FBQ3ZELG9CQUFvQixtQkFBTyxDQUFDLGlGQUF1Qzs7QUFFbkU7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNULEtBQUs7QUFDTDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsc0RBQXNEO0FBQ2pGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ2pGQSw0R0FBOEQsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7QUFDNUcsMEVBQTBFLFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFO0FBQ3hILHVGQUF1RixRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTtBQUNySSw2RkFBNkYsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7QUFDM0ksaUZBQWlGLFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFOztBQUUvSCxzVEFBc1QsbUlBQW1JLFlBQVksZ09BQWdPLDBJQUEwSSxhQUFhLG9JQUFvSSwwSUFBMEksYUFBYSxzSUFBc0ksY0FBYyxtSUFBbUksZ2hCQUFnaEIseUlBQXlJLFlBQVksMENBQTBDLDZGQUE2RixZQUFZLDBDQUEwQyxvTDtBQUNodEUsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsdURBQXVEO0FBQ2xGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ2JBLGdFQUFrQixrQ0FBa0MsaUM7QUFDcEQsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsMkRBQTJEO0FBQ3RGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1BBLHVFQUF5QixtQkFBTyxDQUFDLGtFQUFrQztBQUNuRSxvQkFBb0IsbUJBQU8sQ0FBQyxpRkFBdUM7O0FBRW5FO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwyREFBMkQ7QUFDdEYsS0FBSztBQUNMLEM7Ozs7Ozs7OztBQ25EQSxvUEFBb1AsSUFBSSw2REFBNkQsTUFBTSxxTUFBcU0sbVJBQW1SLGFBQWEsMFFBQTBRLDBRQUEwUSxXQUFXLHFSQUFxUixrSkFBa0osV0FBVyxzcEI7QUFDanZELElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDREQUE0RDtBQUN2RixLQUFLO0FBQ0wsQyIsImZpbGUiOiJidW5kbGUuanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgbWFwID0ge1xuXHRcIi4vYXBwLmNzc1wiOiBcIi4vYXBwLmNzc1wiXG59O1xuXG5cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0KHJlcSkge1xuXHR2YXIgaWQgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmUocmVxKTtcblx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oaWQpO1xufVxuZnVuY3Rpb24gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSkge1xuXHR2YXIgaWQgPSBtYXBbcmVxXTtcblx0aWYoIShpZCArIDEpKSB7IC8vIGNoZWNrIGZvciBudW1iZXIgb3Igc3RyaW5nXG5cdFx0dmFyIGUgPSBuZXcgRXJyb3IoXCJDYW5ub3QgZmluZCBtb2R1bGUgJ1wiICsgcmVxICsgXCInXCIpO1xuXHRcdGUuY29kZSA9ICdNT0RVTEVfTk9UX0ZPVU5EJztcblx0XHR0aHJvdyBlO1xuXHR9XG5cdHJldHVybiBpZDtcbn1cbndlYnBhY2tDb250ZXh0LmtleXMgPSBmdW5jdGlvbiB3ZWJwYWNrQ29udGV4dEtleXMoKSB7XG5cdHJldHVybiBPYmplY3Qua2V5cyhtYXApO1xufTtcbndlYnBhY2tDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrQ29udGV4dFJlc29sdmU7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tDb250ZXh0O1xud2VicGFja0NvbnRleHQuaWQgPSBcIi4vIHN5bmMgXlxcXFwuXFxcXC9hcHBcXFxcLihjc3N8c2Nzc3xsZXNzfHNhc3MpJFwiOyIsInZhciBtYXAgPSB7XG5cdFwiLi9hcHAtcm9vdC54bWxcIjogXCIuL2FwcC1yb290LnhtbFwiLFxuXHRcIi4vYXBwLmNzc1wiOiBcIi4vYXBwLmNzc1wiLFxuXHRcIi4vYXBwLmpzXCI6IFwiLi9hcHAuanNcIixcblx0XCIuL2NvbmZpZ3MuanNcIjogXCIuL2NvbmZpZ3MuanNcIixcblx0XCIuL2xpdmVVcGRhdGUuanNcIjogXCIuL2xpdmVVcGRhdGUuanNcIixcblx0XCIuL3ZpZXdzL2RvY3Rvci9kb2N0b3ItbW9kYWwuY3NzXCI6IFwiLi92aWV3cy9kb2N0b3IvZG9jdG9yLW1vZGFsLmNzc1wiLFxuXHRcIi4vdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC5qc1wiOiBcIi4vdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC5qc1wiLFxuXHRcIi4vdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC54bWxcIjogXCIuL3ZpZXdzL2RvY3Rvci9kb2N0b3ItbW9kYWwueG1sXCIsXG5cdFwiLi92aWV3cy9tYWluL21haW4tcGFnZS1tb2RlbC5qc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UtbW9kZWwuanNcIixcblx0XCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLmNzc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuY3NzXCIsXG5cdFwiLi92aWV3cy9tYWluL21haW4tcGFnZS5qc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuanNcIixcblx0XCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLnhtbFwiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UueG1sXCIsXG5cdFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmNzc1wiOiBcIi4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS5jc3NcIixcblx0XCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIjogXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIixcblx0XCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UueG1sXCI6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLnhtbFwiLFxuXHRcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmNzc1wiOiBcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmNzc1wiLFxuXHRcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmpzXCI6IFwiLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuanNcIixcblx0XCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC54bWxcIjogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC54bWxcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0dmFyIGlkID0gbWFwW3JlcV07XG5cdGlmKCEoaWQgKyAxKSkgeyAvLyBjaGVjayBmb3IgbnVtYmVyIG9yIHN0cmluZ1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gaWQ7XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuLyBzeW5jIHJlY3Vyc2l2ZSAoPzwhXFxcXGJBcHBfUmVzb3VyY2VzXFxcXGIuKikoPzwhXFxcXC5cXFxcL1xcXFxidGVzdHNcXFxcYlxcXFwvLio/KVxcXFwuKHhtbHxjc3N8anN8a3R8KD88IVxcXFwuZFxcXFwuKXRzfCg/PCFcXFxcYl9bXFxcXHctXSpcXFxcLilzY3NzKSRcIjsiLCJcbm1vZHVsZS5leHBvcnRzID0gXCI8RnJhbWUgZGVmYXVsdFBhZ2U9XFxcIi92aWV3cy9tYWluL21haW4tcGFnZVxcXCI+XFxuPC9GcmFtZT5cXG5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9hcHAtcm9vdC54bWxcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwibWFya3VwXCIsIHBhdGg6IFwiLi9hcHAtcm9vdC54bWxcIiB9KTtcbiAgICB9KTtcbn0gIiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwifkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2NvcmUuY3NzXCIsICgpID0+IHJlcXVpcmUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiKSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIikpO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwifkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2RlZmF1bHQuY3NzXCIsICgpID0+IHJlcXVpcmUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9kZWZhdWx0LmNzc1wiKSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9kZWZhdWx0LmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcIikpO21vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIlxcbkluIE5hdGl2ZVNjcmlwdCwgdGhlIGFwcC5jc3MgZmlsZSBpcyB3aGVyZSB5b3UgcGxhY2UgQ1NTIHJ1bGVzIHRoYXRcXG55b3Ugd291bGQgbGlrZSB0byBhcHBseSB0byB5b3VyIGVudGlyZSBhcHBsaWNhdGlvbi4gQ2hlY2sgb3V0XFxuaHR0cDovL2RvY3MubmF0aXZlc2NyaXB0Lm9yZy91aS9zdHlsaW5nIGZvciBhIGZ1bGwgbGlzdCBvZiB0aGUgQ1NTXFxuc2VsZWN0b3JzIGFuZCBwcm9wZXJ0aWVzIHlvdSBjYW4gdXNlIHRvIHN0eWxlIFVJIGNvbXBvbmVudHMuXFxuXFxuLypcXG5JbiBtYW55IGNhc2VzIHlvdSBtYXkgd2FudCB0byB1c2UgdGhlIE5hdGl2ZVNjcmlwdCBjb3JlIHRoZW1lIGluc3RlYWRcXG5vZiB3cml0aW5nIHlvdXIgb3duIENTUyBydWxlcy4gWW91IGNhbiBsZWFybiBtb3JlIGFib3V0IHRoZSBcXG5OYXRpdmVTY3JpcHQgY29yZSB0aGVtZSBhdCBodHRwczovL2dpdGh1Yi5jb20vbmF0aXZlc2NyaXB0L3RoZW1lXFxuVGhlIGltcG9ydGVkIENTUyBydWxlcyBtdXN0IHByZWNlZGUgYWxsIG90aGVyIHR5cGVzIG9mIHJ1bGVzLlxcblwifSx7XCJ0eXBlXCI6XCJpbXBvcnRcIixcImltcG9ydFwiOlwiXFxcIn5AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1xcXCJcIn0se1widHlwZVwiOlwiaW1wb3J0XCIsXCJpbXBvcnRcIjpcIlxcXCJ+QG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcXFwiXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBQbGFjZSBhbnkgQ1NTIHJ1bGVzIHlvdSB3YW50IHRvIGFwcGx5IG9uIGJvdGggaU9TIGFuZCBBbmRyb2lkIGhlcmUuXFxuVGhpcyBpcyB3aGVyZSB0aGUgdmFzdCBtYWpvcml0eSBvZiB5b3VyIENTUyBjb2RlIGdvZXMuIFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCJcXG5UaGUgZm9sbG93aW5nIENTUyBydWxlIGNoYW5nZXMgdGhlIGZvbnQgc2l6ZSBvZiBhbGwgQnV0dG9ucyB0aGF0IGhhdmUgdGhlXFxuXFxcIi1wcmltYXJ5XFxcIiBjbGFzcyBtb2RpZmllci5cXG5cIn0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJ0blwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE4XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnRleHQtZm9udFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LU1lZGl1bVxcXCJcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuZmFzXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1mYW1pbHlcIixcInZhbHVlXCI6XCJcXFwiRm9udCBBd2Vzb21lIDUgRnJlZVxcXCIsIFxcXCJmYS1zb2xpZC05MDBcXFwiXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCI5MDBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGZvbnQtc2l6ZTogMTA7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5mYXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJGb250IEF3ZXNvbWUgNSBGcmVlXFxcIiwgXFxcImZhLXJlZ3VsYXItNDAwXFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiNDAwXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBmb250LXNpemU6IDEwOyBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaGVhZGVyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5ub2RhdGEtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIyMlwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9hcHAuY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi9hcHAuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsIlxuICAgICAgICBsZXQgYXBwbGljYXRpb25DaGVja1BsYXRmb3JtID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIik7XG4gICAgICAgIGlmIChhcHBsaWNhdGlvbkNoZWNrUGxhdGZvcm0uYW5kcm9pZCAmJiAhZ2xvYmFsW1wiX19zbmFwc2hvdFwiXSkge1xuICAgICAgICAgICAgcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWVcIik7XG5yZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy91aS9mcmFtZS9hY3Rpdml0eVwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIFxuICAgICAgICAgICAgcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1kZXYtd2VicGFjay9sb2FkLWFwcGxpY2F0aW9uLWNzcy1yZWd1bGFyXCIpKCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICBpZiAobW9kdWxlLmhvdCkge1xuICAgICAgICAgICAgY29uc3QgaG1yVXBkYXRlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1kZXYtd2VicGFjay9obXJcIikuaG1yVXBkYXRlO1xuICAgICAgICAgICAgZ2xvYmFsLl9fY29yZU1vZHVsZXNMaXZlU3luYyA9IGdsb2JhbC5fX29uTGl2ZVN5bmM7XG5cbiAgICAgICAgICAgIGdsb2JhbC5fX29uTGl2ZVN5bmMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlIGhvdCB1cGRhdGVkIG9uIExpdmVTeW5jXG4gICAgICAgICAgICAgICAgaG1yVXBkYXRlKCk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCA9IGZ1bmN0aW9uKHsgdHlwZSwgcGF0aCB9ID0ge30pIHtcbiAgICAgICAgICAgICAgICAvLyB0aGUgaG90IHVwZGF0ZXMgYXJlIGFwcGxpZWQsIGFzayB0aGUgbW9kdWxlcyB0byBhcHBseSB0aGUgY2hhbmdlc1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBnbG9iYWwuX19jb3JlTW9kdWxlc0xpdmVTeW5jKHsgdHlwZSwgcGF0aCB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZSBob3QgdXBkYXRlZCBvbiBpbml0aWFsIGFwcCBzdGFydFxuICAgICAgICAgICAgaG1yVXBkYXRlKCk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBjb250ZXh0ID0gcmVxdWlyZS5jb250ZXh0KFwifi9cIiwgdHJ1ZSwgLyg/PCFcXGJBcHBfUmVzb3VyY2VzXFxiLiopKD88IVxcLlxcL1xcYnRlc3RzXFxiXFwvLio/KVxcLih4bWx8Y3NzfGpzfGt0fCg/PCFcXC5kXFwuKXRzfCg/PCFcXGJfW1xcdy1dKlxcLilzY3NzKSQvKTtcbiAgICAgICAgICAgIGdsb2JhbC5yZWdpc3RlcldlYnBhY2tNb2R1bGVzKGNvbnRleHQpO1xuICAgICAgICAgICAgaWYgKG1vZHVsZS5ob3QpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuaG90LmFjY2VwdChjb250ZXh0LmlkLCAoKSA9PiB7IFxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkhNUjogQWNjZXB0IG1vZHVsZSAnXCIgKyBjb250ZXh0LmlkICsgXCInIGZyb20gJ1wiICsgbW9kdWxlLmlkICsgXCInXCIpOyBcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9idW5kbGUtZW50cnktcG9pbnRzXCIpO1xuICAgICAgICBsZXQgYXBwbGljYXRpb24gPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiKTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCBMaXZlVXBkYXRlID0gcmVxdWlyZShcIi4vbGl2ZVVwZGF0ZVwiKTtcbmdsb2JhbC5jb25mID0gcmVxdWlyZShcIi4vY29uZmlnc1wiKTtcblxubmV3IExpdmVVcGRhdGUoZ2xvYmFsLmNvbmYudXBkYXRlS2V5KTtcblxubGV0IHRva2VuID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4vLyBjb25zb2xlLmxvZyhcIlJvb3QtVXNlciBUb2tlbjogXCIgKyB0b2tlbik7XG5pZiAodG9rZW4gIT0gXCJ0b2tlbmV4cGlyZWRcIikge1xuICAgIGFwcGxpY2F0aW9uLnJ1bih7IG1vZHVsZU5hbWU6IFwiYXBwLXJvb3QtbWFpblwiIH0pO1xufSBlbHNlIHtcbiAgICBhcHBsaWNhdGlvbi5ydW4oeyBtb2R1bGVOYW1lOiBcImFwcC1yb290XCIgfSk7XG59XG47IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vYXBwLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vYXBwLmpzXCIgfSk7XG4gICAgfSk7XG59IFxuICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgIiwiY29uZmlnID0ge1xuICAgIG5hbWU6IFwiQ2xpbmljIFF1ZXVlXCIsXG4gICAgdmVyOiBcIlZlcnNpb24gMS4wIChidWlsZCAwMTA3MjApXCIsXG5cbiAgICBhcGk6IHtcbiAgICAgICAgaXNkb2N0b3I6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9jbGluaWNxdWV1ZS9kb2N0b3IvaXNkb2N0b3JcIixcbiAgICAgICAgZG9jdG9yY2xpbmljOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvY2xpbmljcXVldWUvZG9jdG9yL2NsaW5pY1wiLFxuICAgICAgICBkb2N0b3JxdWV1ZTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL2RvY3Rvci9xdWV1ZVwiLFxuICAgICAgICBwYXRpZW50aW5mbzogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL3BhdGllbnQvaW5mb1wiLFxuICAgICAgICBwYXRpZW50dGltZTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL3BhdGllbnQvdGltZVwiLFxuICAgICAgICB1cGRhdGVwYXRpZW50dGltZTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL3BhdGllbnQvdGltZXVwZGF0ZVwiLFxuICAgICAgICBpbnNlcnRwYXRpZW50dGltZTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL3BhdGllbnQvdGltZWluc2VydFwiLFxuXG4gICAgfSxcblxuICAgIHVwZGF0ZUtleToge1xuICAgICAgICBhbmRyb2lkOiBcImMwcHhzYTREQmwxaXBaSkhHNmhqaHo5NExTd1pwYlZZaWMwdzVcIlxuICAgIH1cbn07XG5cbm1vZHVsZS5leHBvcnRzID0gY29uZmlnO1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2NvbmZpZ3MuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi9jb25maWdzLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IGFwcGxpY2F0aW9uID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIik7XG5jb25zdCBBcHBTeW5jID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hcHAtc3luY1wiKS5BcHBTeW5jO1xuY29uc3QgSW5zdGFsbE1vZGUgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWFwcC1zeW5jXCIpLkluc3RhbGxNb2RlO1xuY29uc3QgU3luY1N0YXR1cyAgPSByZXF1aXJlKFwibmF0aXZlc2NyaXB0LWFwcC1zeW5jXCIpLlN5bmNTdGF0dXMgO1xuY29uc3QgcGxhdGZvcm0gPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9wbGF0Zm9ybVwiKTtcblxuZnVuY3Rpb24gbGl2ZVVwZGF0ZShjb25maWcpIHtcbiAgICBpZiAocGxhdGZvcm0uaXNJT1MpIHtcbiAgICAgICAgY29uc29sZS5sb2coXCJJT1MgZGV2aWNlIHRva2VuOiBcIiArIGNvbmZpZy5pb3MpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiQW5kcm9pZCBkZXZpY2UgdG9rZW46IFwiICsgY29uZmlnLmFuZHJvaWQpO1xuICAgIH1cbiAgICBhcHBsaWNhdGlvbi5vbihhcHBsaWNhdGlvbi5yZXN1bWVFdmVudCwgZnVuY3Rpb24oKSB7XG4gICAgICAgIEFwcFN5bmMuc3luYyh7XG4gICAgICAgICAgICBlbmFibGVkV2hlblVzaW5nSG1yOiBmYWxzZSxcbiAgICAgICAgICAgIGRlcGxveW1lbnRLZXk6IHBsYXRmb3JtLmlzSU9TID8gY29uZmlnLmlvcyA6IGNvbmZpZy5hbmRyb2lkLFxuICAgICAgICAgICAgaW5zdGFsbE1vZGU6IEluc3RhbGxNb2RlLklNTUVESUFURSxcbiAgICAgICAgICAgIG1hbmRhdG9yeUluc3RhbGxNb2RlOiBwbGF0Zm9ybS5pc0lPU1xuICAgICAgICAgICAgICAgID8gSW5zdGFsbE1vZGUuT05fTkVYVF9SRVNVTUVcbiAgICAgICAgICAgICAgICA6IEluc3RhbGxNb2RlLklNTUVESUFURSxcbiAgICAgICAgICAgIHVwZGF0ZURpYWxvZzoge1xuICAgICAgICAgICAgICAgIG9wdGlvbmFsVXBkYXRlTWVzc2FnZTogXCLguKPguLXguKrguJXguLLguKPguYzguJfguYDguJ7guLfguYjguK3guK3guLHguJ7guYDguJTguJdcIixcbiAgICAgICAgICAgICAgICB1cGRhdGVUaXRsZTogXCLguK3guLHguJ7guYDguJTguJfguYDguKfguK3guKPguYzguIrguLHguJnguYPguKvguKHguYhcIixcbiAgICAgICAgICAgICAgICBtYW5kYXRvcnlVcGRhdGVNZXNzYWdlOiBcIuC4o+C4teC4quC4leC4suC4o+C5jOC4l+C5gOC4nuC4t+C5iOC4reC4reC4seC4nuC5gOC4lOC4l1wiLFxuICAgICAgICAgICAgICAgIG9wdGlvbmFsSWdub3JlQnV0dG9uTGFiZWw6IFwi4Lii4LiB4LmA4Lil4Li04LiBXCIsXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5Q29udGludWVCdXR0b25MYWJlbDogcGxhdGZvcm0uaXNJT1NcbiAgICAgICAgICAgICAgICAgICAgPyBcIuC4m+C4tOC4lFwiXG4gICAgICAgICAgICAgICAgICAgIDogXCLguKPguLXguKrguJXguLLguKPguYzguJdcIixcbiAgICAgICAgICAgICAgICBhcHBlbmRSZWxlYXNlRGVzY3JpcHRpb246IHRydWVcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgIChzeW5jU3RhdHVzKSA9PiB7XG4gICAgICAgICAgICBpZiAoc3luY1N0YXR1cyA9PT0gU3luY1N0YXR1cy5VUF9UT19EQVRFKSB7XG4gICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiTm8gcGVuZGluZyB1cGRhdGVzOyB5b3XigJlyZSBydW5uaW5nIHRoZSBsYXRlc3QgdmVyc2lvbi5cIik7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHN5bmNTdGF0dXMgPT09IFN5bmNTdGF0dXMuVVBEQVRFX0lOU1RBTExFRCkge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIlVwZGF0ZSBmb3VuZCBhbmQgaW5zdGFsbGVkLiBJdCB3aWxsIGJlIGFjdGl2YXRlZCB1cG9uIG5leHQgY29sZCByZWJvb3RcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgICB9KTtcbiAgICB9KTtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBsaXZlVXBkYXRlO1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL2xpdmVVcGRhdGUuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi9saXZlVXBkYXRlLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W10sXCJwYXJzaW5nRXJyb3JzXCI6W119fTs7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL2RvY3Rvci9kb2N0b3ItbW9kYWwuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IG9ic2VydmFibGVNb2R1bGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7XG5jb25zdCBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCIpO1xuXG5sZXQgY2xvc2VDYWxsYmFjaztcbmxldCB0YWI7XG5leHBvcnRzLm9uU2hvd25Nb2RhbGx5ID0gKGFyZ3MpID0+IHtcbiAgIGNsb3NlQ2FsbGJhY2sgPSBhcmdzLmNsb3NlQ2FsbGJhY2s7XG59O1xuXG5leHBvcnRzLm9uQWRkTmV3RG9jdG9yID0gKGFyZ3MpID0+IHtcbiAgICBsZXQgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgbGV0IHBlcmlkID0gcGFnZS5nZXRWaWV3QnlJZChcImRvY3RvcnBlcmlkXCIpO1xuICAgIGNsb3NlQ2FsbGJhY2socGVyaWQudGV4dCk7XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL2RvY3Rvci9kb2N0b3ItbW9kYWwuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9kb2N0b3IvZG9jdG9yLW1vZGFsLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIlxubW9kdWxlLmV4cG9ydHMgPSBcIjxQYWdlIHNob3duTW9kYWxseT1cXFwib25TaG93bk1vZGFsbHlcXFwiXFxuICAgIHhtbG5zOmx2PVxcXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcXFwiPlxcbiAgICA8U3RhY2tMYXlvdXQgd2lkdGg9XFxcIjEwMCVcXFwiPlxcbiAgICAgICAgPEdyaWRMYXlvdXQgY29sdW1ucz1cXFwiYXV0bywgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS1sLTE2IG0tdC0xNlxcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjBcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYwZjA7XFxcIiB3aWR0aD1cXFwiMjBcXFwiIGhlaWdodD1cXFwiMjBcXFwiIGNvbG9yPVxcXCIjNGE3NzRlXFxcIi8+XFxuICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgbS10LTE2IG0tbC04XFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMVxcXCIgZm9udFNpemU9XFxcIjIwXFxcIiB0ZXh0PVxcXCLguKPguLDguJrguLjguKPguKvguLHguKrguYHguJ7guJfguKLguYxcXFwiPjwvTGFiZWw+XFxuICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICA8VGV4dEZpZWxkIGlkPVxcXCJkb2N0b3JwZXJpZFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LWNlbnRlciBtLWItMTYgbS10LThcXFwiIGhpbnQ9XFxcIuC4o+C4q+C4seC4quC5geC4nuC4l+C4ouC5jFxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiBzdHlsZT1cXFwicGxhY2Vob2xkZXItY29sb3I6ICNCQkJCQkJcXFwiIGtleWJvYXJkVHlwZT1cXFwicGhvbmVcXFwiPjwvVGV4dEZpZWxkPlxcbiAgICAgICAgPGJ1dHRvbiBpZD1cXFwiYnRuYWRkZG9jdG9yXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IHAtbC0xNiBwLXItMTYgbS1iLTE2IG0tdC04XFxcIiBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgdGV4dD1cXFwi4Lii4Li34LiZ4Lii4Lix4LiZXFxcIiBib3JkZXJSYWRpdXM9XFxcIjhcXFwiIHRhcD1cXFwib25BZGROZXdEb2N0b3JcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiIzFlNGIyNVxcXCIgY29sb3I9XFxcIndoaXRlXFxcIj48L2J1dHRvbj5cXG4gICAgPC9TdGFja0xheW91dD5cXG48L1BhZ2U+XCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC54bWxcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwibWFya3VwXCIsIHBhdGg6IFwiLi92aWV3cy9kb2N0b3IvZG9jdG9yLW1vZGFsLnhtbFwiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBPYnNlcnZhYmxlID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCIpLk9ic2VydmFibGU7XG5jb25zdCBodHRwID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvaHR0cFwiKTtcblxuZnVuY3Rpb24gQVBJU2VydmljZSgpIHtcbiAgICBsZXQgdmlld01vZGVsID0gbmV3IE9ic2VydmFibGUoKTtcbiAgICBsZXQgcmVzdWx0ID0gbmV3IEFycmF5KCk7XG4gICAgLy8gY29uc29sZS5sb2coZ2xvYmFsLmNvbmYuYXBpLmRvY3RvcmNsaW5pYyk7XG4gICAgdmlld01vZGVsLmdldERvY3RvckluZm8gPSAocGVyaWQpID0+IHtcbiAgICAgICAgcmV0dXJuIGh0dHBcbiAgICAgICAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5pc2RvY3RvcixcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIHBlcmlkOiBwZXJpZCxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSAhPSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogW10sXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiByZXMuZGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIChlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgIH07XG5cbiAgICB2aWV3TW9kZWwuZ2V0Q2xpbmljID0gKHBlcmlkLCBjdXJkYXRlKSA9PiB7XG4gICAgICAgIHJldHVybiBodHRwXG4gICAgICAgICAgICAucmVxdWVzdCh7XG4gICAgICAgICAgICAgICAgdXJsOiBnbG9iYWwuY29uZi5hcGkuZG9jdG9yY2xpbmljLFxuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICAgICAgICAgICAgcGVyaWQ6IHBlcmlkLFxuICAgICAgICAgICAgICAgICAgICBkYXRlX2Q6IGN1cmRhdGUsXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oXG4gICAgICAgICAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzLnN0YXR1c0NvZGUgIT0gMjAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IFtdLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLmRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAoZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgdmlld01vZGVsLmdldERvY3RvclF1ZXVlID0gKGN1cmRhdGUsIGNsaW5pY2NvZGUsIGRvY3RvcnBlcmlkKSA9PiB7XG4gICAgICAgIHJldHVybiBodHRwXG4gICAgICAgICAgICAucmVxdWVzdCh7XG4gICAgICAgICAgICAgICAgdXJsOiBnbG9iYWwuY29uZi5hcGkuZG9jdG9ycXVldWUsXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICBkYXRlX2Q6IGN1cmRhdGUsXG4gICAgICAgICAgICAgICAgICAgIGNvZGVfYjogY2xpbmljY29kZSxcbiAgICAgICAgICAgICAgICAgICAgY19kb2N0OiBkb2N0b3JwZXJpZCxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSAhPSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogW10sXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiByZXMuZGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIChlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgIH07XG5cbiAgICB2aWV3TW9kZWwuZ2V0UGF0aWVudEluZm8gPSAoaG4pID0+IHtcbiAgICAgICAgcmV0dXJuIGh0dHBcbiAgICAgICAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5wYXRpZW50aW5mbyxcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIGhuOiBobixcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuc3RhdHVzQ29kZSAhPSAyMDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogW10sXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiByZXMuZGF0YSxcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIChlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGUpO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeShlKSxcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICApO1xuICAgIH07XG5cbiAgICB2aWV3TW9kZWwuY2hlY2tQYXRpZW50VGltZSA9IChwYXRpZW50aW5mbykgPT4ge1xuICAgICAgICBsZXQgY3VycmVudGRhdGUgPSBwYXRpZW50aW5mby5kYXRlO1xuICAgICAgICBsZXQgY2xpbmljQ29kZSA9IHBhdGllbnRpbmZvLmNvZGVfYjtcbiAgICAgICAgbGV0IGRvY3RvclBlcmlkID0gcGF0aWVudGluZm8uY19kb2N0O1xuICAgICAgICBsZXQgaG4gPSBwYXRpZW50aW5mby5objtcbiAgICAgICAgcmV0dXJuIGh0dHBcbiAgICAgICAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5wYXRpZW50dGltZSxcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIGRhdGVfZDogY3VycmVudGRhdGUsXG4gICAgICAgICAgICAgICAgICAgIGNvZGVfYjogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgY19kb2N0OiBkb2N0b3JQZXJpZCxcbiAgICAgICAgICAgICAgICAgICAgaG46IGhuLFxuICAgICAgICAgICAgICAgIH0pLFxuICAgICAgICAgICAgfSlcbiAgICAgICAgICAgIC50aGVuKFxuICAgICAgICAgICAgICAgIChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBsZXQgcmVzID0gcmVzcG9uc2UuY29udGVudC50b0pTT04oKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogMjAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uOiByZXMuYWN0aW9uLFxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDQwMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjdGlvbjogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgdmlld01vZGVsLnVwZGF0ZVBhdGllbnRUaW1lID0gKHBhdGllbnRpbmZvKSA9PiB7XG4gICAgICAgIGxldCB1cGRhdGVUaW1lVVJMID0gXCJcIjtcbiAgICAgICAgc3dpdGNoIChwYXRpZW50aW5mby5hY3Rpb24pIHtcbiAgICAgICAgICAgIGNhc2UgXCJ1cGRhdGVcIjpcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcInVwZGF0ZWRcIik7XG4gICAgICAgICAgICAgICAgdXBkYXRlVGltZVVSTCA9IGdsb2JhbC5jb25mLmFwaS51cGRhdGVwYXRpZW50dGltZTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJpbnNlcnRcIjpcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImluc2VydFwiKTtcbiAgICAgICAgICAgICAgICB1cGRhdGVUaW1lVVJMID0gZ2xvYmFsLmNvbmYuYXBpLmluc2VydHBhdGllbnR0aW1lO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuXG4gICAgICAgIGxldCBobiA9IHBhdGllbnRpbmZvLmhuO1xuICAgICAgICBsZXQgY3VycmVudGRhdGUgPSBwYXRpZW50aW5mby5kYXRlO1xuICAgICAgICBsZXQgY2xpbmljQ29kZSA9IHBhdGllbnRpbmZvLmNvZGVfYjtcbiAgICAgICAgbGV0IGRvY3RvclBlcmlkID0gcGF0aWVudGluZm8uY19kb2N0O1xuXG4gICAgICAgIHJldHVybiBodHRwXG4gICAgICAgICAgICAucmVxdWVzdCh7XG4gICAgICAgICAgICAgICAgdXJsOiB1cGRhdGVUaW1lVVJMLFxuICAgICAgICAgICAgICAgIG1ldGhvZDogXCJQT1NUXCIsXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICBcIkNvbnRlbnQtVHlwZVwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcbiAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgIGNvbnRlbnQ6IEpTT04uc3RyaW5naWZ5KHtcbiAgICAgICAgICAgICAgICAgICAgaG46IGhuLFxuICAgICAgICAgICAgICAgICAgICBkYXRlX2Q6IGN1cnJlbnRkYXRlLFxuICAgICAgICAgICAgICAgICAgICBjb2RlX2I6IGNsaW5pY0NvZGUsXG4gICAgICAgICAgICAgICAgICAgIGNfZG9jdDogZG9jdG9yUGVyaWQsXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oXG4gICAgICAgICAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IHJlcy5kYXRhLFxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDQwMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IEpTT04uc3RyaW5naWZ5KGUpLFxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgfTtcbiAgICByZXR1cm4gdmlld01vZGVsO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBBUElTZXJ2aWNlOiBBUElTZXJ2aWNlLFxufTtcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluL21haW4tcGFnZS1tb2RlbC5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLW1vZGVsLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIlRhYlN0cmlwXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJkYXJrZ3JheVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIlRhYlN0cmlwSXRlbS50YWJzdHJpcGl0ZW1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcImRhcmtncmF5XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXRvcFwiLFwidmFsdWVcIjpcIjRcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCJUYWJTdHJpcEl0ZW0udGFic3RyaXBpdGVtOmFjdGl2ZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiIzFjODQ0NVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJtaW50Y3JlYW1cIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi50ZXh0LW5hbWUtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1Cb2xkXFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE4XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnRleHQtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5jYXJkLWRvY3Rvci1wcm9maWxlXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTsgXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1pbWFnZVwiLFwidmFsdWVcIjpcInVybChcXFwifi9zcmMvaW1nL2RvY3Rvci1jYXJkLnBuZ1xcXCIpXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1yZXBlYXRcIixcInZhbHVlXCI6XCJuby1yZXBlYXRcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLXNpemVcIixcInZhbHVlXCI6XCJjb3ZlclwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtcG9zaXRpb25cIixcInZhbHVlXCI6XCJ0b3BcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIiM0NTQwNThcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubGlzdHZpZXctcGF0aWVudFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgbWFyZ2luLWxlZnQ6IDg7XFxuICBtYXJnaW4tcmlnaHQ6IDg7XFxuICBtYXJnaW4tYm90dG9tOiA4OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMzAgMzAgMCAwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiNmZWU2ZDJcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGNvbG9yOiB3aGl0ZTsgXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxpc3R2aWV3LWl0ZW1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi10b3A6IDE2O1xcbiAgbWFyZ2luLWxlZnQ6IDE2O1xcbiAgbWFyZ2luLXJpZ2h0OiAxNjtcXG4gIG1hcmdpbi1ib3R0b206IDE2OyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi10b3A6OCA7XFxuICBtYXJnaW4tbGVmdDogMTY7XFxuICBtYXJnaW4tcmlnaHQ6IDE2OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMjBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucGF0aWVudC1pbWFnZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1sZWZ0XCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCI1MCVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiY3JpbXNvblwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5hZGQtZG9jdG9yLWJ0blwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInBhZGRpbmdcIixcInZhbHVlXCI6XCIxNlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJkYXJrb3JhbmdlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluL21haW4tcGFnZS5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCB2aWV3ID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvY29yZS92aWV3XCIpO1xuY29uc3QgTWFpblBhZ2VNb2RlbCA9IHJlcXVpcmUoXCIuL21haW4tcGFnZS1tb2RlbFwiKTtcbmNvbnN0IGZyb21PYmplY3QgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIikuZnJvbU9iamVjdDtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCBwbGF0Zm9ybSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCIpO1xuY29uc3QgZGlhbG9ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIik7XG5jb25zdCBuc3RvYXN0cyA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdG9hc3RzXCIpO1xuXG5jb25zdCBtb2RhbFZpZXdNb2R1bGUgPSBcIi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmpzXCI7XG5jb25zdCBkb2N0b3JWaWV3TW9kdWxlID0gXCIvdmlld3MvZG9jdG9yL2RvY3Rvci1tb2RhbC5qc1wiO1xuY29uc3QgcGF0aWVudFZpZXdNb2R1bGUgPSBcIi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuanNcIjtcblxubGV0IGFwaXNlcnZpY2UgPSBNYWluUGFnZU1vZGVsLkFQSVNlcnZpY2UoKTtcbmxldCBwYWdlO1xubGV0IG1vZGFsVmlldztcblxuLy8gRG9jdG9yMVxubGV0IGRhdGExID0gW107XG5sZXQgYnRuRG9jdG9yMTtcbmxldCBkb2N0b3IxO1xuXG4vLyBEb2N0b3IyXG5sZXQgZGF0YTIgPSBbXTtcbmxldCBidG5Eb2N0b3IyO1xubGV0IGRvY3RvcjI7XG5cbi8vIERvY3RvcjNcbmxldCBkYXRhMyA9IFtdO1xubGV0IGJ0bkRvY3RvcjM7XG5sZXQgZG9jdG9yMztcblxuLy8gRG9jdG9yNFxubGV0IGRhdGE0ID0gW107XG5sZXQgYnRuRG9jdG9yNDtcbmxldCBkb2N0b3I0O1xuXG4vLyBEb2N0b3I1XG5sZXQgZGF0YTUgPSBbXTtcbmxldCBidG5Eb2N0b3I1O1xubGV0IGRvY3RvcjU7XG5cbmxldCBjdXJkYXRlID0gZ2V0RGF0ZVN0cmluZyhuZXcgRGF0ZSgpKTtcbi8vIGxldCBjdXJEYXRlU3RyID0gY3VyZGF0ZS55ZWFyICsgXCItXCIgKyBjdXJkYXRlLm1vbnRoICsgXCItXCIgKyBjdXJkYXRlLmRheTtcbmxldCBjdXJEYXRlU3RyID0gXCIyMDIwLTA3LTA1XCI7XG5cbmV4cG9ydHMub25QYWdlTG9hZGVkID0gKGFyZ3MpID0+IHtcbiAgICBwYWdlID0gYXJncy5vYmplY3QucGFnZTtcbiAgICBtb2RhbFZpZXcgPSBhcmdzLm9iamVjdDtcbiAgICAvLyBuZXdEb2N0b3JCdG4gPSBwYWdlLmdldFZpZXdCeUlkKFwibmV3ZG9jdG9yYnRuXCIpO1xuICAgIC8vIGlmIChwbGF0Zm9ybS5pc0lPUykge1xuICAgIC8vICAgICBuZXdEb2N0b3JCdG4uc3R5bGUgPSBcImZvbnQtc2l6ZTogMThlbTtcIjtcbiAgICAvLyB9IGVsc2Uge1xuICAgIC8vICAgICBuZXdEb2N0b3JCdG4uc3R5bGUgPSBcImZvbnQtc2l6ZTogMTRlbTtcIjtcbiAgICAvLyB9XG59O1xuXG5leHBvcnRzLmFkZERvY3RvcjEgPSAoKSA9PiB7XG4gICAgYnRuRG9jdG9yMSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJidG5Eb2N0b3IxXCIpO1xuICAgIGRvY3RvcjEgPSBwYWdlLmdldFZpZXdCeUlkKFwiZG9jdG9yMVwiKTtcbiAgICBjb25zdCBvcHRpb24gPSB7XG4gICAgICAgIGNsb3NlQ2FsbGJhY2s6IChkb2N0b3JwZXJpZCkgPT4ge1xuICAgICAgICAgICAgaWYgKGRvY3RvcnBlcmlkICE9IFwiXCIpIHtcbiAgICAgICAgICAgICAgICBnZXREb2N0b3JJbmZvKFwibmV3XCIsIFwiMVwiLCBkb2N0b3JwZXJpZCwgY3VyRGF0ZVN0cik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlLFxuICAgICAgICBhbmltYXRlZDogdHJ1ZSxcbiAgICB9O1xuICAgIG1vZGFsVmlldy5zaG93TW9kYWwoZG9jdG9yVmlld01vZHVsZSwgb3B0aW9uKTtcbn07XG5cbmV4cG9ydHMub25QdWxsVG9SZWZyZXNoSW5pdGlhdGVkMSA9ICgpID0+IHtcbiAgICBsZXQgZG9jdG9ycGVyaWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJkb2N0b3IxXCIpO1xuICAgIGxldCBsaXN0VmlldzEgPSBwYWdlLmdldFZpZXdCeUlkKFwibGlzdHZpZXcxXCIpO1xuICAgIGdldERvY3RvckluZm8oXCJyZWxvYWRcIiwgXCIxXCIsIGRvY3RvcnBlcmlkLCBjdXJEYXRlU3RyKTtcbiAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgICAgdGV4dDogXCLguK3guLHguJ7guYDguJTguJfguILguYnguK3guKHguLnguKXguKrguLPguYDguKPguYfguIhcIixcbiAgICAgICAgZHVyYXRpb246IG5zdG9hc3RzLkRVUkFUSU9OLlNIT1JULFxuICAgICAgICBwb3NpdGlvbjogbnN0b2FzdHMuUE9TSVRJT04uQk9UVE9NLCAvL29wdGlvbmFsXG4gICAgfTtcbiAgICBuc3RvYXN0cy5zaG93KG9wdGlvbnMpO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBsaXN0VmlldzEubm90aWZ5UHVsbFRvUmVmcmVzaEZpbmlzaGVkKCk7XG4gICAgfSwgNTAwKTtcbn07XG5cbmV4cG9ydHMuYWRkRG9jdG9yMiA9ICgpID0+IHtcbiAgICBidG5Eb2N0b3IyID0gcGFnZS5nZXRWaWV3QnlJZChcImJ0bkRvY3RvcjJcIik7XG4gICAgZG9jdG9yMiA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJkb2N0b3IyXCIpO1xuICAgIGNvbnN0IG9wdGlvbiA9IHtcbiAgICAgICAgY2xvc2VDYWxsYmFjazogKGRvY3RvcnBlcmlkKSA9PiB7XG4gICAgICAgICAgICBpZiAoZG9jdG9ycGVyaWQgIT0gXCJcIikge1xuICAgICAgICAgICAgICAgIGdldERvY3RvckluZm8oXCJuZXdcIiwgXCIyXCIsIGRvY3RvcnBlcmlkLCBjdXJEYXRlU3RyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZnVsbHNjcmVlbjogZmFsc2UsXG4gICAgICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgIH07XG4gICAgbW9kYWxWaWV3LnNob3dNb2RhbChkb2N0b3JWaWV3TW9kdWxlLCBvcHRpb24pO1xufTtcblxuZXhwb3J0cy5vblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQyID0gKCkgPT4ge1xuICAgIGxldCBwZXJpZCA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImRvY3RvcjJcIik7XG4gICAgbGV0IGxpc3RWaWV3MiA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0dmlldzJcIik7XG4gICAgZ2V0RG9jdG9ySW5mbyhcInJlbG9hZFwiLCBcIjJcIiwgcGVyaWQsIGN1ckRhdGVTdHIpO1xuICAgIGxldCBvcHRpb25zID0ge1xuICAgICAgICB0ZXh0OiBcIuC4reC4seC4nuC5gOC4lOC4l+C4guC5ieC4reC4oeC4ueC4peC4quC4s+C5gOC4o+C5h+C4iFwiLFxuICAgICAgICBkdXJhdGlvbjogbnN0b2FzdHMuRFVSQVRJT04uU0hPUlQsXG4gICAgICAgIHBvc2l0aW9uOiBuc3RvYXN0cy5QT1NJVElPTi5CT1RUT00sIC8vb3B0aW9uYWxcbiAgICB9O1xuICAgIG5zdG9hc3RzLnNob3cob3B0aW9ucyk7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGxpc3RWaWV3Mi5ub3RpZnlQdWxsVG9SZWZyZXNoRmluaXNoZWQoKTtcbiAgICB9LCA1MDApO1xufTtcblxuZXhwb3J0cy5hZGREb2N0b3IzID0gKCkgPT4ge1xuICAgIGJ0bkRvY3RvcjMgPSBwYWdlLmdldFZpZXdCeUlkKFwiYnRuRG9jdG9yM1wiKTtcbiAgICBkb2N0b3IzID0gcGFnZS5nZXRWaWV3QnlJZChcImRvY3RvcjNcIik7XG4gICAgY29uc3Qgb3B0aW9uID0ge1xuICAgICAgICBjbG9zZUNhbGxiYWNrOiAoZG9jdG9ycGVyaWQpID0+IHtcbiAgICAgICAgICAgIGlmIChkb2N0b3JwZXJpZCAhPSBcIlwiKSB7XG4gICAgICAgICAgICAgICAgZ2V0RG9jdG9ySW5mbyhcIm5ld1wiLCBcIjNcIiwgZG9jdG9ycGVyaWQsIGN1ckRhdGVTdHIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcbiAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgfTtcbiAgICBtb2RhbFZpZXcuc2hvd01vZGFsKGRvY3RvclZpZXdNb2R1bGUsIG9wdGlvbik7XG59O1xuXG5leHBvcnRzLm9uUHVsbFRvUmVmcmVzaEluaXRpYXRlZDMgPSAoKSA9PiB7XG4gICAgbGV0IHBlcmlkID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yM1wiKTtcbiAgICBsZXQgbGlzdFZpZXczID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3R2aWV3M1wiKTtcbiAgICBnZXREb2N0b3JJbmZvKFwicmVsb2FkXCIsIFwiM1wiLCBwZXJpZCwgY3VyRGF0ZVN0cik7XG4gICAgbGV0IG9wdGlvbnMgPSB7XG4gICAgICAgIHRleHQ6IFwi4Lit4Lix4Lie4LmA4LiU4LiX4LiC4LmJ4Lit4Lih4Li54Lil4Liq4Liz4LmA4Lij4LmH4LiIXCIsXG4gICAgICAgIGR1cmF0aW9uOiBuc3RvYXN0cy5EVVJBVElPTi5TSE9SVCxcbiAgICAgICAgcG9zaXRpb246IG5zdG9hc3RzLlBPU0lUSU9OLkJPVFRPTSwgLy9vcHRpb25hbFxuICAgIH07XG4gICAgbnN0b2FzdHMuc2hvdyhvcHRpb25zKTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgbGlzdFZpZXczLm5vdGlmeVB1bGxUb1JlZnJlc2hGaW5pc2hlZCgpO1xuICAgIH0sIDUwMCk7XG59O1xuXG5leHBvcnRzLmFkZERvY3RvcjQgPSAoKSA9PiB7XG4gICAgYnRuRG9jdG9yNCA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJidG5Eb2N0b3I0XCIpO1xuICAgIGRvY3RvcjQgPSBwYWdlLmdldFZpZXdCeUlkKFwiZG9jdG9yNFwiKTtcbiAgICBjb25zdCBvcHRpb24gPSB7XG4gICAgICAgIGNsb3NlQ2FsbGJhY2s6IChkb2N0b3JwZXJpZCkgPT4ge1xuICAgICAgICAgICAgaWYgKGRvY3RvcnBlcmlkICE9IFwiXCIpIHtcbiAgICAgICAgICAgICAgICBnZXREb2N0b3JJbmZvKFwibmV3XCIsIFwiNFwiLCBkb2N0b3JwZXJpZCwgY3VyRGF0ZVN0cik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIGZ1bGxzY3JlZW46IGZhbHNlLFxuICAgICAgICBhbmltYXRlZDogdHJ1ZSxcbiAgICB9O1xuICAgIG1vZGFsVmlldy5zaG93TW9kYWwoZG9jdG9yVmlld01vZHVsZSwgb3B0aW9uKTtcbn07XG5cbmV4cG9ydHMub25QdWxsVG9SZWZyZXNoSW5pdGlhdGVkNCA9ICgpID0+IHtcbiAgICBsZXQgcGVyaWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJkb2N0b3I0XCIpO1xuICAgIGxldCBsaXN0VmlldzQgPSBwYWdlLmdldFZpZXdCeUlkKFwibGlzdHZpZXc0XCIpO1xuICAgIGdldERvY3RvckluZm8oXCJyZWxvYWRcIiwgXCI0XCIsIHBlcmlkLCBjdXJEYXRlU3RyKTtcbiAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgICAgdGV4dDogXCLguK3guLHguJ7guYDguJTguJfguILguYnguK3guKHguLnguKXguKrguLPguYDguKPguYfguIhcIixcbiAgICAgICAgZHVyYXRpb246IG5zdG9hc3RzLkRVUkFUSU9OLlNIT1JULFxuICAgICAgICBwb3NpdGlvbjogbnN0b2FzdHMuUE9TSVRJT04uQk9UVE9NLCAvL29wdGlvbmFsXG4gICAgfTtcbiAgICBuc3RvYXN0cy5zaG93KG9wdGlvbnMpO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBsaXN0VmlldzQubm90aWZ5UHVsbFRvUmVmcmVzaEZpbmlzaGVkKCk7XG4gICAgfSwgNTAwKTtcbn07XG5cbmV4cG9ydHMuYWRkRG9jdG9yNSA9ICgpID0+IHtcbiAgICBidG5Eb2N0b3I1ID0gcGFnZS5nZXRWaWV3QnlJZChcImJ0bkRvY3RvcjVcIik7XG4gICAgZG9jdG9yNSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJkb2N0b3I1XCIpO1xuICAgIGNvbnN0IG9wdGlvbiA9IHtcbiAgICAgICAgY2xvc2VDYWxsYmFjazogKGRvY3RvcnBlcmlkKSA9PiB7XG4gICAgICAgICAgICBpZiAoZG9jdG9ycGVyaWQgIT0gXCJcIikge1xuICAgICAgICAgICAgICAgIGdldERvY3RvckluZm8oXCJuZXdcIiwgXCI1XCIsIGRvY3RvcnBlcmlkLCBjdXJEYXRlU3RyKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSxcbiAgICAgICAgZnVsbHNjcmVlbjogZmFsc2UsXG4gICAgICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgIH07XG4gICAgbW9kYWxWaWV3LnNob3dNb2RhbChkb2N0b3JWaWV3TW9kdWxlLCBvcHRpb24pO1xufTtcblxuZXhwb3J0cy5vblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQ1ID0gKCkgPT4ge1xuICAgIGxldCBwZXJpZCA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImRvY3RvcjVcIik7XG4gICAgbGV0IGxpc3RWaWV3NSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0dmlldzVcIik7XG4gICAgZ2V0RG9jdG9ySW5mbyhcInJlbG9hZFwiLCBcIjVcIiwgcGVyaWQsIGN1ckRhdGVTdHIpO1xuICAgIGxldCBvcHRpb25zID0ge1xuICAgICAgICB0ZXh0OiBcIuC4reC4seC4nuC5gOC4lOC4l+C4guC5ieC4reC4oeC4ueC4peC4quC4s+C5gOC4o+C5h+C4iFwiLFxuICAgICAgICBkdXJhdGlvbjogbnN0b2FzdHMuRFVSQVRJT04uU0hPUlQsXG4gICAgICAgIHBvc2l0aW9uOiBuc3RvYXN0cy5QT1NJVElPTi5CT1RUT00sIC8vb3B0aW9uYWxcbiAgICB9O1xuICAgIG5zdG9hc3RzLnNob3cob3B0aW9ucyk7XG4gICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgIGxpc3RWaWV3NS5ub3RpZnlQdWxsVG9SZWZyZXNoRmluaXNoZWQoKTtcbiAgICB9LCA1MDApO1xufTtcblxuZXhwb3J0cy5vbkl0ZW1TZWxlY3RlZCA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgbGV0IHRhYmluZGV4ID0gYXJncy5vYmplY3QudGFiaW5kZXg7XG4gICAgbGV0IGxpc3RWaWV3ID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3R2aWV3XCIgKyB0YWJpbmRleCk7XG4gICAgbGV0IHNlbGVjdGVkSXRlbXMgPSBsaXN0Vmlldy5nZXRTZWxlY3RlZEl0ZW1zKCk7XG5cbiAgICBsZXQgcGF0aWVudGRhdGEgPSBzZWxlY3RlZEl0ZW1zWzBdO1xuICAgIC8vIGNvbnNvbGUubG9nKHBhdGllbnRkYXRhKTtcbiAgICAvLyBsZXQgZG9jdG9ycGVyaWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJkb2N0b3JcIiArIHRhYmluZGV4KTtcbiAgICAvLyBsZXQgY2xpbmljQ29kZSA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImNsaW5pY2NvZGVcIiArIHRhYmluZGV4KTtcbiAgICBsZXQgcEhOID0gcGF0aWVudGRhdGFbXCJoblwiICsgdGFiaW5kZXhdO1xuICAgIGxldCBwTmFtZSA9IHBhdGllbnRkYXRhW1wibmFtZVwiICsgdGFiaW5kZXhdO1xuICAgIGxldCBxVGltZSA9IHBhdGllbnRkYXRhW1wicXRpbWVcIiArIHRhYmluZGV4XTtcblxuICAgIGNvbnN0IG9wdGlvbiA9IHtcbiAgICAgICAgY29udGV4dDoge1xuICAgICAgICAgICAgdGFiOiB0YWJpbmRleCxcbiAgICAgICAgICAgIGRhdGU6IFwiMjAyMC0wNy0wNVwiLCAvL+C4reC4ouC5iOC4suC4peC4t+C4oeC5geC4geC5ieC4geC4peC4seC4muC5gOC4m+C5h+C4meC4p+C4seC4meC4leC4o+C4p+C4iOC4m+C4seC4iOC4iOC4uOC4muC4seC4mSBDdXJkYXRlXG4gICAgICAgICAgICBobjogcEhOLFxuICAgICAgICAgICAgbmFtZTogcE5hbWUsXG4gICAgICAgICAgICB0aW1lOiBxVGltZSxcbiAgICAgICAgfSxcbiAgICAgICAgY2xvc2VDYWxsYmFjazogKHBhdGllbnRpbmZvKSA9PiB7XG4gICAgICAgICAgICBpZiAodHlwZW9mIHBhdGllbnRpbmZvID09IFwib2JqZWN0XCIpIHtcbiAgICAgICAgICAgICAgICBsZXQgZG9jdG9ycGVyaWQgPSBwYXRpZW50aW5mby5jX2RvY3Q7XG4gICAgICAgICAgICAgICAgbGV0IGRhdGUgPSBwYXRpZW50aW5mby5kYXRlO1xuICAgICAgICAgICAgICAgIGFwaXNlcnZpY2VcbiAgICAgICAgICAgICAgICAgICAgLmNoZWNrUGF0aWVudFRpbWUocGF0aWVudGluZm8pXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChyZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgcGF0aWVudGluZm9bXCJhY3Rpb25cIl0gPSByZXNwb25zZS5hY3Rpb247XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhwYXRpZW50aW5mbyk7XG4gICAgICAgICAgICAgICAgICAgICAgICBhcGlzZXJ2aWNlXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLnVwZGF0ZVBhdGllbnRUaW1lKHBhdGllbnRpbmZvKVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlamVjdChlcnIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgZ2V0RG9jdG9ySW5mbyhcInJlbG9hZFwiLCB0YWJpbmRleCwgZG9jdG9ycGVyaWQsIGRhdGUpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJjbG9zZSBwYXRpZW50IG1vZGFsXCIpO1xuICAgICAgICAgICAgICAgIGxpc3RWaWV3LmRlc2VsZWN0SXRlbUF0KGFyZ3MuaW5kZXgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcbiAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgfTtcbiAgICBtb2RhbFZpZXcuc2hvd01vZGFsKHBhdGllbnRWaWV3TW9kdWxlLCBvcHRpb24pO1xufTtcblxuZXhwb3J0cy5vbkRvY3RvckNsb3NlZCA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgbGV0IHRhYmluZGV4ID0gYXJncy5vYmplY3QudGFiaW5kZXg7XG4gICAgbGV0IHBlcmlkID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yXCIgKyB0YWJpbmRleCk7XG4gICAgZGlhbG9nc1xuICAgICAgICAuY29uZmlybSh7XG4gICAgICAgICAgICB0aXRsZTogXCLguJnguLPguYHguJ7guJfguKLguYzguK3guK3guIHguIjguLLguIHguITguKXguLTguJnguLTguIFcIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiXCIsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwi4LiV4LiB4Lil4LiHXCIsXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIuC4ouC4geC5gOC4peC4tOC4gVwiLFxuICAgICAgICAgICAgY2FuY2VsYWJsZTogZmFsc2UsXG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKChyKSA9PiB7XG4gICAgICAgICAgICBpZiAocikge1xuICAgICAgICAgICAgICAgIGNsZWFyRGF0YSh0YWJpbmRleCk7XG4gICAgICAgICAgICAgICAgc2V0UmFkTGlzdFZpZXcoXCJoaWRlbGlzdHZpZXdcIiwgdGFiaW5kZXgpO1xuICAgICAgICAgICAgICAgIGxldCBvcHRpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICB0ZXh0OiBcIuC4meC4s+C5geC4nuC4l+C4ouC5jOC4reC4reC4geC4iOC4suC4geC4hOC4peC4tOC4meC4tOC4geC4quC4s+C5gOC4o+C5h+C4iFwiLFxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogbnN0b2FzdHMuRFVSQVRJT04uU0hPUlQsXG4gICAgICAgICAgICAgICAgICAgIHBvc2l0aW9uOiBuc3RvYXN0cy5QT1NJVElPTi5CT1RUT00sIC8vb3B0aW9uYWxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgIG5zdG9hc3RzLnNob3cob3B0aW9ucyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xufTtcblxuLy8gU3RlcCAxIOC4leC4o+C4p+C4iOC4quC4reC4muC4guC5ieC4reC4oeC4ueC4peC5geC4nuC4l+C4ouC5jFxuZnVuY3Rpb24gZ2V0RG9jdG9ySW5mbyhhY3Rpb24sIHRhYiwgcGVyaWQsIGN1cmRhdGUpIHtcbiAgICBsZXQgZG9jdG9ySW5mb1Byb21zID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBhcGlzZXJ2aWNlXG4gICAgICAgICAgICAuZ2V0RG9jdG9ySW5mbyhwZXJpZClcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigoaW5mb3Jlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IGluZm8gPSBpbmZvcmVzcG9uc2UuZGF0YTtcbiAgICAgICAgICAgICAgICBpZiAoaW5mby5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgICAgICAgICBhcHBTZXR0aW5ncy5yZW1vdmUoXCJkb2N0b3JcIiArIHRhYik7XG4gICAgICAgICAgICAgICAgICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImRvY3RvclwiICsgdGFiLCBwZXJpZCk7XG4gICAgICAgICAgICAgICAgICAgIC8vIGxldCBkb2N0b3JuYW1lID0gZG9jdG9yaW5mb1swXS5kb2N0b3JuYW1lO1xuICAgICAgICAgICAgICAgICAgICAvLyBsZXQgZG9jdG9yc2V4ID0gZG9jdG9yaW5mb1swXS5zZXg7XG4gICAgICAgICAgICAgICAgICAgIGxldCBkb2N0b3JpbmZvID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgZG9jdG9ybmFtZTogaW5mb1swXS5kb2N0b3JuYW1lLFxuICAgICAgICAgICAgICAgICAgICAgICAgc2V4OiBpbmZvWzBdLnNleCxcbiAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShkb2N0b3JpbmZvKTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuC5hOC4oeC5iOC4nuC4muC4guC5ieC4reC4oeC4ueC4peC5geC4nuC4l+C4ouC5jFwiKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICB9KTtcbiAgICBkb2N0b3JJbmZvUHJvbXMudGhlbigoZG9jdG9yaW5mbykgPT4ge1xuICAgICAgICBnZXREb2N0b3JDbGluaWNJbmZvKGFjdGlvbiwgdGFiLCBwZXJpZCwgY3VyZGF0ZSwgZG9jdG9yaW5mbyk7XG4gICAgfSk7XG59XG5cbi8vIFN0ZXAgMiDguJXguKPguKfguIjguKrguK3guJrguILguYnguK3guKHguLnguKXguITguKXguLTguJnguLTguIHguJfguLXguYjguYDguKXguLfguK3guIFcbmZ1bmN0aW9uIGdldERvY3RvckNsaW5pY0luZm8oYWN0aW9uLCB0YWIsIHBlcmlkLCBjdXJkYXRlLCBkb2N0b3JpbmZvKSB7XG4gICAgbGV0IGNsaW5pY0luZm9Qcm9tcyA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcbiAgICAgICAgYXBpc2VydmljZVxuICAgICAgICAgICAgLmdldENsaW5pYyhwZXJpZCwgY3VyZGF0ZSlcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coZXJyKTtcbiAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgICAgICByZXR1cm4gUHJvbWlzZS5yZWplY3QoKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbigocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICBsZXQgY2xpbmljTGlzdCA9IHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgaWYgKGNsaW5pY0xpc3QubGVuZ3RoICE9IDApIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShjbGluaWNMaXN0KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKFtdKTtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLguYTguKHguYjguKHguLXguITguKXguLTguJnguLTguIFcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfSk7XG4gICAgY2xpbmljSW5mb1Byb21zLnRoZW4oKGNsaW5pY2luZm8pID0+IHtcbiAgICAgICAgZ2V0RG9jdG9yUXVldWUoYWN0aW9uLCB0YWIsIGNsaW5pY2luZm8sIGRvY3RvcmluZm8sIGN1cmRhdGUpO1xuICAgIH0pO1xufVxuXG4vLyBTdGVwIDMg4LiU4Li24LiH4LiC4LmJ4Lit4Lih4Li54Lil4LiE4Li04Lin4LiV4Lij4Lin4LiI4LiC4Lit4LiH4LmB4Lie4LiX4Lii4LmM4LmD4LiZ4LiE4Lil4Li04LiZ4Li04LiB4LiZ4Lix4LmJ4LiZ4LmGXG5mdW5jdGlvbiBnZXREb2N0b3JRdWV1ZShhY3Rpb24sIHRhYiwgY2xpbmljaW5mbywgZG9jdG9yaW5mbywgY3VyZGF0ZSkge1xuICAgIGlmIChjbGluaWNpbmZvLmxlbmd0aCAhPSAwKSB7XG4gICAgICAgIC8vIGNvbnN0IG1haW5WaWV3ID0gYXJncy5vYmplY3Q7XG4gICAgICAgIHN3aXRjaCAoYWN0aW9uKSB7XG4gICAgICAgICAgICBjYXNlIFwibmV3XCI6XG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9uID0ge1xuICAgICAgICAgICAgICAgICAgICBjb250ZXh0OiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IHRhYixcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3Rvcm5hbWU6IGRvY3RvcmluZm8uZG9jdG9ybmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsaW5pY2xpc3Q6IGNsaW5pY2luZm8sXG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIGNsb3NlQ2FsbGJhY2s6IChkb2N0b3JxdWV1ZWluZm8pID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZG9jdG9ycXVldWVpbmZvID09IFwib2JqZWN0XCIpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXVlbGlzdCA9IGRvY3RvcnF1ZXVlaW5mby5xdWV1ZUxpc3Q7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0UXVldWVMaXN0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0YWIsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcmluZm8sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcnF1ZXVlaW5mbyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVldWVsaXN0XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJjbG9zZSBtb2RhbFwiKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgZnVsbHNjcmVlbjogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgIGFuaW1hdGVkOiB0cnVlLFxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgbW9kYWxWaWV3LnNob3dNb2RhbChtb2RhbFZpZXdNb2R1bGUsIG9wdGlvbik7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwicmVsb2FkXCI6XG4gICAgICAgICAgICAgICAgbGV0IGRvY3RvclBlcmlkID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yXCIgKyB0YWIpO1xuICAgICAgICAgICAgICAgIGxldCBjbGluaWNDb2RlID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiY2xpbmljY29kZVwiICsgdGFiKTtcbiAgICAgICAgICAgICAgICBsZXQgY2xpbmljTmFtZSA9IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImNsaW5pY25hbWVcIiArIHRhYik7XG4gICAgICAgICAgICAgICAgYXBpc2VydmljZVxuICAgICAgICAgICAgICAgICAgICAuZ2V0RG9jdG9yUXVldWUoY3VyZGF0ZSwgY2xpbmljQ29kZSwgZG9jdG9yUGVyaWQpXG4gICAgICAgICAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgICAgIC50aGVuKChjbGluaWNxdWV1ZXJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgcXVldWVsaXN0ID0gY2xpbmljcXVldWVyZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGRvY3RvcnF1ZXVlaW5mbztcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChxdWV1ZWxpc3QubGVuZ3RoICE9IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N0b3JxdWV1ZWluZm8gPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaW5pY0NvZGU6IGNsaW5pY0NvZGUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsaW5pY05hbWU6IGNsaW5pY05hbWUsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXVlTGlzdDogcXVldWVsaXN0LFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcnF1ZXVlaW5mbyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogXCJcIixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBbXSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgc2V0UXVldWVMaXN0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N0b3JpbmZvLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcnF1ZXVlaW5mbyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWV1ZWxpc3RcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4LmE4Lih4LmI4Lie4Lia4LiC4LmJ4Lit4Lih4Li54Lil4LiE4Lil4Li04LiZ4Li04LiBXCIpO1xuICAgIH1cbn1cblxuLy8gU3RlcCA0IOC5gOC4iuC5h+C4hCBhY3Rpb24g4Lin4LmI4Liy4LmA4Lib4LmH4LiZIG5ldyDguKvguKPguLfguK0gcmVsb2FkXG5mdW5jdGlvbiBzZXRRdWV1ZUxpc3QodGFiLCBkb2N0b3JpbmZvLCBkb2N0b3JxdWV1ZWluZm8sIHF1ZXVlbGlzdCkge1xuICAgIGxldCBkYXRhID0gW107XG4gICAgaWYgKHF1ZXVlbGlzdC5sZW5ndGggIT0gMCkge1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhxdWV1ZWxpc3QpO1xuICAgICAgICBxdWV1ZWxpc3QuZm9yRWFjaCgoZWxlbWVudCkgPT4ge1xuICAgICAgICAgICAgbGV0IG5hbWUgPSBcIm5hbWVcIiArIHRhYjtcbiAgICAgICAgICAgIGxldCBobiA9IFwiaG5cIiArIHRhYjtcbiAgICAgICAgICAgIGxldCBxdGltZSA9IFwicXRpbWVcIiArIHRhYjtcbiAgICAgICAgICAgIGxldCBxU3RhdHVzID0gXCJcIjtcbiAgICAgICAgICAgIGxldCBzdGF0dXNjb2xvciA9IFwic3RhdHVzY29sb3JcIiArIHRhYjtcbiAgICAgICAgICAgIGxldCB0aW1lO1xuICAgICAgICAgICAgbGV0IHN0YXR1cyA9IFwic3RhdHVzXCIgKyB0YWI7XG4gICAgICAgICAgICBsZXQgcGF0aWVudGltZyA9IFwicGF0aWVudGltZ1wiICsgdGFiO1xuICAgICAgICAgICAgbGV0IHBhdGllbnRpbWdQYXRoID0gXCJcIjtcblxuICAgICAgICAgICAgc3dpdGNoIChlbGVtZW50LnNleCkge1xuICAgICAgICAgICAgICAgIGNhc2UgXCIxXCI6XG4gICAgICAgICAgICAgICAgICAgIHBhdGllbnRpbWdQYXRoID0gXCJ+L3NyYy9pbWcvbWFuLXBhdGllbnQucG5nXCI7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgXCIyXCI6XG4gICAgICAgICAgICAgICAgICAgIHBhdGllbnRpbWdQYXRoID0gXCJ+L3NyYy9pbWcvd29tYW4tcGF0aWVudC5wbmdcIjtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC8vIOC4leC4o+C4p+C4iOC4quC4reC4muC4nOC4ueC5ieC4m+C5iOC4p+C4ouC4l+C4teC5iOC4ouC4seC4h+C4leC4o+C4p+C4iOC5hOC4oeC5iOC5gOC4quC4o+C5h+C4iCDguKvguKPguLfguK0g4LiB4Liz4Lil4Lix4LiH4Lij4Lit4Lie4Lia4LmB4Lie4LiX4Lii4LmMIOC4o+C4reC4nuC4muC4nuC4ouC4suC4muC4suC4pVxuICAgICAgICAgICAgLy8gbnVyc2VfdGltZSA9IOC5gOC4p+C4peC4suC4l+C4teC5iOC4nOC4ueC5ieC4m+C5iOC4p+C4ouC4nuC4muC4nuC4ouC4suC4muC4suC4peC5gOC4o+C4teC4ouC4muC4o+C5ieC4reC4ouC5geC4peC5ieC4pyDguKvguKPguLfguK0g4Lit4Lit4LiB4LiI4Liy4LiB4LiE4Lil4Li04LiZ4Li04LiB4LmB4Lil4LmJ4LinXG4gICAgICAgICAgICAvLyBpZiAoZWxlbWVudC5udXJzZV90aW1lID09IFwiXCIpIHtcbiAgICAgICAgICAgIGxldCBiYWNrZ3JvdW5kQ29sb3IgPSBcIlwiO1xuICAgICAgICAgICAgaWYgKGVsZW1lbnQucV9leGl0X251cnNlICE9IFwiXCIgJiYgZWxlbWVudC5xX2V4aXRfbnVyc2UgIT0gXCIwMDowMFwiKSB7XG4gICAgICAgICAgICAgICAgcVN0YXR1cyA9IFwi4Lij4Lit4Lie4Lia4Lie4Lii4Liy4Lia4Liy4LilXCI7XG4gICAgICAgICAgICAgICAgdGltZSA9IGVsZW1lbnQucV9leGl0X251cnNlO1xuICAgICAgICAgICAgICAgIGJhY2tncm91bmRDb2xvciA9IFwiIzRCQUVBMFwiO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChcbiAgICAgICAgICAgICAgICBlbGVtZW50LnFfZG9jdF90aW1lICE9IFwiXCIgJiZcbiAgICAgICAgICAgICAgICBlbGVtZW50LnFfZG9jdF90aW1lICE9IFwiMDA6MDBcIlxuICAgICAgICAgICAgKSB7XG4gICAgICAgICAgICAgICAgcVN0YXR1cyA9IFwi4Lij4Lit4Lie4Lia4LmB4Lie4LiX4Lii4LmMXCI7XG4gICAgICAgICAgICAgICAgdGltZSA9IGVsZW1lbnQucV9kb2N0X3RpbWU7XG4gICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yID0gXCIjRDQ1RDc5XCI7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHFTdGF0dXMgPSBcIuC4o+C4reC5gOC4o+C4teC4ouC4gVwiO1xuICAgICAgICAgICAgICAgIHRpbWUgPSBlbGVtZW50LnFfdGltZTtcbiAgICAgICAgICAgICAgICBiYWNrZ3JvdW5kQ29sb3IgPSBcIiMzMDQ3NUVcIjtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRhdGEucHVzaCh7XG4gICAgICAgICAgICAgICAgW25hbWVdOiBlbGVtZW50LnBhdGllbnRuYW1lLFxuICAgICAgICAgICAgICAgIFtobl06IFwiSE5cIiArIGVsZW1lbnQuaG4sXG4gICAgICAgICAgICAgICAgW3N0YXR1c106IHFTdGF0dXMsXG4gICAgICAgICAgICAgICAgW3N0YXR1c2NvbG9yXTogYmFja2dyb3VuZENvbG9yLFxuICAgICAgICAgICAgICAgIFtxdGltZV06IHRpbWUsXG4gICAgICAgICAgICAgICAgW3BhdGllbnRpbWddOiBwYXRpZW50aW1nUGF0aCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgLy8gfVxuICAgICAgICB9KTtcbiAgICAgICAgbGV0IGNOYW1lID0gZG9jdG9ycXVldWVpbmZvLmNsaW5pY05hbWU7XG4gICAgICAgIGxldCBwcm9maWxlaW1nID0gcGFnZS5nZXRWaWV3QnlJZChcInByb2ZpbGVpbWdcIiArIHRhYik7XG4gICAgICAgIGxldCBkb2N0b3JuYW1lID0gcGFnZS5nZXRWaWV3QnlJZChcImRvY3Rvcm5hbWVcIiArIHRhYik7XG4gICAgICAgIGxldCBjbGluaWNuYW1lID0gcGFnZS5nZXRWaWV3QnlJZChcImNsaW5pY25hbWVcIiArIHRhYik7XG4gICAgICAgIGxldCBwYXRpZW50YW1vdW50ID0gcGFnZS5nZXRWaWV3QnlJZChcInBhdGllbnRhbW91bnRcIiArIHRhYik7XG5cbiAgICAgICAgbGV0IGROYW1lID0gZG9jdG9yaW5mby5kb2N0b3JuYW1lO1xuICAgICAgICBsZXQgZFNleCA9IGRvY3RvcmluZm8uc2V4O1xuICAgICAgICAvLyBDaGVjayBkb2N0b3Igc2V4IHRvIHNldCBkZWZhdWx0IHByb2ZpbGUgaW1hZ2VcbiAgICAgICAgc3dpdGNoIChkU2V4KSB7XG4gICAgICAgICAgICBjYXNlIFwiMVwiOlxuICAgICAgICAgICAgICAgIHByb2ZpbGVpbWcuc3JjID0gXCJ+L3NyYy9pbWcvd29tYW4tZG9jdG9yLnBuZ1wiO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBcIjJcIjpcbiAgICAgICAgICAgICAgICBwcm9maWxlaW1nLnNyYyA9IFwifi9zcmMvaW1nL21hbi1kb2N0b3IucG5nXCI7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG5cbiAgICAgICAgZG9jdG9ybmFtZS50ZXh0ID0gZE5hbWU7XG4gICAgICAgIGNsaW5pY25hbWUudGV4dCA9IFwi4LiE4Lil4Li04LiZ4Li04LiBXCIgKyBjTmFtZS50cmltKCk7XG4gICAgICAgIHBhdGllbnRhbW91bnQudGV4dCA9IFwi4LiI4Liz4LiZ4Lin4LiZ4Lic4Li54LmJ4Lib4LmI4Lin4LiiOiBcIiArIGRhdGEubGVuZ3RoICsgXCIg4LiE4LiZXCI7XG5cbiAgICAgICAgc3dpdGNoICh0YWIpIHtcbiAgICAgICAgICAgIGNhc2UgXCIxXCI6XG4gICAgICAgICAgICAgICAgZGF0YTEgPSBkYXRhO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgY2FzZSBcIjJcIjpcbiAgICAgICAgICAgICAgICBkYXRhMiA9IGRhdGE7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiM1wiOlxuICAgICAgICAgICAgICAgIGRhdGEzID0gZGF0YTtcbiAgICAgICAgICAgIGNhc2UgXCI0XCI6XG4gICAgICAgICAgICAgICAgZGF0YTQgPSBkYXRhO1xuICAgICAgICAgICAgY2FzZSBcIjVcIjpcbiAgICAgICAgICAgICAgICBkYXRhNSA9IGRhdGE7XG4gICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICB9XG4gICAgICAgIHNldFJhZExpc3RWaWV3KFwic2hvd2xpc3R2aWV3XCIsIHRhYik7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgY29uc29sZS5sb2coXCLguYTguKHguYjguJ7guJrguILguYnguK3guKHguLnguKXguITguLTguKfguJXguKPguKfguIhcIik7XG4gICAgfVxufVxuXG4vLyBTdGVwIDUg4LiZ4Liz4LiC4LmJ4Lit4Lih4Li54Lil4LiE4Li04Lin4LmD4Liq4LmI4LmD4LiZIGxpc3RcbmZ1bmN0aW9uIHNldFJhZExpc3RWaWV3KGFjdGlvbiwgdGFiKSB7XG4gICAgY29uc3Qgdm0gPSBmcm9tT2JqZWN0KHtcbiAgICAgICAgZGF0YUl0ZW1zMTogZGF0YTEsXG4gICAgICAgIGRhdGFJdGVtczI6IGRhdGEyLFxuICAgICAgICBkYXRhSXRlbXMzOiBkYXRhMyxcbiAgICAgICAgZGF0YUl0ZW1zNDogZGF0YTQsXG4gICAgICAgIGRhdGFJdGVtczU6IGRhdGE1LFxuICAgIH0pO1xuICAgIHBhZ2UuYmluZGluZ0NvbnRleHQgPSB2bTtcbiAgICBzd2l0Y2hVSShhY3Rpb24sIHRhYik7XG59XG5cbmZ1bmN0aW9uIGdldERhdGVTdHJpbmcoZGF0ZSkge1xuICAgIGxldCBtb250aFN0cjtcbiAgICBsZXQgZGF5U3RyO1xuICAgIGlmIChwYXJzZUludChkYXRlLmdldE1vbnRoKCkgKyAxKSA8PSA5KSB7XG4gICAgICAgIG1vbnRoU3RyID0gXCIwXCIgKyAoZGF0ZS5nZXRNb250aCgpICsgMSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgbW9udGhTdHIgPSBkYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgIH1cblxuICAgIGlmIChwYXJzZUludChkYXRlLmdldERhdGUoKSkgPD0gOSkge1xuICAgICAgICBkYXlTdHIgPSBcIjBcIiArIGRhdGUuZ2V0RGF0ZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGRheVN0ciA9IGRhdGUuZ2V0RGF0ZSgpO1xuICAgIH1cblxuICAgIGxldCBuZXdEYXRlRm9ybWF0ID0ge1xuICAgICAgICB5ZWFyOiBkYXRlLmdldEZ1bGxZZWFyKCksXG4gICAgICAgIG1vbnRoOiBtb250aFN0cixcbiAgICAgICAgZGF5OiBkYXlTdHIsXG4gICAgfTtcblxuICAgIHJldHVybiBuZXdEYXRlRm9ybWF0O1xufVxuXG5mdW5jdGlvbiBjbGVhckRhdGEodGFiKSB7XG4gICAgYXBwU2V0dGluZ3MucmVtb3ZlKFwiZG9jdG9yXCIgKyB0YWIpO1xuICAgIHN3aXRjaCAodGFiKSB7XG4gICAgICAgIGNhc2UgXCIxXCI6XG4gICAgICAgICAgICBkYXRhMSA9IFtdO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCIyXCI6XG4gICAgICAgICAgICBkYXRhMiA9IFtdO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCIzXCI6XG4gICAgICAgICAgICBkYXRhMyA9IFtdO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCI0XCI6XG4gICAgICAgICAgICBkYXRhNCA9IFtdO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCI1XCI6XG4gICAgICAgICAgICBkYXRhNSA9IFtdO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG59XG5cbmZ1bmN0aW9uIHN3aXRjaFVJKGFjdGlvbiwgdGFiKSB7XG4gICAgLy8gY29uc29sZS5sb2coYWN0aW9uKTtcbiAgICAvLyBjb25zb2xlLmxvZyh0YWIpO1xuICAgIHN3aXRjaCAoYWN0aW9uKSB7XG4gICAgICAgIGNhc2UgXCJzaG93bGlzdHZpZXdcIjpcbiAgICAgICAgICAgIHN3aXRjaCAodGFiKSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjFcIjpcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yMS52aXNpYmlsaXR5ID0gXCJjb2xsYXBzZVwiO1xuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3IxLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjEudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgICAgICBkb2N0b3IxLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwiMlwiOlxuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3IyLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjIuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yMi52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjIuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgXCIzXCI6XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjMudmlzaWJpbGl0eSA9IFwiY29sbGFwc2VcIjtcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yMy5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwLFxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBkb2N0b3IzLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yMy5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjRcIjpcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yNC52aXNpYmlsaXR5ID0gXCJjb2xsYXBzZVwiO1xuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3I0LmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjQudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgICAgICBkb2N0b3I0LmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwiNVwiOlxuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3I1LnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjUuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yNS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjUuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGNhc2UgXCJoaWRlbGlzdHZpZXdcIjpcbiAgICAgICAgICAgIHN3aXRjaCAodGFiKSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjFcIjpcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yMS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjEuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yMS52aXNpYmlsaXR5ID0gXCJjb2xsYXBzZVwiO1xuICAgICAgICAgICAgICAgICAgICBkb2N0b3IxLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwiMlwiOlxuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3IyLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yMi5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwLFxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBkb2N0b3IyLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjIuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgXCIzXCI6XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjMudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3IzLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjMudmlzaWJpbGl0eSA9IFwiY29sbGFwc2VcIjtcbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yMy5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwLFxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjRcIjpcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yNC52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICAgICAgICAgIGJ0bkRvY3RvcjQuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgICAgICAgICAgZG9jdG9yNC52aXNpYmlsaXR5ID0gXCJjb2xsYXBzZVwiO1xuICAgICAgICAgICAgICAgICAgICBkb2N0b3I0LmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAzMDAsXG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIFwiNVwiOlxuICAgICAgICAgICAgICAgICAgICBidG5Eb2N0b3I1LnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgICAgICAgICAgYnRuRG9jdG9yNS5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMzAwLFxuICAgICAgICAgICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgICAgICAgICBkb2N0b3I1LnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gICAgICAgICAgICAgICAgICAgIGRvY3RvcjUuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDMwMCxcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG59XG47IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9tYWluL21haW4tcGFnZS5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJnbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcbmdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlldy9SYWRMaXN0Vmlld1wiLCBmdW5jdGlvbigpIHsgcmV0dXJuIHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIik7IH0pO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3L1JhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZVwiLCBmdW5jdGlvbigpIHsgcmV0dXJuIHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIik7IH0pO1xuXG5tb2R1bGUuZXhwb3J0cyA9IFwiPFBhZ2UgeG1sbnM9XFxcImh0dHA6Ly9zY2hlbWFzLm5hdGl2ZXNjcmlwdC5vcmcvdG5zLnhzZFxcXCJcXG4gICAgeG1sbnM6bHY9XFxcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1xcXCIgbG9hZGVkPVxcXCJvblBhZ2VMb2FkZWRcXFwiPlxcblxcbiAgICA8UGFnZS5hY3Rpb25CYXI+XFxuICAgICAgICA8QWN0aW9uQmFyIGJhY2tncm91bmRDb2xvcj1cXFwiIzFlNGIyNVxcXCIgY29sb3I9XFxcIndoaXRlXFxcIj5cXG4gICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcImhlYWRlclxcXCIgaWQ9XFxcImhlYWRlclxcXCIgdGV4dD1cXFwiQ2xpbmljIFF1ZXVlXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgIDwhLS0gPEFjdGlvbkl0ZW0gY2xhc3M9XFxcImZhc1xcXCIgaWQ9XFxcIm5ld2RvY3RvcmJ0blxcXCIgaWNvbj1cXFwiZm9udDovLyYjeGYwNTU7XFxcIiBpb3MucG9zaXRpb249XFxcInJpZ2h0XFxcIiAvPiAtLT5cXG4gICAgICAgIDwvQWN0aW9uQmFyPlxcbiAgICA8L1BhZ2UuYWN0aW9uQmFyPlxcblxcbiAgICA8R3JpZExheW91dD5cXG4gICAgICAgIDxUYWJzIGlkPVxcXCJkb2N0b3J0YWJzXFxcIiBzZWxlY3RlZEluZGV4PVxcXCIwXFxcIiBzZWxlY3RlZEluZGV4Q2hhbmdlZD1cXFwib25JbmRleFNlbGVjdGVkXFxcIj5cXG4gICAgICAgICAgICA8VGFiU3RyaXA+XFxuICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWxcXFwiIHRleHQ9XFxcIuC5geC4nuC4l+C4ouC5jCAxXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbFxcXCIgdGV4dD1cXFwi4LmB4Lie4LiX4Lii4LmMIDJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsXFxcIiB0ZXh0PVxcXCLguYHguJ7guJfguKLguYwgM1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgPC9UYWJTdHJpcEl0ZW0+XFxuICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWxcXFwiIHRleHQ9XFxcIuC5geC4nuC4l+C4ouC5jCA0XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbFxcXCIgdGV4dD1cXFwi4LmB4Lie4LiX4Lii4LmMIDVcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgIDwvVGFiU3RyaXA+XFxuXFxuICAgICAgICAgICAgPFRhYkNvbnRlbnRJdGVtPlxcbiAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBpZD1cXFwiYnRuRG9jdG9yMVxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcInZpc2libGVcXFwiIG9wYWNpdHk9XFxcIjFcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IHJvd3M9XFxcIipcXFwiIGNvbHVtbnM9XFxcImF1dG9cXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8QnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIGFkZC1kb2N0b3ItYnRuXFxcIiB0ZXh0PVxcXCLguYDguJ7guLTguYjguKHguYHguJ7guJfguKLguYwgMVxcXCIgdGFwPVxcXCJhZGREb2N0b3IxXFxcIj48L0J1dHRvbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8L0ZsZXhib3hMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgaWQ9XFxcImRvY3RvcjFcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiI2VlZVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiB2aXNpYmlsaXR5PVxcXCJjb2xsYXBzZVxcXCIgb3BhY2l0eT1cXFwiMFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJjYXJkLWRvY3Rvci1wcm9maWxlIG0tMTYgcC04IHRleHQtY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgY29sdW1ucz1cXFwiYXV0bywgKiwgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGlkPVxcXCJwcm9maWxlaW1nMVxcXCIgd2lkdGg9XFxcIjEyMFxcXCIgaGVpZ2h0PVxcXCIxMjBcXFwiIHN0cmV0Y2g9XFxcImFzcGVjdEZpbGxcXFwiLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcIm0tMTZcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1uYW1lLWxhYmVsIHRleHQtbGVmdFxcXCIgaWQ9XFxcImRvY3Rvcm5hbWUxXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbCB0ZXh0LWxlZnRcXFwiIGlkPVxcXCJjbGluaWNuYW1lMVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwicGF0aWVudGFtb3VudDFcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJwLThcXFwiIGNvbD1cXFwiMlxcXCIgcm93PVxcXCIwXFxcIiBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJyaWdodFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cXFwidGV4dC1jZW50ZXJcXFwiIHRleHQ9XFxcInhcXFwiIGJvcmRlclJhZGl1cz1cXFwiNTAlXFxcIiB3aWR0aD1cXFwiMjRcXFwiIGhlaWdodD1cXFwiMjRcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiY3JpbXNvblxcXCIgY29sb3I9XFxcIndoaXRlXFxcIiB0YXA9XFxcIm9uRG9jdG9yQ2xvc2VkXFxcIiB0YWJpbmRleD1cXFwiMVxcXCI+PC9idXR0b24+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8bHY6UmFkTGlzdFZpZXcgaWQ9XFxcImxpc3R2aWV3MVxcXCIgY2xhc3M9XFxcImxpc3R2aWV3LXBhdGllbnQgcC1iLTE2XFxcIiBpdGVtcz1cXFwie3tkYXRhSXRlbXMxfX1cXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgcHVsbFRvUmVmcmVzaD1cXFwidHJ1ZVxcXCIgcHVsbFRvUmVmcmVzaEluaXRpYXRlZD1cXFwib25QdWxsVG9SZWZyZXNoSW5pdGlhdGVkMVxcXCIgc2VsZWN0aW9uQmVoYXZpb3I9XFxcIlByZXNzXFxcIiBpdGVtU2VsZWN0ZWQ9XFxcIm9uSXRlbVNlbGVjdGVkXFxcIiB0YWJpbmRleD1cXFwiMVxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwicGF0aWVudC1pbWFnZVxcXCIgc3JjPVxcXCJ7e3BhdGllbnRpbWcxfX1cXFwiIHdpZHRoPVxcXCI2MFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lMX19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCIyKiwgKiwgKlxcXCIgcm93cz1cXFwiKlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3tobjF9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LWNlbnRlciBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIiB0ZXh0PVxcXCJ7e3N0YXR1czF9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJ7e3N0YXR1c2NvbG9yMX19XFxcIiBib3JkZXJSYWRpdXM9XFxcIjUwJVxcXCIgY29sb3I9XFxcInllbGxvd1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LXJpZ2h0IG0tci0xNlxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiB0ZXh0PVxcXCJ7e3F0aW1lMX19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXG5cXG4gICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGlkPVxcXCJidG5Eb2N0b3IyXFxcIiBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwidmlzaWJsZVxcXCIgb3BhY2l0eT1cXFwiMVxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gY2xhc3M9XFxcInRleHQtbGFiZWwgYWRkLWRvY3Rvci1idG5cXFwiIHRleHQ9XFxcIuC5gOC4nuC4tOC5iOC4oeC5geC4nuC4l+C4ouC5jCAyXFxcIiB0YXA9XFxcImFkZERvY3RvcjJcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBpZD1cXFwiZG9jdG9yMlxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcImNvbGxhcHNlXFxcIiBvcGFjaXR5PVxcXCIwXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImNhcmQtZG9jdG9yLXByb2ZpbGUgbS0xNiBwLTggdGV4dC1jZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCAqLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgaWQ9XFxcInByb2ZpbGVpbWcyXFxcIiB3aWR0aD1cXFwiMTIwXFxcIiBoZWlnaHQ9XFxcIjEyMFxcXCIgc3RyZXRjaD1cXFwiYXNwZWN0RmlsbFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwibS0xNlxcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIHRleHQtbGVmdFxcXCIgaWQ9XFxcImNsaW5pY25hbWUyXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbCB0ZXh0LWxlZnRcXFwiIGlkPVxcXCJwYXRpZW50YW1vdW50MlxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcInAtOFxcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCIgdGV4dD1cXFwieFxcXCIgYm9yZGVyUmFkaXVzPVxcXCI1MCVcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJjcmltc29uXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25Eb2N0b3JDbG9zZWRcXFwiIHRhYmluZGV4PVxcXCIyXFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXcyXFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczJ9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQyXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRhYmluZGV4PVxcXCIyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNsYXNzPVxcXCJsaXN0dmlldy1pdGVtIG0tdC0xNiBtLXItMTYgbS1sLTE2XFxcIiBjb2x1bW5zPVxcXCJhdXRvLCAqXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJwYXRpZW50LWltYWdlXFxcIiBzcmM9XFxcInt7cGF0aWVudGltZzJ9fVxcXCIgd2lkdGg9XFxcIjYwXFxcIiBoZWlnaHQ9XFxcIjYwXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBvcmllbnRhdGlvbj1cXFwidmVydGljYWxcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjIwXFxcIiB0ZXh0PVxcXCJ7e25hbWUyfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcIjIqLCAqLCAqXFxcIiByb3dzPVxcXCIqXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiB0ZXh0PVxcXCJ7e2huMn19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IHRleHQtY2VudGVyIHAtbC04IHAtci04XFxcIiBmb250U2l6ZT1cXFwiMTRcXFwiIHRleHQ9XFxcInt7c3RhdHVzMn19XFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcInt7c3RhdHVzY29sb3IyfX1cXFwiIGJvcmRlclJhZGl1cz1cXFwiNTAlXFxcIiBjb2xvcj1cXFwieWVsbG93XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMlxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IHRleHQtcmlnaHQgbS1yLTE2XFxcIiBmb250U2l6ZT1cXFwiMTZcXFwiIHRleHQ9XFxcInt7cXRpbWUyfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2x2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L2x2OlJhZExpc3RWaWV3PlxcbiAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICA8L1RhYkNvbnRlbnRJdGVtPlxcblxcbiAgICAgICAgICAgIDxUYWJDb250ZW50SXRlbT5cXG4gICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgPEZsZXhib3hMYXlvdXQgaWQ9XFxcImJ0bkRvY3RvcjNcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiB2aXNpYmlsaXR5PVxcXCJ2aXNpYmxlXFxcIiBvcGFjaXR5PVxcXCIxXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCByb3dzPVxcXCIqXFxcIiBjb2x1bW5zPVxcXCJhdXRvXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwidGV4dC1sYWJlbCBhZGQtZG9jdG9yLWJ0blxcXCIgdGV4dD1cXFwi4LmA4Lie4Li04LmI4Lih4LmB4Lie4LiX4Lii4LmMIDNcXFwiIHRhcD1cXFwiYWRkRG9jdG9yM1xcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGlkPVxcXCJkb2N0b3IzXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNlZWVcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwiY29sbGFwc2VcXFwiIG9wYWNpdHk9XFxcIjBcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwiY2FyZC1kb2N0b3ItcHJvZmlsZSBtLTE2IHAtOCB0ZXh0LWNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcImF1dG8sICosIGF1dG9cXFwiIHJvd3M9XFxcImF1dG9cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBpZD1cXFwicHJvZmlsZWltZzNcXFwiIHdpZHRoPVxcXCIxMjBcXFwiIGhlaWdodD1cXFwiMTIwXFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJtLTE2XFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbmFtZS1sYWJlbCB0ZXh0LWxlZnRcXFwiIGlkPVxcXCJkb2N0b3JuYW1lM1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiY2xpbmljbmFtZTNcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIHRleHQtbGVmdFxcXCIgaWQ9XFxcInBhdGllbnRhbW91bnQzXFxcIj48L0xhYmVsPiBcXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcInAtOFxcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCIgdGV4dD1cXFwieFxcXCIgYm9yZGVyUmFkaXVzPVxcXCI1MCVcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJjcmltc29uXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25Eb2N0b3JDbG9zZWRcXFwiIHRhYmluZGV4PVxcXCIzXFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXczXFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczN9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQzXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRhYmluZGV4PVxcXCIzXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwicGF0aWVudC1pbWFnZVxcXCIgc3JjPVxcXCJ7e3BhdGllbnRpbWczfX1cXFwiIHdpZHRoPVxcXCI2MFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lM319XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCIyKiwgKiwgKlxcXCIgcm93cz1cXFwiKlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3tobjN9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LWNlbnRlciBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIiB0ZXh0PVxcXCJ7e3N0YXR1czN9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJ7e3N0YXR1c2NvbG9yM319XFxcIiBib3JkZXJSYWRpdXM9XFxcIjUwJVxcXCIgY29sb3I9XFxcInllbGxvd1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LXJpZ2h0IG0tci0xNlxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiB0ZXh0PVxcXCJ7e3F0aW1lM319XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXG5cXG4gICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGlkPVxcXCJidG5Eb2N0b3I0XFxcIiBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwidmlzaWJsZVxcXCIgb3BhY2l0eT1cXFwiMVxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gY2xhc3M9XFxcInRleHQtbGFiZWwgYWRkLWRvY3Rvci1idG5cXFwiIHRleHQ9XFxcIuC5gOC4nuC4tOC5iOC4oeC5geC4nuC4l+C4ouC5jCA0XFxcIiB0YXA9XFxcImFkZERvY3RvcjRcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBpZD1cXFwiZG9jdG9yNFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcImNvbGxhcHNlXFxcIiBvcGFjaXR5PVxcXCIwXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImNhcmQtZG9jdG9yLXByb2ZpbGUgbS0xNiBwLTggdGV4dC1jZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCAqLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgaWQ9XFxcInByb2ZpbGVpbWc0XFxcIiB3aWR0aD1cXFwiMTIwXFxcIiBoZWlnaHQ9XFxcIjEyMFxcXCIgc3RyZXRjaD1cXFwiYXNwZWN0RmlsbFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwibS0xNlxcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTRcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIHRleHQtbGVmdFxcXCIgaWQ9XFxcImNsaW5pY25hbWU0XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbCB0ZXh0LWxlZnRcXFwiIGlkPVxcXCJwYXRpZW50YW1vdW50NFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcInAtOFxcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCIgdGV4dD1cXFwieFxcXCIgYm9yZGVyUmFkaXVzPVxcXCI1MCVcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJjcmltc29uXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25Eb2N0b3JDbG9zZWRcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXc0XFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczR9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQ0XFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRhYmluZGV4PVxcXCI0XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwicGF0aWVudC1pbWFnZVxcXCIgc3JjPVxcXCJ7e3BhdGllbnRpbWc0fX1cXFwiIHdpZHRoPVxcXCI2MFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lNH19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCIyKiwgKiwgKlxcXCIgcm93cz1cXFwiKlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3tobjR9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LWNlbnRlciBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIiB0ZXh0PVxcXCJ7e3N0YXR1czR9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJ7e3N0YXR1c2NvbG9yNH19XFxcIiBib3JkZXJSYWRpdXM9XFxcIjUwJVxcXCIgY29sb3I9XFxcInllbGxvd1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LXJpZ2h0IG0tci0xNlxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiB0ZXh0PVxcXCJ7e3F0aW1lNH19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXG5cXG4gICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxGbGV4Ym94TGF5b3V0IGlkPVxcXCJidG5Eb2N0b3I1XFxcIiBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwidmlzaWJsZVxcXCIgb3BhY2l0eT1cXFwiMVxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxCdXR0b24gY2xhc3M9XFxcInRleHQtbGFiZWwgYWRkLWRvY3Rvci1idG5cXFwiIHRleHQ9XFxcIuC5gOC4nuC4tOC5iOC4oeC5geC4nuC4l+C4ouC5jCA1XFxcIiB0YXA9XFxcImFkZERvY3RvcjVcXFwiPjwvQnV0dG9uPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBpZD1cXFwiZG9jdG9yNVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcImNvbGxhcHNlXFxcIiBvcGFjaXR5PVxcXCIwXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcImNhcmQtZG9jdG9yLXByb2ZpbGUgbS0xNiBwLTggdGV4dC1jZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCAqLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgaWQ9XFxcInByb2ZpbGVpbWc1XFxcIiB3aWR0aD1cXFwiMTIwXFxcIiBoZWlnaHQ9XFxcIjEyMFxcXCIgc3RyZXRjaD1cXFwiYXNwZWN0RmlsbFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwibS0xNlxcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTVcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIHRleHQtbGVmdFxcXCIgaWQ9XFxcImNsaW5pY25hbWU1XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbCB0ZXh0LWxlZnRcXFwiIGlkPVxcXCJwYXRpZW50YW1vdW50NVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY2xhc3M9XFxcInAtOFxcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcInJpZ2h0XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWNlbnRlclxcXCIgdGV4dD1cXFwieFxcXCIgYm9yZGVyUmFkaXVzPVxcXCI1MCVcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJjcmltc29uXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25Eb2N0b3JDbG9zZWRcXFwiIHRhYmluZGV4PVxcXCI1XFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcblxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXc1XFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczV9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQ1XFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRhYmluZGV4PVxcXCI1XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKlxcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwicGF0aWVudC1pbWFnZVxcXCIgc3JjPVxcXCJ7e3BhdGllbnRpbWc1fX1cXFwiIHdpZHRoPVxcXCI2MFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lNX19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCIyKiwgKiwgKlxcXCIgcm93cz1cXFwiKlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLThcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3tobjV9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LWNlbnRlciBwLWwtOCBwLXItOFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIiB0ZXh0PVxcXCJ7e3N0YXR1czV9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCJ7e3N0YXR1c2NvbG9yNX19XFxcIiBib3JkZXJSYWRpdXM9XFxcIjUwJVxcXCIgY29sb3I9XFxcInllbGxvd1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCB0ZXh0LXJpZ2h0IG0tci0xNlxcXCIgZm9udFNpemU9XFxcIjE2XFxcIiB0ZXh0PVxcXCJ7e3F0aW1lNX19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXG5cXG4gICAgICAgIDwvVGFicz5cXG4gICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICA8IS0tIDxCb3R0b21OYXZpZ2F0aW9uIGlkPVxcXCJib3R0b21OYXZcXFwiIHNlbGVjdGVkSW5kZXg9XFxcIjBcXFwiPlxcbiAgICAgICAgPFRhYlN0cmlwPlxcbiAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0gY2xhc3M9XFxcImZhc1xcXCIgdGFwPVxcXCJkb2N0b3JxdWV1ZVxcXCI+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcImZvbnQ6Ly8mI3hmMGYwO1xcXCI+PC9JbWFnZT5cXG4gICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnRcXFwiIHRleHQ9XFxcIuC5geC4nuC4l+C4ouC5jFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcblxcbiAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0gY2xhc3M9XFxcInRhYnN0cmlwaXRlbSBmYXNcXFwiIHRhcD1cXFwic2V0dGluZ3NcXFwiPlxcbiAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJmb250Oi8vJiN4ZjA4NTtcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250XFxcIiB0ZXh0PVxcXCLguJXguLHguYnguIfguITguYjguLJcXFwiIGZvbnRTaXplPVxcXCIxNFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgIDwvVGFiU3RyaXA+XFxuXFxuXFxuXFxuICAgICAgICA8VGFiQ29udGVudEl0ZW0gYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIj5cXG4gICAgICAgICAgICA8U2Nyb2xsVmlldyBvcmllbnRhdGlvbj1cXFwidmVydGljYWxcXFwiPlxcbiAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVxcXCJjb2x1bW5cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgYmFja2dyb3VuZENvbG9yPVxcXCIjZmZmXFxcIiBjb2x1bW5zPVxcXCJhdXRvLGF1dG9cXFwiIHJvd3M9XFxcIipcXFwiIGJvcmRlckJvdHRvbUNvbG9yPVxcXCJsaWdodGdyYXlcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBpZD1cXFwic2V0dGluZ3Byb2ZpbGVpbWdcXFwiIGNsYXNzPVxcXCJtLThcXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiBzcmM9XFxcInt7cHJvZmlsZX19XFxcIiB3aWR0aD1cXFwiNjVcXFwiIGhlaWdodD1cXFwiNjVcXFwiIHN0cmV0Y2g9XFxcImFzcGVjdEZpbGxcXFwiIGhvcml6b250YWxBbGlnbm1lbnQ9XFxcImxlZnRcXFwiLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbmFtZS1sYWJlbFxcXCIgdGV4dD1cXFwi4LiK4Li34LmI4LitLeC4meC4suC4oeC4quC4geC4uOC4pVxcXCIgY29sU3Bhbj1cXFwiMlxcXCIgLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsXFxcIiB0ZXh0PVxcXCLguKvguJnguYjguKfguKLguIfguLLguJlcXFwiIGNvbFNwYW49XFxcIjJcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNsYXNzPVxcXCJtLXQtOFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIgY29sdW1ucz1cXFwiYXV0byxhdXRvLCpcXFwiIHJvd3M9XFxcIipcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiI2ZmZlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS1sLTI4XFxcIiBjb2w9XFxcIjBcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYyZjU7XFxcIiB3aWR0aD1cXFwiMjJcXFwiIGhlaWdodD1cXFwiMjJcXFwiIGNvbG9yPVxcXCIjNzU3NTc1XFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIG0tbC0xNlxcXCIgY29sPVxcXCIxXFxcIiB0ZXh0PVxcXCLguK3guK3guIHguIjguLLguIHguKPguLDguJrguJpcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICA8L1Njcm9sbFZpZXc+XFxuICAgICAgICA8L1RhYkNvbnRlbnRJdGVtPlxcbiAgICA8L0JvdHRvbU5hdmlnYXRpb24+IC0tPlxcbjwvUGFnZT5cXG5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluL21haW4tcGFnZS54bWxcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwibWFya3VwXCIsIHBhdGg6IFwiLi92aWV3cy9tYWluL21haW4tcGFnZS54bWxcIiB9KTtcbiAgICB9KTtcbn0gIiwibW9kdWxlLmV4cG9ydHMgPSB7XCJ0eXBlXCI6XCJzdHlsZXNoZWV0XCIsXCJzdHlsZXNoZWV0XCI6e1wicnVsZXNcIjpbe1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxpc3R2aWV3LWNvbnRlbnRcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGJhY2tncm91bmQtaW1hZ2U6IHVybChcXFwiYXBwL3ZpZXdzL21haW4tcGFnZS9zcmMvY2hlY2tpbi5qcGdcXFwiKSBuby1yZXBlYXQ7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgd2lkdGg6IDk1JTsgXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBoZWlnaHQ6IDEwMCU7IFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi10b3BcIixcInZhbHVlXCI6XCI4XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLWxlZnRcIixcInZhbHVlXCI6XCI4XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXJpZ2h0XCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgbWFyZ2luLWxlZnQ6IDg7XFxuICAgIG1hcmdpbi1yaWdodDogODsgXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBtYXJnaW4tYm90dG9tOiA4OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgY29sb3I6IHdoaXRlOyBcIn1dfV0sXCJwYXJzaW5nRXJyb3JzXCI6W119fTs7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IG9ic2VydmFibGVNb2R1bGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7XG5jb25zdCBNYWluUGFnZU1vZGVsID0gcmVxdWlyZShcIi4uL21haW4vbWFpbi1wYWdlLW1vZGVsXCIpO1xuY29uc3QgYXBwU2V0dGluZ3MgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvbi1zZXR0aW5nc1wiKTtcblxubGV0IGFwaXNlcnZpY2UgPSBNYWluUGFnZU1vZGVsLkFQSVNlcnZpY2UoKTtcbmxldCBjbG9zZUNhbGxiYWNrO1xubGV0IHRhYjtcblxuZXhwb3J0cy5vblNob3duTW9kYWxseSA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgY2xpbmljQm9keSA9IGFyZ3MuY29udGV4dDtcbiAgICB0YWIgPSBjbGluaWNCb2R5LnRhYjtcbiAgICBjbG9zZUNhbGxiYWNrID0gYXJncy5jbG9zZUNhbGxiYWNrO1xuICAgIGNvbnN0IHBhZ2UgPSBhcmdzLm9iamVjdDtcbiAgICBsZXQgY2xpbmljZGF0YSA9IFtdO1xuICAgIGxldCBkb2N0b3JQZXJpZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5jX2RvY3Q7XG4gICAgbGV0IGRvY3Rvck5hbWUgPSBjbGluaWNCb2R5LmRvY3Rvcm5hbWU7XG4gICAgbGV0IGRhdGVfZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5kYXRlX2Q7XG4gICAgY2xpbmljQm9keS5jbGluaWNsaXN0LmZvckVhY2goKGVsZW1lbnQpID0+IHtcbiAgICAgICAgY2xpbmljZGF0YS5wdXNoKHtcbiAgICAgICAgICAgIGNsaW5pY2NvZGU6IGVsZW1lbnQuY2xpbmljX2NvZGUsXG4gICAgICAgICAgICBjbGluaWNuYW1lOiBlbGVtZW50LmNsaW5pY19uYW1lLFxuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gb2JzZXJ2YWJsZU1vZHVsZS5mcm9tT2JqZWN0KHtcbiAgICAgICAgZG9jdG9ycGVyaWQ6IGRvY3RvclBlcmlkLFxuICAgICAgICBkb2N0b3JuYW1lOiBkb2N0b3JOYW1lLFxuICAgICAgICBjdXJyZW50ZGF0ZTogZGF0ZV9kLFxuICAgICAgICBjbGluaWNkYXRhOiBjbGluaWNkYXRhLFxuICAgIH0pO1xuXG4gICAgLy8gY29uc3QgYmluZGluZ0NvbnRleHQgPSBwYWdlLnBhZ2UuYmluZGluZ0NvbnRleHQ7XG4gICAgLy8gbGV0IGlkID0gYmluZGluZ0NvbnRleHQuZ2V0KFwiaWRcIik7XG4gICAgLy8gbGV0IG5hbWUgPSBiaW5kaW5nQ29udGV4dC5nZXQoXCJuYW1lXCIpO1xuICAgIC8vIGNsb3NlQ2FsbGJhY2soaWQsIG5hbWUpO1xufTtcblxuZXhwb3J0cy5vbkl0ZW1TZWxlY3RlZCA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgbGV0IGRvY3RvclBlcmlkID0gcGFnZS5nZXRWaWV3QnlJZChcImRvY3RvcnBlcmlkXCIpLnRleHQ7XG4gICAgbGV0IGN1cnJlbnRkYXRlID0gcGFnZS5nZXRWaWV3QnlJZChcImN1cnJlbnRkYXRlXCIpLnRleHQ7XG4gICAgbGV0IGxpc3RWaWV3ID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3RWaWV3XCIpO1xuICAgIHZhciBzZWxlY3RlZEl0ZW1zID0gbGlzdFZpZXcuZ2V0U2VsZWN0ZWRJdGVtcygpO1xuICAgIGxldCBjbGluaWNDb2RlID0gc2VsZWN0ZWRJdGVtc1swXS5jbGluaWNjb2RlO1xuICAgIGxldCBjbGluaWNOYW1lID0gc2VsZWN0ZWRJdGVtc1swXS5jbGluaWNuYW1lO1xuICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImNsaW5pY2NvZGVcIit0YWIsIGNsaW5pY0NvZGUpO1xuICAgIGFwcFNldHRpbmdzLnNldFN0cmluZyhcImNsaW5pY25hbWVcIit0YWIsIGNsaW5pY05hbWUpO1xuICAgIC8vIGNvbnNvbGUubG9nKFwiRG9jdG9yIFBlcmlkOiBcIiArIGRvY3RvclBlcmlkLnRleHQpO1xuICAgIC8vIGNvbnNvbGUubG9nKFwiRGF0ZTogXCIgKyBjdXJyZW50ZGF0ZS50ZXh0KTtcbiAgICAvLyBjb25zb2xlLmxvZyhzZWxlY3RlZEl0ZW1zWzBdKTtcbiAgICBhcGlzZXJ2aWNlXG4gICAgICAgIC5nZXREb2N0b3JRdWV1ZShjdXJyZW50ZGF0ZSwgY2xpbmljQ29kZSwgZG9jdG9yUGVyaWQpXG4gICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKChjbGluaWNxdWV1ZXJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBsZXQgY2xpbmljcXVldWVsaXN0ID0gY2xpbmljcXVldWVyZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IGNsaW5pY0RhdGE7XG4gICAgICAgICAgICBpZiAoY2xpbmljcXVldWVsaXN0Lmxlbmd0aCAhPSAwKSB7XG4gICAgICAgICAgICAgICAgY2xpbmljRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogY2xpbmljTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBjbGluaWNxdWV1ZWxpc3QsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2xpbmljRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogY2xpbmljTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBbXSxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xvc2VDYWxsYmFjayhjbGluaWNEYXRhKTtcbiAgICAgICAgfSk7XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsImdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiLCBmdW5jdGlvbigpIHsgcmV0dXJuIHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIik7IH0pO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3L1JhZExpc3RWaWV3XCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcucHVsbFRvUmVmcmVzaFN0eWxlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUHVsbFRvUmVmcmVzaFN0eWxlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5cbm1vZHVsZS5leHBvcnRzID0gXCI8UGFnZSBzaG93bk1vZGFsbHk9XFxcIm9uU2hvd25Nb2RhbGx5XFxcIlxcbiAgICB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIj5cXG4gICAgPFN0YWNrTGF5b3V0IGJhY2tncm91bmRDb2xvcj1cXFwiIzFlNGIyNVxcXCIgY29sb3I9XFxcIndoaXRlXFxcIj5cXG4gICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcImF1dG8sIGF1dG9cXFwiIHJvd3M9XFxcImF1dG9cXFwiPlxcbiAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwiZmFzIG0tbC0xNiBtLXQtOFxcXCIgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYwZjA7XFxcIiB3aWR0aD1cXFwiMjRcXFwiIGhlaWdodD1cXFwiMjRcXFwiLz5cXG4gICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgbS1sLTggbS10LThcXFwiIGlkPVxcXCJkb2N0b3JuYW1lXFxcIiB0ZXh0PVxcXCJ7e2RvY3Rvcm5hbWV9fVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvLCBhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImZhciBtLWwtMTYgbS10LTggbS1iLThcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcImZvbnQ6Ly8mI3hmMmMyO1xcXCIgd2lkdGg9XFxcIjI0XFxcIiBoZWlnaHQ9XFxcIjI0XFxcIi8+XFxuICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IG0tbC04IG0tdC04IG0tYi04XFxcIiBpZD1cXFwiZG9jdG9ycGVyaWRcXFwiIHRleHQ9XFxcInt7ZG9jdG9ycGVyaWR9fVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuXFxuICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS1sLTE2IG0tdC04IG0tYi04XFxcIiBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgc3JjPVxcXCJmb250Oi8vJiN4ZjA3MztcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIvPlxcbiAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjNcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCBtLWwtOCBtLXQtOCBtLWItOFxcXCIgaWQ9XFxcImN1cnJlbnRkYXRlXFxcIiB0ZXh0PVxcXCJ7e2N1cnJlbnRkYXRlfX1cXFwiIHN0eWxlPVxcXCJmb250LXNpemU6IDIwXFxcIj48L0xhYmVsPlxcbiAgICAgICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICAgICAgPGx2OlJhZExpc3RWaWV3IGlkPVxcXCJsaXN0Vmlld1xcXCIgY2xhc3M9XFxcIm0tbC04IG0tci04XFxcIiBpdGVtcz1cXFwie3sgY2xpbmljZGF0YSB9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRleHQ9XFxcInRyYW5zZmVyZGV0YWlsXFxcIiBzdHlsZT1cXFwiaGVpZ2h0OjUwJTsgd2lkdGg6MTAwJVxcXCI+XFxuICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3LnB1bGxUb1JlZnJlc2hTdHlsZT5cXG4gICAgICAgICAgICAgICAgPGx2OlB1bGxUb1JlZnJlc2hTdHlsZSBpbmRpY2F0b3JDb2xvcj1cXFwiIzRhNzc0ZVxcXCIgaW5kaWNhdG9yQmFja2dyb3VuZENvbG9yPVxcXCJ3aGl0ZVxcXCIvPlxcbiAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXcucHVsbFRvUmVmcmVzaFN0eWxlPlxcbiAgICAgICAgICAgIDxsdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwibGlzdHZpZXctY29udGVudFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIiBwYWRkaW5nPVxcXCI4XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcImF1dG8sIGF1dG8sIGF1dG9cXFwiIHJvd3M9XFxcImF1dG8sIGF1dG9cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwiZmFzXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgc3JjPVxcXCJmb250Oi8vJiN4ZjBmMTtcXFwiIHdpZHRoPVxcXCIyMFxcXCIgaGVpZ2h0PVxcXCIyMFxcXCIgY29sb3I9XFxcIiM0YTc3NGVcXFwiLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBtLWwtOFxcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIHRleHQ9XFxcInt7Y2xpbmljY29kZX19XFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZToxODtcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnRcXFwiIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIyXFxcIiB0ZXh0PVxcXCJ7e2NsaW5pY25hbWV9fVxcXCIgY29sb3I9XFxcIiM0YTc3NGVcXFwiIHN0eWxlPVxcXCJmb250LXNpemU6MTg7XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgPC9TdGFja0xheW91dD5cXG48L1BhZ2U+XCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS54bWxcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwibWFya3VwXCIsIHBhdGg6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLnhtbFwiIH0pO1xuICAgIH0pO1xufSAiLCJtb2R1bGUuZXhwb3J0cyA9IHtcInR5cGVcIjpcInN0eWxlc2hlZXRcIixcInN0eWxlc2hlZXRcIjp7XCJydWxlc1wiOltdLFwicGFyc2luZ0Vycm9yc1wiOltdfX07OyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5jc3NcIiB9KTtcbiAgICB9KTtcbn0gIiwiY29uc3Qgb2JzZXJ2YWJsZU1vZHVsZSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2RhdGEvb2JzZXJ2YWJsZVwiKTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5cbmxldCBwYWdlO1xubGV0IGNsb3NlQ2FsbGJhY2s7XG5sZXQgY3VyRGF0ZTtcbmxldCB0YWJpbmRleDtcblxuZXhwb3J0cy5vblNob3duTW9kYWxseSA9IChhcmdzKSA9PiB7XG4gICAgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgY2xvc2VDYWxsYmFjayA9IGFyZ3MuY2xvc2VDYWxsYmFjaztcbiAgICBjb25zdCBwYXRpZW50Qm9keSA9IGFyZ3MuY29udGV4dDtcbiAgICBjdXJEYXRlID0gcGF0aWVudEJvZHkuZGF0ZTtcbiAgICB0YWJpbmRleCA9IHBhdGllbnRCb2R5LnRhYjtcbiAgICAvLyBjbG9zZUNhbGxiYWNrID0gYXJncy5jbG9zZUNhbGxiYWNrO1xuICAgIC8vIGNvbnN0IHBhZ2UgPSBhcmdzLm9iamVjdDtcbiAgICAvLyBjb25zb2xlLmxvZyhjbGluaWNCb2R5KTtcbiAgICAvLyBsZXQgY2xpbmljZGF0YSA9IFtdO1xuICAgIC8vIGxldCBkb2N0b3JQZXJpZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5jX2RvY3Q7XG4gICAgLy8gbGV0IGRhdGVfZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5kYXRlX2Q7XG5cbiAgICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gb2JzZXJ2YWJsZU1vZHVsZS5mcm9tT2JqZWN0KHtcbiAgICAgICAgcGF0aWVudG5hbWU6IHBhdGllbnRCb2R5Lm5hbWUsXG4gICAgICAgIHBhdGllbnRobjogcGF0aWVudEJvZHkuaG4uc3BsaXQoXCJITlwiKVsxXSxcbiAgICAgICAgcXVldWV0aW1lOiBwYXRpZW50Qm9keS50aW1lLFxuICAgIH0pO1xufTtcblxuZXhwb3J0cy5vbkNsaWNrZWQgPSAoYXJncykgPT4ge1xuICAgIGxldCBzZXJ2aWNlID0gYXJncy5vYmplY3Quc2VydmljZTtcbiAgICBsZXQgaG5UZXh0ID0gcGFnZS5nZXRWaWV3QnlJZChcInBhdGllbnRoblwiKTtcbiAgICBsZXQgaG4gPSBoblRleHQudGV4dDtcblxuICAgIGxldCByZXN1bHQgPSB7XG4gICAgICAgIHN0YXR1c0NvZGU6IDIwMCxcbiAgICAgICAgc2VydmljZTogc2VydmljZSxcbiAgICAgICAgYWN0aW9uOiBcIlwiLFxuICAgICAgICBkYXRlOiBjdXJEYXRlLFxuICAgICAgICBobjogaG4sXG4gICAgICAgIGNvZGVfYjogYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiY2xpbmljY29kZVwiICsgdGFiaW5kZXgpLFxuICAgICAgICBjX2RvY3Q6IGFwcFNldHRpbmdzLmdldFN0cmluZyhcImRvY3RvclwiICsgdGFiaW5kZXgpLFxuICAgIH07XG5cbiAgICBjbG9zZUNhbGxiYWNrKHJlc3VsdCk7XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJcbm1vZHVsZS5leHBvcnRzID0gXCI8UGFnZSBzaG93bk1vZGFsbHk9XFxcIm9uU2hvd25Nb2RhbGx5XFxcIlxcbiAgICB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIj5cXG4gICAgPFN0YWNrTGF5b3V0IHdpZHRoPVxcXCIxMDAlXFxcIj5cXG4gICAgICAgIDwhLS0gPEdyaWRMYXlvdXQgcm93cz1cXFwiMTAwIDEwMFxcXCIgY29sdW1ucz1cXFwiKiAyKlxcXCI+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJ7e2lkfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiB0ZXh0PVxcXCJ7e25hbWV9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgIDwvR3JpZExheW91dD4gLS0+XFxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICA8IS0tIDxJbWFnZSBjbGFzcz1cXFwiZmFzIG0tdC04IG0tbC0xNiBtLXItOFxcXCIgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYyYzI7XFxcIiB3aWR0aD1cXFwiMjRcXFwiIGhlaWdodD1cXFwiMjRcXFwiIGNvbG9yPVxcXCIjNGE3NzRlXFxcIi8+IC0tPlxcbiAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IG0tdC04IG0tbC0xNlxcXCIgdGV4dD1cXFwi4LiK4Li34LmI4LitLeC4quC4geC4uOC4pTogXFxcIiBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IG0tdC04XFxcIiBpZD1cXFwicGF0aWVudG5hbWVcXFwiIHRleHQ9XFxcInt7cGF0aWVudG5hbWV9fVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiIGNvbG9yPVxcXCIjNGE3NzRlXFxcIj48L0xhYmVsPlxcbiAgICAgICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICAgICAgPEdyaWRMYXlvdXQgY2xhc3M9XFxcIm0tYi0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgYXV0bywgYXV0bywgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgPCEtLSA8SW1hZ2UgY2xhc3M9XFxcImZhcyBtLXQtOCBtLWwtMTYgbS1yLThcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcImZvbnQ6Ly8mI3hmMGY4O1xcXCIgd2lkdGg9XFxcIjI0XFxcIiBoZWlnaHQ9XFxcIjI0XFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCIvPiAtLT5cXG4gICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBtLXQtOCBtLWwtMTZcXFwiIHRleHQ9XFxcIkhOOiBcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOiAyMFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgbS10LThcXFwiIGlkPVxcXCJwYXRpZW50aG5cXFwiIHRleHQ9XFxcInt7cGF0aWVudGhufX1cXFwiIHN0eWxlPVxcXCJmb250LXNpemU6IDIwXFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCI+PC9MYWJlbD5cXG5cXG4gICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBtLXQtOCBtLWwtMTZcXFwiIHRleHQ9XFxcIuC5gOC4p+C4peC4sjogXFxcIiBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPCEtLSA8SW1hZ2UgY2xhc3M9XFxcImZhcyBtLXQtOCBtLWwtMTYgbS1yLThcXFwiIGNvbD1cXFwiMlxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcImZvbnQ6Ly8mI3hmMDE3O1xcXCIgd2lkdGg9XFxcIjI0XFxcIiBoZWlnaHQ9XFxcIjI0XFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCIvPiAtLT5cXG4gICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIzXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgbS10LThcXFwiIGlkPVxcXCJxdWV1ZXRpbWVcXFwiIHRleHQ9XFxcInt7cXVldWV0aW1lfX1cXFwiIHN0eWxlPVxcXCJmb250LXNpemU6IDIwXFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCI+PC9MYWJlbD5cXG4gICAgICAgIDwvR3JpZExheW91dD5cXG5cXG4gICAgICAgIDxHcmlkTGF5b3V0IGNsYXNzPVxcXCJtLWItOFxcXCIgY29sdW1ucz1cXFwiKlxcXCIgcm93cz1cXFwiYXV0bywgYXV0b1xcXCI+XFxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cXFwidGV4dC1mb250IHAtbC0xNiBwLXItMTZcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiB0ZXh0PVxcXCLguKPguK3guJ7guJrguYHguJ7guJfguKLguYxcXFwiIGJvcmRlclJhZGl1cz1cXFwiOFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjRDQ1RDc5XFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25DbGlja2VkXFxcIiBzZXJ2aWNlPVxcXCJkb2N0b3JjbG9zZXRpbWVcXFwiIGZvbnRTaXplPVxcXCIxOFxcXCI+PC9idXR0b24+XFxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cXFwidGV4dC1mb250IHAtbC0xNiBwLXItMTZcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIxXFxcIiB0ZXh0PVxcXCLguKPguK3guJ7guJrguJ7guKLguLLguJrguLLguKVcXFwiIGJvcmRlclJhZGl1cz1cXFwiOFxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjNEJBRUEwXFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiIHRhcD1cXFwib25DbGlja2VkXFxcIiBzZXJ2aWNlPVxcXCJudXJzZWNsb3NldGltZVxcXCIgZm9udFNpemU9XFxcIjE4XFxcIj48L2J1dHRvbj5cXG4gICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgPC9TdGFja0xheW91dD5cXG48L1BhZ2U+XCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLnhtbFwiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJtYXJrdXBcIiwgcGF0aDogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC54bWxcIiB9KTtcbiAgICB9KTtcbn0gIl0sInNvdXJjZVJvb3QiOiIifQ==