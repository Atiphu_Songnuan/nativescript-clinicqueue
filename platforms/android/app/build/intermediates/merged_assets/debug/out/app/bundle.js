require("./runtime.js");require("./vendor.js");module.exports =
(global["webpackJsonp"] = global["webpackJsonp"] || []).push([["bundle"],{

/***/ "./ sync ^\\.\\/app\\.(css|scss|less|sass)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app.css": "./app.css"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync ^\\.\\/app\\.(css|scss|less|sass)$";

/***/ }),

/***/ "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app-root.xml": "./app-root.xml",
	"./app.css": "./app.css",
	"./app.js": "./app.js",
	"./configs.js": "./configs.js",
	"./liveUpdate.js": "./liveUpdate.js",
	"./views/main/main-page-model.js": "./views/main/main-page-model.js",
	"./views/main/main-page.css": "./views/main/main-page.css",
	"./views/main/main-page.js": "./views/main/main-page.js",
	"./views/main/main-page.xml": "./views/main/main-page.xml",
	"./views/modal/modal-page.css": "./views/modal/modal-page.css",
	"./views/modal/modal-page.js": "./views/modal/modal-page.js",
	"./views/modal/modal-page.xml": "./views/modal/modal-page.xml",
	"./views/patient/patient-modal.css": "./views/patient/patient-modal.css",
	"./views/patient/patient-modal.js": "./views/patient/patient-modal.js",
	"./views/patient/patient-modal.xml": "./views/patient/patient-modal.xml"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) { // check for number or string
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return id;
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$";

/***/ }),

/***/ "./app-root.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Frame defaultPage=\"/views/main/main-page\">\n</Frame>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app-root.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./app-root.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("~@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("@nativescript/theme/css/core.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/core.css"));
global.registerModule("~@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));
global.registerModule("@nativescript/theme/css/default.css", () => __webpack_require__("../node_modules/@nativescript/theme/css/default.css"));module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"comment","comment":"\nIn NativeScript, the app.css file is where you place CSS rules that\nyou would like to apply to your entire application. Check out\nhttp://docs.nativescript.org/ui/styling for a full list of the CSS\nselectors and properties you can use to style UI components.\n\n/*\nIn many cases you may want to use the NativeScript core theme instead\nof writing your own CSS rules. You can learn more about the \nNativeScript core theme at https://github.com/nativescript/theme\nThe imported CSS rules must precede all other types of rules.\n"},{"type":"import","import":"\"~@nativescript/theme/css/core.css\""},{"type":"import","import":"\"~@nativescript/theme/css/default.css\""},{"type":"comment","comment":" Place any CSS rules you want to apply on both iOS and Android here.\nThis is where the vast majority of your CSS code goes. "},{"type":"comment","comment":"\nThe following CSS rule changes the font size of all Buttons that have the\n\"-primary\" class modifier.\n"},{"type":"rule","selectors":[".btn"],"declarations":[{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".text-font"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Medium\""}]},{"type":"rule","selectors":[".fas"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-solid-900\""},{"type":"declaration","property":"font-weight","value":"900"},{"type":"comment","comment":" font-size: 10; "}]},{"type":"rule","selectors":[".far"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Font Awesome 5 Free\", \"fa-regular-400\""},{"type":"declaration","property":"font-weight","value":"400"},{"type":"comment","comment":" font-size: 10; "}]},{"type":"rule","selectors":[".header"],"declarations":[{"type":"declaration","property":"color","value":"white"},{"type":"declaration","property":"text-align","value":"center"},{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".nodata-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"22"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./app.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./app.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
        let applicationCheckPlatform = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
        if (applicationCheckPlatform.android && !global["__snapshot"]) {
            __webpack_require__("../node_modules/@nativescript/core/ui/frame/frame.js");
__webpack_require__("../node_modules/@nativescript/core/ui/frame/activity.js");
        }

        
            __webpack_require__("../node_modules/nativescript-dev-webpack/load-application-css-regular.js")();
            
            
        if (true) {
            const hmrUpdate = __webpack_require__("../node_modules/nativescript-dev-webpack/hmr/index.js").hmrUpdate;
            global.__coreModulesLiveSync = global.__onLiveSync;

            global.__onLiveSync = function () {
                // handle hot updated on LiveSync
                hmrUpdate();
            };

            global.hmrRefresh = function({ type, path } = {}) {
                // the hot updates are applied, ask the modules to apply the changes
                setTimeout(() => {
                    global.__coreModulesLiveSync({ type, path });
                });
            };

            // handle hot updated on initial app start
            hmrUpdate();
        }
        
            const context = __webpack_require__("./ sync recursive (?<!\\bApp_Resources\\b.*)(?<!\\.\\/\\btests\\b\\/.*?)\\.(xml|css|js|kt|(?<!\\.d\\.)ts|(?<!\\b_[\\w-]*\\.)scss)$");
            global.registerWebpackModules(context);
            if (true) {
                module.hot.accept(context.id, () => { 
                    console.log("HMR: Accept module '" + context.id + "' from '" + module.i + "'"); 
                });
            }
            
        __webpack_require__("../node_modules/@nativescript/core/bundle-entry-points.js");
        let application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const LiveUpdate = __webpack_require__("./liveUpdate.js");
global.conf = __webpack_require__("./configs.js");

new LiveUpdate(global.conf.updateKey);

let token = appSettings.getString("token", "tokenexpired");
// console.log("Root-User Token: " + token);
if (token != "tokenexpired") {
    application.run({ moduleName: "app-root-main" });
} else {
    application.run({ moduleName: "app-root" });
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./app.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./app.js" });
    });
} 
    
        
        
    
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./configs.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {config = {
    name: "Clinic Queue",
    ver: "Version 1.0 (build 010720)",

    api: {
        isdoctor: "http://61.19.201.20:19539/clinicqueue/doctor/isdoctor",
        doctorclinic: "http://61.19.201.20:19539/clinicqueue/doctor/clinic",
        doctorqueue: "http://61.19.201.20:19539/clinicqueue/doctor/queue",
        patientinfo: "http://61.19.201.20:19539/clinicqueue/doctor/patientinfo",
    },

    updateKey: {
        android: "c0pxsa4DBl1ipZJHG6hjhz94LSwZpbVYic0w5"
    }
};

module.exports = config;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./configs.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./configs.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./liveUpdate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const application = __webpack_require__("../node_modules/@nativescript/core/application/application.js");
const AppSync = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").AppSync;
const InstallMode = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").InstallMode;
const SyncStatus  = __webpack_require__("../node_modules/nativescript-app-sync/app-sync.js").SyncStatus ;
const platform = __webpack_require__("../node_modules/@nativescript/core/platform/platform.js");

function liveUpdate(config) {
    if (platform.isIOS) {
        console.log("IOS device token: " + config.ios);
    } else {
        console.log("Android device token: " + config.android);
    }
    application.on(application.resumeEvent, function() {
        AppSync.sync({
            enabledWhenUsingHmr: false,
            deploymentKey: platform.isIOS ? config.ios : config.android,
            installMode: InstallMode.IMMEDIATE,
            mandatoryInstallMode: platform.isIOS
                ? InstallMode.ON_NEXT_RESUME
                : InstallMode.IMMEDIATE,
            updateDialog: {
                optionalUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                updateTitle: "อัพเดทเวอร์ชันใหม่",
                mandatoryUpdateMessage: "รีสตาร์ทเพื่ออัพเดท",
                optionalIgnoreButtonLabel: "ยกเลิก",
                mandatoryContinueButtonLabel: platform.isIOS
                    ? "ปิด"
                    : "รีสตาร์ท",
                appendReleaseDescription: true
            }
        },
         (syncStatus) => {
            if (syncStatus === SyncStatus.UP_TO_DATE) {
              console.log("No pending updates; you’re running the latest version.");
            } else if (syncStatus === SyncStatus.UPDATE_INSTALLED) {
              console.log("Update found and installed. It will be activated upon next cold reboot");
            }
         });
    });
}

module.exports = liveUpdate;
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./liveUpdate.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./liveUpdate.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./package.json":
/***/ (function(module) {

module.exports = {"main":"app.js","android":{"v8Flags":"--expose_gc","markingMode":"none"}};

/***/ }),

/***/ "./views/main/main-page-model.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const Observable = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").Observable;
const http = __webpack_require__("../node_modules/@nativescript/core/http/http.js");

function DoctorData() {
    let viewModel = new Observable();
    let result = new Array();
    // console.log(global.conf.api.doctorclinic);
    viewModel.getDoctorInfo = (perid) => {
        return http
            .request({
                url: global.conf.api.isdoctor,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.length == 0) {
                        result = {
                            statusCode: 400,
                            data: res,
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };
    viewModel.getClinic = (perid, curdate) => {
        return http
            .request({
                url: global.conf.api.doctorclinic,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    perid: perid,
                    date_d: curdate,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.length == 0) {
                        result = {
                            statusCode: 400,
                            data: res,
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getDoctorQueue = (curdate, cliniccode, doctorperid) => {
        return http
            .request({
                url: global.conf.api.doctorqueue,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    date_d: curdate,
                    code_b: cliniccode,
                    c_doct: doctorperid,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.length == 0) {
                        result = {
                            statusCode: 400,
                            data: res,
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };

    viewModel.getPatientInfo = (hn) => {
        return http
            .request({
                url: global.conf.api.patientinfo,
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                content: JSON.stringify({
                    hn: hn,
                }),
            })
            .then(
                (response) => {
                    let res = response.content.toJSON();
                    if (res.length == 0) {
                        result = {
                            statusCode: 400,
                            data: res,
                        };
                    } else {
                        result = {
                            statusCode: 200,
                            data: res.data,
                        };
                    }
                    return result;
                },
                (e) => {
                    console.log(e);
                    result = {
                        statusCode: 401,
                        data: JSON.stringify(e),
                    };
                    return result;
                }
            );
    };
    return viewModel;
}

module.exports = {
    DoctorData: DoctorData,
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page-model.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main/main-page-model.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":["TabStrip"],"declarations":[{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"color","value":"darkgray"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem"],"declarations":[{"type":"declaration","property":"color","value":"darkgray"},{"type":"declaration","property":"background-color","value":"white"},{"type":"declaration","property":"margin-top","value":"4"}]},{"type":"rule","selectors":["TabStripItem.tabstripitem:active"],"declarations":[{"type":"declaration","property":"color","value":"#1c8445"},{"type":"declaration","property":"background-color","value":"mintcream"},{"type":"declaration","property":"margin-top","value":"4"}]},{"type":"rule","selectors":[".text-name-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Bold\""},{"type":"declaration","property":"font-size","value":"18"}]},{"type":"rule","selectors":[".text-label"],"declarations":[{"type":"declaration","property":"font-family","value":"\"Kanit\", \"Kanit-Light\""},{"type":"declaration","property":"font-size","value":"14"}]},{"type":"rule","selectors":[".card-doctor-profile"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"comment","comment":" background-color: white; "},{"type":"declaration","property":"background-image","value":"url(\"~/src/img/doctor-card.png\")"},{"type":"declaration","property":"background-repeat","value":"no-repeat"},{"type":"declaration","property":"background-size","value":"cover"},{"type":"declaration","property":"background-position","value":"top"},{"type":"declaration","property":"color","value":"#454058"}]},{"type":"rule","selectors":[".listview-patient"],"declarations":[{"type":"comment","comment":" margin-left: 8;\n  margin-right: 8;\n  margin-bottom: 8; "},{"type":"declaration","property":"border-radius","value":"30 30 0 0"},{"type":"declaration","property":"background-color","value":"#fee6d2"},{"type":"comment","comment":" color: white; "}]},{"type":"rule","selectors":[".listview-item"],"declarations":[{"type":"comment","comment":" margin-top: 16;\n  margin-left: 16;\n  margin-right: 16;\n  margin-bottom: 16; "},{"type":"comment","comment":" margin-top:8 ;\n  margin-left: 16;\n  margin-right: 16; "},{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"background-color","value":"white"}]},{"type":"rule","selectors":[".patient-image"],"declarations":[{"type":"declaration","property":"margin-left","value":"8"},{"type":"declaration","property":"border-radius","value":"50%"},{"type":"declaration","property":"background-color","value":"crimson"}]},{"type":"rule","selectors":[".add-doctor-btn"],"declarations":[{"type":"declaration","property":"border-radius","value":"20"},{"type":"declaration","property":"padding","value":"16"},{"type":"declaration","property":"background-color","value":"darkorange"},{"type":"declaration","property":"color","value":"white"}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/main/main-page.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const view = __webpack_require__("../node_modules/@nativescript/core/ui/core/view/view.js");
const MainPageModel = __webpack_require__("./views/main/main-page-model.js");
const fromObject = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js").fromObject;
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");
const platform = __webpack_require__("../node_modules/@nativescript/core/platform/platform.js");
const dialogs = __webpack_require__("../node_modules/@nativescript/core/ui/dialogs/dialogs.js");
const nstoasts = __webpack_require__("../node_modules/nativescript-toasts/index.js");

const modalViewModule = "/views/modal/modal-page.js";
const patientViewModule = "/views/patient/patient-modal.js";

let doctor = MainPageModel.DoctorData();
let page;
let modalView;
let newDoctorBtn;

// Doctor1
let data1 = [];
let btnDoctor1;
let doctor1;

// Doctor2
let data2 = [];
let btnDoctor2;
let doctor2;

// Doctor3
let data3 = [];
let btnDoctor3;
let doctor3;

exports.onPageLoaded = (args) => {
    page = args.object.page;
    modalView = args.object;
    newDoctorBtn = page.getViewById("newdoctorbtn");
    if (platform.isIOS) {
        newDoctorBtn.style = "font-size: 18em;";
    } else {
        newDoctorBtn.style = "font-size: 14em;";
    }
};

exports.addDoctor1 = (args) => {
    btnDoctor1 = page.getViewById("btnDoctor1");
    doctor1 = page.getViewById("doctor1");

    dialogs
        .prompt({
            title: "กรอกรหัสแพทย์",
            message: "",
            okButtonText: "ตกลง",
            cancelButtonText: "ยกเลิก",
            cancelable: false,
            inputType: dialogs.inputType.number,
        })
        .then((r) => {
            if (r.result) {
                let perid = r.text;
                let curdate = getDateString(new Date());
                let curDateStr =
                    curdate.year + "-" + curdate.month + "-" + curdate.day;
                // console.log("Perid: " + perid);
                // console.log("Curdate: " + curDateStr);
                getDoctorInfo("new", "1", "1349");
            } else {
                console.log("ยกเลิก");
            }
        });
};

exports.onPullToRefreshInitiated1 = (args) => {
    let perid = appSettings.getString("doctor1");
    let listView1 = page.getViewById("listview1");
    getDoctorInfo("reload", "1", perid);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView1.notifyPullToRefreshFinished();
    }, 500);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    let listviewIndex = args.object.index;
    let listView = page.getViewById("listview" + listviewIndex);
    let selectedItems = listView.getSelectedItems();
    // console.log(selectedItems);
    let patientdata = selectedItems[0];
    let pName = patientdata["name" + listviewIndex];
    let pHN = patientdata["hn" + listviewIndex];
    let qTime = patientdata["qtime" + listviewIndex];
    const option = {
        context: {
            name: pName,
            hn: pHN,
            time: qTime,
        },
        closeCallback: (patientinfo) => {
            if (typeof patientinfo == "object") {
                console.log(patientinfo);
            } else {
                console.log("close modal");
            }
        },
        fullscreen: false,
        animated: true,
    };
    modalView.showModal(patientViewModule, option);
    // switch (listviewIndex) {
    //     case "1":
    //         console.log("ListView 1");
    //         var selectedItems = listView.getSelectedItems();
    //         console.log(selectedItems);
    //         break;
    //     case "2":
    //         console.log("ListView 2");
    //         break;
    //     default:
    //         break;
    // }
};

exports.addDoctor2 = () => {
    btnDoctor2 = page.getViewById("btnDoctor2");
    doctor2 = page.getViewById("doctor2");

    dialogs
        .prompt({
            title: "กรอกรหัสแพทย์",
            message: "",
            okButtonText: "ตกลง",
            cancelButtonText: "ยกเลิก",
            cancelable: false,
            inputType: dialogs.inputType.number,
        })
        .then((r) => {
            if (r.result) {
                let perid = r.text;
                let curdate = getDateString(new Date());
                let curDateStr =
                    curdate.year + "-" + curdate.month + "-" + curdate.day;
                // console.log("Perid: " + perid);
                // console.log("Curdate: " + curDateStr);
                getDoctorInfo("new", "2", "11400");
            } else {
                console.log("ยกเลิก");
            }
        });
};

exports.onPullToRefreshInitiated2 = () => {
    let perid = appSettings.getString("doctor2");
    let listView2 = page.getViewById("listview2");
    getDoctorInfo("reload", "2", perid);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView2.notifyPullToRefreshFinished();
    }, 500);
};

exports.addDoctor3 = () => {
    btnDoctor3 = page.getViewById("btnDoctor3");
    doctor3 = page.getViewById("doctor3");

    dialogs
        .prompt({
            title: "กรอกรหัสแพทย์",
            message: "",
            okButtonText: "ตกลง",
            cancelButtonText: "ยกเลิก",
            cancelable: false,
            inputType: dialogs.inputType.number,
        })
        .then((r) => {
            if (r.result) {
                let perid = r.text;
                let curdate = getDateString(new Date());
                let curDateStr =
                    curdate.year + "-" + curdate.month + "-" + curdate.day;
                // console.log("Perid: " + perid);
                // console.log("Curdate: " + curDateStr);
                getDoctorInfo("new", "3", "11400");
            } else {
                console.log("ยกเลิก");
            }
        });
};

exports.onPullToRefreshInitiated3 = () => {
    let perid = appSettings.getString("doctor3");
    let listView3 = page.getViewById("listview3");
    getDoctorInfo("reload", "3", perid);
    let options = {
        text: "อัพเดทข้อมูลสำเร็จ",
        duration: nstoasts.DURATION.SHORT,
        position: nstoasts.POSITION.BOTTOM, //optional
    };
    nstoasts.show(options);
    setTimeout(() => {
        listView3.notifyPullToRefreshFinished();
    }, 500);
};

// Step 1 ตรวจสอบข้อมูลแพทย์
function getDoctorInfo(action, tab, perid) {
    let doctorInfoProms = new Promise((resolve, reject) => {
        doctor
            .getDoctorInfo(perid)
            .catch((err) => {
                console.log(err);
                reject(err);
            })
            .then((inforesponse) => {
                let info = inforesponse.data;
                if (info.length != 0) {
                    appSettings.remove("doctor" + tab);
                    appSettings.setString("doctor" + tab, perid);
                    // let doctorname = doctorinfo[0].doctorname;
                    // let doctorsex = doctorinfo[0].sex;
                    let doctorinfo = {
                        doctorname: info[0].doctorname,
                        sex: info[0].sex,
                    };
                    resolve(doctorinfo);
                } else {
                    console.log("ไม่พบข้อมูลแพทย์");
                }
            });
    });
    doctorInfoProms.then((doctorinfo) => {
        getDoctorClinicInfo(action, tab, perid, "2020-07-05", doctorinfo);
    });
}

// Step 2 ตรวจสอบข้อมูลคลินิกที่เลือก
function getDoctorClinicInfo(action, tab, perid, curdate, doctorinfo) {
    let clinicInfoProms = new Promise((resolve, reject) => {
        doctor
            .getClinic(perid, curdate)
            .catch((err) => {
                console.log(err);
                reject(err);
                return Promise.reject();
            })
            .then((response) => {
                let clinicList = response.data;
                if (clinicList.length != 0) {
                    resolve(clinicList);
                } else {
                    resolve([]);
                    console.log("ไม่มีคลินิก");
                }
            });
    });
    clinicInfoProms.then((clinicinfo) => {
        getDoctorQueue(action, tab, clinicinfo, doctorinfo);
    });
}

// Step 3 ดึงข้อมูลคิวตรวจของแพทย์ในคลินิกนั้นๆ
function getDoctorQueue(action, tab, clinicinfo, doctorinfo) {
    if (clinicinfo.length != 0) {
        // const mainView = args.object;
        switch (action) {
            case "new":
                const option = {
                    context: {
                        tab: tab,
                        cliniclist: clinicinfo,
                    },
                    closeCallback: (doctorqueueinfo) => {
                        if (typeof doctorqueueinfo == "object") {
                            let queuelist = doctorqueueinfo.queueList;
                            setQueueList(
                                action,
                                tab,
                                doctorinfo,
                                doctorqueueinfo,
                                queuelist
                            );
                        } else {
                            console.log("close modal");
                        }
                    },
                    fullscreen: false,
                    animated: true,
                };
                modalView.showModal(modalViewModule, option);
                break;
            case "reload":
                let doctorPerid = appSettings.getString("doctor" + tab);
                let clinicCode = appSettings.getString("cliniccode" + tab);
                let clinicName = appSettings.getString("clinicname" + tab);
                doctor
                    .getDoctorQueue("2020-07-12", clinicCode, doctorPerid)
                    .catch((err) => {
                        console.log(err);
                        reject(err);
                    })
                    .then((clinicqueueresponse) => {
                        let queuelist = clinicqueueresponse.data;
                        let doctorqueueinfo;
                        if (queuelist.length != 0) {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: clinicName,
                                queueList: queuelist,
                            };
                        } else {
                            doctorqueueinfo = {
                                clinicCode: clinicCode,
                                clinicName: "",
                                queueList: [],
                            };
                        }
                        setQueueList(
                            action,
                            tab,
                            doctorinfo,
                            doctorqueueinfo,
                            queuelist
                        );
                    });
                break;
            default:
                break;
        }
    } else {
        console.log("ไม่พบข้อมูลคลินิก");
    }
}

// Step 4 เช็ค action ว่าเป็น new หรือ reload
function setQueueList(action, tab, doctorinfo, doctorqueueinfo, queuelist) {
    let data = [];
    if (queuelist.length != 0) {
        queuelist.forEach((element) => {
            let name = "name" + tab;
            let hn = "hn" + tab;
            let qtime = "qtime" + tab;
            let patientimg = "patientimg" + tab;
            let patientimgPath = "";
            switch (element.sex) {
                case "1":
                    patientimgPath = "~/src/img/man-patient.png";
                    break;
                case "2":
                    patientimgPath = "~/src/img/woman-patient.png";
                    break;
                default:
                    break;
            }
            data.push({
                [name]: element.patientname,
                [hn]: "HN" + element.hn,
                [qtime]: element.q_time,
                [patientimg]: patientimgPath,
            });
        });
        if (action == "new") {
            let cName = doctorqueueinfo.clinicName;
            let profileimg = page.getViewById("profileimg" + tab);
            let doctorname = page.getViewById("doctorname" + tab);
            let clinicname = page.getViewById("clinicname" + tab);

            let dName = doctorinfo.doctorname;
            let dSex = doctorinfo.sex;
            // Check doctor sex to set default profile image
            switch (dSex) {
                case "1":
                    profileimg.src = "~/src/img/woman-doctor.png";
                    break;
                case "2":
                    profileimg.src = "~/src/img/man-doctor.png";
                    break;
                default:
                    break;
            }

            doctorname.text = dName;
            clinicname.text = "คลินิก" + cName.trim();
        }
        switch (tab) {
            case "1":
                data1 = data;
                break;
            case "2":
                data2 = data;
                break;
            case "3":
                data3 = data;
            default:
                break;
        }
        setRadListView(tab);
    } else {
        console.log("ไม่พบข้อมูลคิวตรวจ");
    }
}

// Step 5 นำข้อมูลคิวใส่ใน list
function setRadListView(tab) {
    const vm = fromObject({
        dataItems1: data1,
        dataItems2: data2,
        dataItems3: data3,
    });
    page.bindingContext = vm;

    switch (tab) {
        case "1":
            btnDoctor1.visibility = "collapse";
            btnDoctor1.animate({
                opacity: 0,
                duration: 1000,
            });

            doctor1.visibility = "visible";
            doctor1.animate({
                opacity: 1,
                duration: 1000,
            });
            break;
        case "2":
            btnDoctor2.visibility = "collapse";
            btnDoctor2.animate({
                opacity: 0,
                duration: 1000,
            });

            doctor2.visibility = "visible";
            doctor2.animate({
                opacity: 1,
                duration: 1000,
            });
            break;
        case "3":
            btnDoctor3.visibility = "collapse";
            btnDoctor3.animate({
                opacity: 0,
                duration: 1000,
            });

            doctor3.visibility = "visible";
            doctor3.animate({
                opacity: 1,
                duration: 1000,
            });
            break;
        default:
            break;
    }
}

function getDateString(date) {
    let monthStr;
    let dayStr;
    if (parseInt(date.getMonth() + 1) <= 9) {
        monthStr = "0" + (date.getMonth() + 1);
    } else {
        monthStr = date.getMonth() + 1;
    }

    if (parseInt(date.getDate()) <= 9) {
        dayStr = "0" + date.getDate();
    } else {
        dayStr = date.getDate();
    }

    let newDateFormat = {
        year: date.getFullYear(),
        month: monthStr,
        day: dayStr,
    };

    return newDateFormat;
}
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/main/main-page.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/main/main-page.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplate", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });

module.exports = "<Page xmlns=\"http://schemas.nativescript.org/tns.xsd\" \n    xmlns:lv=\"nativescript-ui-listview\" loaded=\"onPageLoaded\">\n\n    <Page.actionBar>\n        <ActionBar backgroundColor=\"#1e4b25\" color=\"white\">\n            <Label class=\"header\" id=\"header\" text=\"คิวตรวจทั้งหมด\"></Label>\n            <ActionItem class=\"fas\" id=\"newdoctorbtn\" icon=\"font://&#xf055;\" ios.position=\"right\" />\n        </ActionBar>\n    </Page.actionBar>\n\n    <BottomNavigation id=\"bottomNav\" selectedIndex=\"0\">\n        <TabStrip>\n            <TabStripItem class=\"fas\" tap=\"doctorqueue\">\n                <Image src=\"font://&#xf0f0;\"></Image>\n                <Label class=\"text-font\" text=\"แพทย์\" fontSize=\"14\"></Label>\n            </TabStripItem>\n\n            <TabStripItem class=\"tabstripitem fas\" tap=\"settings\">\n                <Image src=\"font://&#xf085;\"></Image>\n                <Label class=\"text-font\" text=\"ตั้งค่า\" fontSize=\"14\"></Label>\n            </TabStripItem>\n        </TabStrip>\n\n        <TabContentItem>\n            <GridLayout>\n                <Tabs id=\"doctortabs\" selectedIndex=\"0\" selectedIndexChanged=\"onIndexSelected\">\n                    <TabStrip>\n                        <TabStripItem>\n                            <Label class=\"text-label\" text=\"แพทย์ 1\"></Label>\n                        </TabStripItem>\n                        <TabStripItem>\n                            <Label class=\"text-label\" text=\"แพทย์ 2\"></Label>\n                        </TabStripItem>\n                        <TabStripItem>\n                            <Label class=\"text-label\" text=\"แพทย์ 3\"></Label>\n                        </TabStripItem>\n                    </TabStrip>\n\n                    <TabContentItem>\n                        <StackLayout>\n                            <FlexboxLayout id=\"btnDoctor1\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                                <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                                    <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 1\" tap=\"addDoctor1\"></Button>\n                                </GridLayout>\n                            </FlexboxLayout>\n                            <StackLayout id=\"doctor1\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                                <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                                    <GridLayout columns=\"auto, auto\" rows=\"auto\">\n                                        <Image col=\"0\" row=\"0\" id=\"profileimg1\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                        <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                            <Label class=\"text-name-label text-left\" id=\"doctorname1\"></Label>\n                                            <Label class=\"text-label text-left\" id=\"clinicname1\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"ห้องตรวจ\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"จำนวนผู้ป่วย\"></Label>\n                                        </StackLayout>\n                                    </GridLayout>\n                                </StackLayout>\n\n                                <lv:RadListView id=\"listview1\" class=\"listview-patient p-b-16\" items=\"{{dataItems1}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated1\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" index=\"1\">\n                                    <lv:RadListView.itemTemplate>\n                                        <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *, auto\" rows=\"auto\">\n                                            <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg1}}\" width=\"60\" height=\"60\"/>\n                                            <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                                <Label class=\"text-font p-l-8 p-r-8 p-b-8\" fontSize=\"20\" text=\"{{name1}}\"></Label>\n                                                <Label class=\"text-font p-l-8 p-r-8 p-t-8\" fontSize=\"16\" text=\"{{hn1}}\"></Label>\n                                            </StackLayout>\n                                            <GridLayout col=\"2\" row=\"0\" verticalAlignment=\"center\">\n                                                <Label class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime1}}\"></Label>\n                                            </GridLayout>\n                                        </GridLayout>\n                                    </lv:RadListView.itemTemplate>\n                                </lv:RadListView>\n                            </StackLayout>\n                        </StackLayout>\n                    </TabContentItem>\n\n                    <TabContentItem>\n                        <StackLayout>\n                            <FlexboxLayout id=\"btnDoctor2\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                                <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                                    <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 2\" tap=\"addDoctor2\"></Button>\n                                </GridLayout>\n                            </FlexboxLayout>\n                            <StackLayout id=\"doctor2\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                                <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                                    <GridLayout columns=\"auto, auto\" rows=\"auto\">\n                                        <Image col=\"0\" row=\"0\" id=\"profileimg2\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                        <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                            <Label class=\"text-name-label text-left\" id=\"doctorname2\"></Label>\n                                            <Label class=\"text-label text-left\" id=\"clinicname2\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"ห้องตรวจ\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"จำนวนผู้ป่วย\"></Label>\n                                        </StackLayout>\n                                    </GridLayout>\n                                </StackLayout>\n\n                                <lv:RadListView id=\"listview2\" class=\"listview-patient p-b-16\" items=\"{{dataItems2}}\" height=\"100%\" pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated2\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" index=\"2\">\n                                    <lv:RadListView.itemTemplate>\n                                        <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *, auto\" rows=\"auto\">\n                                            <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg2}}\" width=\"60\" height=\"60\"/>\n                                            <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                                <Label class=\"text-font p-l-8 p-r-8 p-b-8\" fontSize=\"20\" text=\"{{name2}}\"></Label>\n                                                <Label class=\"text-font p-l-8 p-r-8 p-t-8\" fontSize=\"16\" text=\"{{hn2}}\"></Label>\n                                            </StackLayout>\n                                            <GridLayout col=\"2\" row=\"0\" verticalAlignment=\"center\">\n                                                <Label class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime2}}\"></Label>\n                                            </GridLayout>\n                                        </GridLayout>\n                                    </lv:RadListView.itemTemplate>\n                                </lv:RadListView>\n                            </StackLayout>\n                        </StackLayout>\n                    </TabContentItem>\n\n                    <TabContentItem>\n                        <StackLayout>\n                            <FlexboxLayout id=\"btnDoctor3\" horizontalAlignment=\"center\" height=\"100%\" visibility=\"visible\" opacity=\"1\">\n                                <GridLayout rows=\"*\" columns=\"auto\" verticalAlignment=\"center\">\n                                    <Button class=\"text-label add-doctor-btn\" text=\"เพิ่มแพทย์ 3\" tap=\"addDoctor3\"></Button>\n                                </GridLayout>\n                            </FlexboxLayout>\n                            <StackLayout id=\"doctor3\" backgroundColor=\"#eee\" height=\"100%\" visibility=\"collapse\" opacity=\"0\">\n                                <StackLayout class=\"card-doctor-profile m-16 p-8 text-center\">\n                                    <GridLayout columns=\"auto, auto\" rows=\"auto\">\n                                        <Image col=\"0\" row=\"0\" id=\"profileimg3\" width=\"120\" height=\"120\" stretch=\"aspectFill\"/>\n                                        <StackLayout col=\"1\" row=\"0\" class=\"m-16\" verticalAlignment=\"center\">\n                                            <Label class=\"text-name-label text-left\" id=\"doctorname3\"></Label>\n                                            <Label class=\"text-label text-left\" id=\"clinicname3\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"ห้องตรวจ\"></Label>\n                                            <Label class=\"text-label text-left\" text=\"จำนวนผู้ป่วย\"></Label>\n                                        </StackLayout>\n                                    </GridLayout>\n                                </StackLayout>\n\n                                <lv:RadListView id=\"listview3\" class=\"listview-patient p-b-16\" items=\"{{dataItems3}}\" height=\"100%\"  pullToRefresh=\"true\" pullToRefreshInitiated=\"onPullToRefreshInitiated3\">\n                                    <lv:RadListView.itemTemplate>\n                                        <GridLayout class=\"listview-item m-t-16 m-r-16 m-l-16\" columns=\"auto, *, auto\" rows=\"auto\">\n                                            <Image col=\"0\" row=\"0\" class=\"patient-image\" src=\"{{patientimg3}}\" width=\"60\" height=\"60\"/>\n                                             <StackLayout col=\"1\" row=\"0\" orientation=\"vertical\">\n                                                <Label class=\"text-font p-l-8 p-r-8 p-b-8\" fontSize=\"20\" text=\"{{name3}}\"></Label>\n                                                <Label class=\"text-font p-l-8 p-r-8 p-t-8\" fontSize=\"16\" text=\"{{hn3}}\"></Label>\n                                            </StackLayout>\n                                            <GridLayout col=\"2\" row=\"0\" verticalAlignment=\"center\">\n                                                <Label class=\"text-font text-right m-r-16\" fontSize=\"16\" text=\"{{qtime3}}\"></Label>\n                                            </GridLayout>\n                                        </GridLayout>\n                                    </lv:RadListView.itemTemplate>\n                                </lv:RadListView>\n                            </StackLayout>\n                        </StackLayout>\n                    </TabContentItem>\n                </Tabs>\n            </GridLayout>\n        </TabContentItem>\n\n        <TabContentItem backgroundColor=\"#eee\">\n            <ScrollView orientation=\"vertical\">\n                <FlexboxLayout flexDirection=\"column\">\n                    <!-- <RL:Ripple backgroundColor=\"#fff\" height=\"100\" borderBottomColor=\"lightgray\"> -->\n                    <GridLayout backgroundColor=\"#fff\" columns=\"auto,auto\" rows=\"*\" borderBottomColor=\"lightgray\">\n                        <Image id=\"settingprofileimg\" class=\"m-8\" row=\"0\" col=\"0\" src=\"{{profile}}\" width=\"65\" height=\"65\" stretch=\"aspectFill\" horizontalAlignment=\"left\"/>\n                        <StackLayout row=\"0\" col=\"1\" verticalAlignment=\"center\">\n                            <Label class=\"text-name-label\" text=\"ชื่อ-นามสกุล\" colSpan=\"2\" />\n                            <Label class=\"text-label\" text=\"หน่วยงาน\" colSpan=\"2\" />\n                        </StackLayout>\n                    </GridLayout>\n                    <!-- </RL:Ripple> -->\n\n                    <!-- <RL:Ripple class=\"m-t-8\" backgroundColor=\"#fff\" height=\"60\" borderBottomColor=\"lightgray\" tap=\"logout\"> -->\n                    <GridLayout class=\"m-t-8\" height=\"60\" columns=\"auto,auto,*\" rows=\"*\" backgroundColor=\"#fff\">\n                        <Image class=\"fas m-l-28\" col=\"0\" src=\"font://&#xf2f5;\" width=\"22\" height=\"22\" color=\"#757575\"/>\n                        <Label class=\"text-label m-l-16\" col=\"1\" text=\"ออกจากระบบ\" verticalAlignment=\"center\"></Label>\n                    </GridLayout>\n                    <!-- </RL:Ripple> -->\n                </FlexboxLayout>\n            </ScrollView>\n        </TabContentItem>\n    </BottomNavigation>\n</Page>\n"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/main/main-page.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/main/main-page.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[{"type":"rule","selectors":[".listview-content"],"declarations":[{"type":"comment","comment":" background-image: url(\"app/views/main-page/src/checkin.jpg\") no-repeat; "},{"type":"comment","comment":" width: 95%; "},{"type":"comment","comment":" height: 100%; "},{"type":"declaration","property":"margin-top","value":"8"},{"type":"declaration","property":"margin-left","value":"8"},{"type":"declaration","property":"margin-right","value":"8"},{"type":"comment","comment":" margin-left: 8;\n    margin-right: 8; "},{"type":"comment","comment":" margin-bottom: 8; "},{"type":"declaration","property":"border-radius","value":"8"},{"type":"declaration","property":"background-color","value":"white"},{"type":"comment","comment":" color: white; "}]}],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/modal/modal-page.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const observableModule = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");
const MainPageModel = __webpack_require__("./views/main/main-page-model.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");

let doctor = MainPageModel.DoctorData();
let closeCallback;
let tab;

exports.onShownModally = (args) => {
    const clinicBody = args.context;
    tab = clinicBody.tab;
    closeCallback = args.closeCallback;
    const page = args.object;
    // console.log(clinicBody);
    let clinicdata = [];
    let doctorPerid = clinicBody.cliniclist[0].c_doct;
    let date_d = clinicBody.cliniclist[0].date_d;
    clinicBody.cliniclist.forEach((element) => {
        clinicdata.push({
            cliniccode: element.clinic_code,
            clinicname: element.clinic_name,
        });
    });
    page.bindingContext = observableModule.fromObject({
        doctorperid: doctorPerid,
        currentdate: date_d,
        clinicdata: clinicdata,
    });

    // const bindingContext = page.page.bindingContext;
    // let id = bindingContext.get("id");
    // let name = bindingContext.get("name");
    // closeCallback(id, name);
};

exports.onItemSelected = (args) => {
    const page = args.object.page;
    const namepage = args.object.text;
    let doctorPerid = page.getViewById("doctorperid").text;
    let currentdate = page.getViewById("currentdate").text;
    let listView = page.getViewById("listView");
    var selectedItems = listView.getSelectedItems();
    let clinicCode = selectedItems[0].cliniccode;
    let clinicName = selectedItems[0].clinicname;
    appSettings.setString("cliniccode"+tab, clinicCode);
    appSettings.setString("clinicname"+tab, clinicName);
    // console.log("Doctor Perid: " + doctorPerid.text);
    // console.log("Date: " + currentdate.text);
    // console.log(selectedItems[0]);
    doctor
        .getDoctorQueue(currentdate, clinicCode, doctorPerid)
        .catch((err) => {
            console.log(err);
            reject(err);
        })
        .then((clinicqueueresponse) => {
            let clinicqueuelist = clinicqueueresponse.data;
            let clinicData;
            if (clinicqueuelist.length != 0) {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: clinicqueuelist,
                };
            } else {
                clinicData = {
                    clinicCode: clinicCode,
                    clinicName: clinicName,
                    queueList: [],
                };
            }
            closeCallback(clinicData);
        });
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/modal/modal-page.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/modal/modal-page.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {global.registerModule("nativescript-ui-listview", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.itemTemplate", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/PullToRefreshStyle", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });
global.registerModule("nativescript-ui-listview/RadListView.pullToRefreshStyle", function() { return __webpack_require__("../node_modules/nativescript-ui-listview/ui-listview.js"); });

module.exports = "<Page shownModally=\"onShownModally\" \n    xmlns:lv=\"nativescript-ui-listview\">\n    <StackLayout>\n        <!-- <GridLayout rows=\"100 100\" columns=\"* 2*\">\n            <Label row=\"0\" col=\"0\" text=\"{{id}}\"></Label>\n            <Label row=\"0\" col=\"1\" text=\"{{name}}\"></Label>\n        </GridLayout> -->\n        <GridLayout columns=\"auto, auto\" rows=\"auto, auto\">\n            <!-- <Label class=\"text-font p-8\" text=\"รหัสแพทย์: \" col=\"0\" row=\"0\" style=\"font-size: 16\"></Label> -->\n            <Image class=\"fas m-8\" col=\"0\" row=\"0\" src=\"font://&#xf0f0;\" width=\"24\" height=\"24\" color=\"#4a774e\"/>\n            <Label col=\"1\" row=\"0\" class=\"text-font p-8\" id=\"doctorperid\" text=\"{{doctorperid}}\" style=\"font-size: 20\"></Label>\n\n            <!-- <Label class=\"text-font p-8\" text=\"วันที่: \" col=\"0\" row=\"1\" style=\"font-size: 16\"></Label> -->\n            <Image class=\"fas m-8\" col=\"0\" row=\"1\" src=\"font://&#xf073;\" width=\"24\" height=\"24\" color=\"#4a774e\"/>\n            <Label col=\"1\" row=\"1\" class=\"text-font p-8\" id=\"currentdate\" text=\"{{currentdate}}\" style=\"font-size: 20\"></Label>\n        </GridLayout>\n        <lv:RadListView id=\"listView\" class=\"m-l-8 m-r-8\" items=\"{{ clinicdata }}\" backgroundColor=\"#eee\" selectionBehavior=\"Press\" itemSelected=\"onItemSelected\" text=\"transferdetail\" style=\"height:50%; width:100%\">\n            <lv:RadListView.pullToRefreshStyle>\n                <lv:PullToRefreshStyle indicatorColor=\"#4a774e\" indicatorBackgroundColor=\"white\"/>\n            </lv:RadListView.pullToRefreshStyle>\n            <lv:RadListView.itemTemplate>\n                <StackLayout class=\"listview-content\" orientation=\"vertical\" padding=\"8\">\n                    <GridLayout columns=\"auto, auto, auto\" rows=\"auto, auto\">\n                        <Image class=\"fas\" row=\"0\" col=\"0\" src=\"font://&#xf0f1;\" width=\"20\" height=\"20\" color=\"#4a774e\"/>\n                        <Label class=\"text-font m-l-8\" row=\"0\" col=\"1\" text=\"{{cliniccode}}\" style=\"font-size:20;\"></Label>\n                        <Label class=\"text-font\" row=\"0\" col=\"2\" text=\"{{clinicname}}\" style=\"font-size:16;\"></Label>\n                    </GridLayout>\n                </StackLayout>\n            </lv:RadListView.itemTemplate>\n        </lv:RadListView>\n    </StackLayout>\n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/modal/modal-page.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/modal/modal-page.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.css":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {module.exports = {"type":"stylesheet","stylesheet":{"rules":[],"parsingErrors":[]}};; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.css") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "style", path: "./views/patient/patient-modal.css" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {const observableModule = __webpack_require__("../node_modules/@nativescript/core/data/observable/observable.js");
const appSettings = __webpack_require__("../node_modules/@nativescript/core/application-settings/application-settings.js");

let closeCallback;
let tab;

exports.onShownModally = (args) => {
    let page = args.object.page;
    const patientBody = args.context;
    console.log(patientBody);
    // tab = clinicBody.tab;
    // closeCallback = args.closeCallback;
    // const page = args.object;
    // console.log(clinicBody);
    // let clinicdata = [];
    // let doctorPerid = clinicBody.cliniclist[0].c_doct;
    // let date_d = clinicBody.cliniclist[0].date_d;

    page.bindingContext = observableModule.fromObject({
        patientname: patientBody.name,
        patienthn: patientBody.hn, 
        queuetime: patientBody.time
    });
};
; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.js") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "script", path: "./views/patient/patient-modal.js" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./views/patient/patient-modal.xml":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {
module.exports = "<Page shownModally=\"onShownModally\"\n    xmlns:lv=\"nativescript-ui-listview\">\n    <StackLayout width=\"100%\">\n        <!-- <GridLayout rows=\"100 100\" columns=\"* 2*\">\n            <Label row=\"0\" col=\"0\" text=\"{{id}}\"></Label>\n            <Label row=\"0\" col=\"1\" text=\"{{name}}\"></Label>\n        </GridLayout> -->\n        <GridLayout columns=\"auto, auto\" rows=\"auto, auto, auto\">\n            <Image class=\"fas m-t-8 m-l-16 m-r-8\" col=\"0\" row=\"0\" src=\"font://&#xf2c2;\" width=\"24\" height=\"24\" color=\"#4a774e\"/>\n            <Label col=\"1\" row=\"0\" class=\"text-font p-8\" id=\"doctorperid\" text=\"{{patientname}}\" style=\"font-size: 20\"></Label>\n\n            <Image class=\"fas m-t-8 m-l-16 m-r-8\" col=\"0\" row=\"1\" src=\"font://&#xf0f8;\" width=\"24\" height=\"24\" color=\"#4a774e\"/>\n            <Label col=\"1\" row=\"1\" class=\"text-font p-8\" id=\"currentdate\" text=\"{{patienthn}}\" style=\"font-size: 20\"></Label>\n\n            <Image class=\"fas m-t-8 m-l-16 m-r-8 m-b-8\" col=\"0\" row=\"2\" src=\"font://&#xf017;\" width=\"24\" height=\"24\" color=\"#4a774e\"/>\n            <Label col=\"1\" row=\"2\" class=\"text-font p-8\" id=\"currentdate\" text=\"{{queuetime}}\" style=\"font-size: 20\"></Label>\n        </GridLayout>\n\n        <GridLayout class=\"m-b-8\" columns=\"auto, auto, auto\" rows=\"auto\">\n            <button class=\"text-font p-l-16 p-r-16\" col=\"0\" row=\"0\" text=\"รอพบแพทย์\" borderRadius=\"8\"></button>\n            <button class=\"text-font p-l-16 p-r-16\" col=\"1\" row=\"0\" text=\"รอพบพยาบาล\" borderRadius=\"8\"></button>\n            <button class=\"text-font p-l-16 p-r-16\" col=\"2\" row=\"0\" text=\"ตรวจเสร็จแล้ว\" borderRadius=\"8\" backgroundColor=\"#4a774e\" color=\"white\"></button>\n        </GridLayout>\n    </StackLayout> \n</Page>"; 
if ( true && global._isModuleLoadedForUI && global._isModuleLoadedForUI("./views/patient/patient-modal.xml") ) {
    
    module.hot.accept();
    module.hot.dispose(() => {
        global.hmrRefresh({ type: "markup", path: "./views/patient/patient-modal.xml" });
    });
} 
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("../node_modules/webpack/buildin/global.js")))

/***/ })

},[["./app.js","runtime","vendor"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLiBzeW5jIG5vbnJlY3Vyc2l2ZSBeXFwuXFwvYXBwXFwuKGNzc3xzY3NzfGxlc3N8c2FzcykkIiwid2VicGFjazovLy9cXGJfW1xcdy1dKlxcLilzY3NzKSQiLCJ3ZWJwYWNrOi8vLy4vYXBwLXJvb3QueG1sIiwid2VicGFjazovLy8uL2FwcC5jc3MiLCJ3ZWJwYWNrOi8vLy4vYXBwLmpzIiwid2VicGFjazovLy8uL2NvbmZpZ3MuanMiLCJ3ZWJwYWNrOi8vLy4vbGl2ZVVwZGF0ZS5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9tYWluL21haW4tcGFnZS1tb2RlbC5qcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9tYWluL21haW4tcGFnZS5jc3MiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbWFpbi9tYWluLXBhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbWFpbi9tYWluLXBhZ2UueG1sIiwid2VicGFjazovLy8uL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuY3NzIiwid2VicGFjazovLy8uL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanMiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS54bWwiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmNzcyIsIndlYnBhY2s6Ly8vLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuanMiLCJ3ZWJwYWNrOi8vLy4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLnhtbCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQTtBQUNBO0FBQ0E7OztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGdCQUFnQjtBQUNoQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUU7Ozs7Ozs7QUN2QkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsZ0JBQWdCO0FBQ2hCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5Sjs7Ozs7Ozs7QUNwQ0EsNkU7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQix5Q0FBeUM7QUFDcEUsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDUkEsK0dBQWlFLG1CQUFPLENBQUMsa0RBQWtDO0FBQzNHLGdFQUFnRSxtQkFBTyxDQUFDLGtEQUFrQztBQUMxRyxvRUFBb0UsbUJBQU8sQ0FBQyxxREFBcUM7QUFDakgsbUVBQW1FLG1CQUFPLENBQUMscURBQXFDLEdBQUcsa0JBQWtCLGtDQUFrQyxVQUFVLGtqQkFBa2pCLEVBQUUsaUVBQWlFLEVBQUUsb0VBQW9FLEVBQUUsMkpBQTJKLEVBQUUseUlBQXlJLEVBQUUsb0RBQW9ELHlEQUF5RCxFQUFFLEVBQUUsMERBQTBELG9GQUFvRixFQUFFLEVBQUUsb0RBQW9ELGtHQUFrRyxFQUFFLDREQUE0RCxFQUFFLDJDQUEyQyxHQUFHLEVBQUUsRUFBRSxvREFBb0Qsb0dBQW9HLEVBQUUsNERBQTRELEVBQUUsMkNBQTJDLEdBQUcsRUFBRSxFQUFFLHVEQUF1RCx3REFBd0QsRUFBRSw4REFBOEQsRUFBRSxtRkFBbUYsRUFBRSx5REFBeUQsRUFBRSxFQUFFLDZEQUE2RCxtRkFBbUYsRUFBRSx5REFBeUQsRUFBRSx3QjtBQUMzN0UsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsbUNBQW1DO0FBQzlELEtBQUs7QUFDTCxDOzs7Ozs7Ozs7QUNUQSx1Q0FBdUMsbUJBQU8sQ0FBQywrREFBOEI7QUFDN0U7QUFDQSxZQUFZLG1CQUFPLENBQUMsc0RBQTJCO0FBQy9DLG1CQUFPLENBQUMseURBQW9DO0FBQzVDOzs7QUFHQSxZQUFZLG1CQUFPLENBQUMsMEVBQXVEOzs7QUFHM0UsWUFBWSxJQUFVO0FBQ3RCLDhCQUE4QixtQkFBTyxDQUFDLHVEQUE4QjtBQUNwRTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSwwQ0FBMEMsYUFBYSxLQUFLO0FBQzVEO0FBQ0E7QUFDQSxrREFBa0QsYUFBYTtBQUMvRCxpQkFBaUI7QUFDakI7O0FBRUE7QUFDQTtBQUNBOztBQUVBLDRCQUE0Qix5SkFBa0k7QUFDOUo7QUFDQSxnQkFBZ0IsSUFBVTtBQUMxQixxRDtBQUNBLG1GQUFtRixRQUFTLFE7QUFDNUYsaUJBQWlCO0FBQ2pCOztBQUVBLFFBQVEsbUJBQU8sQ0FBQywyREFBc0M7QUFDdEQsMEJBQTBCLG1CQUFPLENBQUMsK0RBQThCO0FBQ2hFLG9CQUFvQixtQkFBTyxDQUFDLGlGQUF1QztBQUNuRSxtQkFBbUIsbUJBQU8sQ0FBQyxpQkFBYztBQUN6QyxjQUFjLG1CQUFPLENBQUMsY0FBVzs7QUFFakM7O0FBRUE7QUFDQTtBQUNBO0FBQ0EscUJBQXFCLDhCQUE4QjtBQUNuRCxDQUFDO0FBQ0QscUJBQXFCLHlCQUF5QjtBQUM5QztBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixtQ0FBbUM7QUFDOUQsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7OztBQzdEQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7O0FBRUw7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsdUNBQXVDO0FBQ2xFLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ3hCQSxrRUFBb0IsbUJBQU8sQ0FBQywrREFBOEI7QUFDMUQsZ0JBQWdCLG1CQUFPLENBQUMsbURBQXVCO0FBQy9DLG9CQUFvQixtQkFBTyxDQUFDLG1EQUF1QjtBQUNuRCxvQkFBb0IsbUJBQU8sQ0FBQyxtREFBdUI7QUFDbkQsaUJBQWlCLG1CQUFPLENBQUMseURBQTJCOztBQUVwRDtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0EsOENBQThDO0FBQzlDLGFBQWE7QUFDYjtBQUNBO0FBQ0EsVUFBVTtBQUNWLEtBQUs7QUFDTDs7QUFFQTtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiwwQ0FBMEM7QUFDckUsS0FBSztBQUNMLEM7Ozs7Ozs7Ozs7Ozs7OztBQ2pEQSxpRUFBbUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDN0QsYUFBYSxtQkFBTyxDQUFDLGlEQUF1Qjs7QUFFNUM7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQixhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQSxpQkFBaUI7QUFDakIsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsaUJBQWlCO0FBQ2pCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsMERBQTBEO0FBQ3JGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ2pMQSxnRUFBa0Isa0NBQWtDLFVBQVUsd0RBQXdELG1FQUFtRSxFQUFFLDJEQUEyRCxFQUFFLEVBQUUseUVBQXlFLDJEQUEyRCxFQUFFLG1FQUFtRSxFQUFFLHlEQUF5RCxFQUFFLEVBQUUsZ0ZBQWdGLDBEQUEwRCxFQUFFLHVFQUF1RSxFQUFFLHlEQUF5RCxFQUFFLEVBQUUsZ0VBQWdFLGtGQUFrRixFQUFFLHlEQUF5RCxFQUFFLEVBQUUsMkRBQTJELG1GQUFtRixFQUFFLHlEQUF5RCxFQUFFLEVBQUUsb0VBQW9FLDZEQUE2RCxFQUFFLHFEQUFxRCxHQUFHLEVBQUUsZ0dBQWdHLEVBQUUsd0VBQXdFLEVBQUUsa0VBQWtFLEVBQUUsb0VBQW9FLEVBQUUsMERBQTBELEVBQUUsRUFBRSxpRUFBaUUsNENBQTRDLG9CQUFvQixxQkFBcUIsR0FBRyxFQUFFLG9FQUFvRSxFQUFFLHFFQUFxRSxFQUFFLDBDQUEwQyxHQUFHLEVBQUUsRUFBRSw4REFBOEQsNENBQTRDLG9CQUFvQixxQkFBcUIsc0JBQXNCLEdBQUcsRUFBRSwyQ0FBMkMsb0JBQW9CLHFCQUFxQixHQUFHLEVBQUUsNkRBQTZELEVBQUUsbUVBQW1FLEVBQUUsRUFBRSw4REFBOEQsMERBQTBELEVBQUUsOERBQThELEVBQUUscUVBQXFFLEVBQUUsRUFBRSwrREFBK0QsNkRBQTZELEVBQUUsdURBQXVELEVBQUUsd0VBQXdFLEVBQUUsd0RBQXdELEVBQUUsd0I7QUFDdmdHLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLG9EQUFvRDtBQUMvRSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNQQSwyREFBYSxtQkFBTyxDQUFDLHlEQUErQjtBQUNwRCxzQkFBc0IsbUJBQU8sQ0FBQyxpQ0FBbUI7QUFDakQsbUJBQW1CLG1CQUFPLENBQUMsa0VBQWtDO0FBQzdELG9CQUFvQixtQkFBTyxDQUFDLGlGQUF1QztBQUNuRSxpQkFBaUIsbUJBQU8sQ0FBQyx5REFBMkI7QUFDcEQsZ0JBQWdCLG1CQUFPLENBQUMsMERBQTZCO0FBQ3JELGlCQUFpQixtQkFBTyxDQUFDLDhDQUFxQjs7QUFFOUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsOENBQThDO0FBQzlDLEtBQUs7QUFDTCw4Q0FBOEM7QUFDOUM7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBLFNBQVM7QUFDVDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQSxTQUFTO0FBQ1Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0EsU0FBUztBQUNUOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0EsYUFBYTtBQUNiLEtBQUs7QUFDTDtBQUNBO0FBQ0EsS0FBSztBQUNMOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGlCQUFpQjtBQUNqQjtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsS0FBSztBQUNMO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxxQkFBcUI7QUFDckI7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSx5QkFBeUI7QUFDekI7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlCQUF5QjtBQUN6QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHFCQUFxQjtBQUNyQjtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2IsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEtBQUs7QUFDTDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhOztBQUViO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTtBQUNiO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7O0FBRWI7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFhO0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsYUFBYTs7QUFFYjtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsS0FBSztBQUNMO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0EsQztBQUNBLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLG9EQUFvRDtBQUMvRSxLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUM3ZUEsNEdBQThELFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFO0FBQzVHLDBFQUEwRSxRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTtBQUN4SCx1RkFBdUYsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7O0FBRXJJLDZZQUE2WSx3UUFBd1Esb1BBQW9QLDRwRkFBNHBGLFlBQVksOGRBQThkLGFBQWEsK1BBQStQLE9BQU8sbUlBQW1JLEtBQUssMFNBQTBTLFFBQVEsOHFFQUE4cUUsWUFBWSw4ZEFBOGQsYUFBYSwrUEFBK1AsT0FBTyxtSUFBbUksS0FBSywwU0FBMFMsUUFBUSw4cUVBQThxRSxZQUFZLHVaQUF1WixhQUFhLGdRQUFnUSxPQUFPLG1JQUFtSSxLQUFLLDBTQUEwUyxRQUFRLGs3QkFBazdCLFNBQVMscTBCQUFxMEIsNlg7QUFDaG1hLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHFEQUFxRDtBQUNoRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNYQSxnRUFBa0Isa0NBQWtDLFVBQVUsaUVBQWlFLHNHQUFzRyxHQUFHLEVBQUUsd0NBQXdDLEdBQUcsRUFBRSwwQ0FBMEMsR0FBRyxFQUFFLHlEQUF5RCxFQUFFLDBEQUEwRCxFQUFFLDJEQUEyRCxFQUFFLDRDQUE0QyxzQkFBc0IsR0FBRyxFQUFFLDhDQUE4QyxHQUFHLEVBQUUsNERBQTRELEVBQUUsbUVBQW1FLEVBQUUsMENBQTBDLEdBQUcsRUFBRSx3QjtBQUN0eUIsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsc0RBQXNEO0FBQ2pGLEtBQUs7QUFDTCxDOzs7Ozs7OztBQ1BBLHVFQUF5QixtQkFBTyxDQUFDLGtFQUFrQztBQUNuRSxzQkFBc0IsbUJBQU8sQ0FBQyxpQ0FBeUI7QUFDdkQsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDOztBQUVuRTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxTQUFTO0FBQ1QsS0FBSztBQUNMO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsS0FBSzs7QUFFTDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsU0FBUztBQUNUO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLGFBQWE7QUFDYjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLFNBQVM7QUFDVDtBQUNBLEM7QUFDQSxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQixzREFBc0Q7QUFDakYsS0FBSztBQUNMLEM7Ozs7Ozs7O0FDakZBLDRHQUE4RCxRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTtBQUM1RywwRUFBMEUsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7QUFDeEgsdUZBQXVGLFFBQVEsbUJBQU8sQ0FBQyx5REFBMEIsRUFBRSxFQUFFO0FBQ3JJLGlGQUFpRixRQUFRLG1CQUFPLENBQUMseURBQTBCLEVBQUUsRUFBRTtBQUMvSCw2RkFBNkYsUUFBUSxtQkFBTyxDQUFDLHlEQUEwQixFQUFFLEVBQUU7O0FBRTNJLHNPQUFzTyxJQUFJLDZEQUE2RCxNQUFNLHNUQUFzVCw4SUFBOEksYUFBYSxpUEFBaVAsOElBQThJLGFBQWEsb0lBQW9JLGNBQWMsbUlBQW1JLGdoQkFBZ2hCLHlJQUF5SSxZQUFZLHdCQUF3Qiw2RkFBNkYsWUFBWSx3QkFBd0Isb0w7QUFDN3RFLElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLHVEQUF1RDtBQUNsRixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNiQSxnRUFBa0Isa0NBQWtDLGlDO0FBQ3BELElBQUksS0FBVTs7QUFFZDtBQUNBO0FBQ0EsMkJBQTJCLDJEQUEyRDtBQUN0RixLQUFLO0FBQ0wsQzs7Ozs7Ozs7QUNQQSx1RUFBeUIsbUJBQU8sQ0FBQyxrRUFBa0M7QUFDbkUsb0JBQW9CLG1CQUFPLENBQUMsaUZBQXVDOztBQUVuRTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0w7QUFDQSxDO0FBQ0EsSUFBSSxLQUFVOztBQUVkO0FBQ0E7QUFDQSwyQkFBMkIsMkRBQTJEO0FBQ3RGLEtBQUs7QUFDTCxDOzs7Ozs7Ozs7QUM5QkEsb1BBQW9QLElBQUksNkRBQTZELE1BQU0sNE1BQTRNLDhJQUE4SSxhQUFhLG9JQUFvSSw4SUFBOEksV0FBVywwSUFBMEksOElBQThJLFdBQVcsc21CO0FBQ2x1QyxJQUFJLEtBQVU7O0FBRWQ7QUFDQTtBQUNBLDJCQUEyQiw0REFBNEQ7QUFDdkYsS0FBSztBQUNMLEMiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsidmFyIG1hcCA9IHtcblx0XCIuL2FwcC5jc3NcIjogXCIuL2FwcC5jc3NcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0dmFyIGlkID0gbWFwW3JlcV07XG5cdGlmKCEoaWQgKyAxKSkgeyAvLyBjaGVjayBmb3IgbnVtYmVyIG9yIHN0cmluZ1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gaWQ7XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuLyBzeW5jIF5cXFxcLlxcXFwvYXBwXFxcXC4oY3NzfHNjc3N8bGVzc3xzYXNzKSRcIjsiLCJ2YXIgbWFwID0ge1xuXHRcIi4vYXBwLXJvb3QueG1sXCI6IFwiLi9hcHAtcm9vdC54bWxcIixcblx0XCIuL2FwcC5jc3NcIjogXCIuL2FwcC5jc3NcIixcblx0XCIuL2FwcC5qc1wiOiBcIi4vYXBwLmpzXCIsXG5cdFwiLi9jb25maWdzLmpzXCI6IFwiLi9jb25maWdzLmpzXCIsXG5cdFwiLi9saXZlVXBkYXRlLmpzXCI6IFwiLi9saXZlVXBkYXRlLmpzXCIsXG5cdFwiLi92aWV3cy9tYWluL21haW4tcGFnZS1tb2RlbC5qc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UtbW9kZWwuanNcIixcblx0XCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLmNzc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuY3NzXCIsXG5cdFwiLi92aWV3cy9tYWluL21haW4tcGFnZS5qc1wiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuanNcIixcblx0XCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLnhtbFwiOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UueG1sXCIsXG5cdFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmNzc1wiOiBcIi4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS5jc3NcIixcblx0XCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIjogXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIixcblx0XCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UueG1sXCI6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLnhtbFwiLFxuXHRcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmNzc1wiOiBcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmNzc1wiLFxuXHRcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLmpzXCI6IFwiLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuanNcIixcblx0XCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC54bWxcIjogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC54bWxcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0dmFyIGlkID0gbWFwW3JlcV07XG5cdGlmKCEoaWQgKyAxKSkgeyAvLyBjaGVjayBmb3IgbnVtYmVyIG9yIHN0cmluZ1xuXHRcdHZhciBlID0gbmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIiArIHJlcSArIFwiJ1wiKTtcblx0XHRlLmNvZGUgPSAnTU9EVUxFX05PVF9GT1VORCc7XG5cdFx0dGhyb3cgZTtcblx0fVxuXHRyZXR1cm4gaWQ7XG59XG53ZWJwYWNrQ29udGV4dC5rZXlzID0gZnVuY3Rpb24gd2VicGFja0NvbnRleHRLZXlzKCkge1xuXHRyZXR1cm4gT2JqZWN0LmtleXMobWFwKTtcbn07XG53ZWJwYWNrQ29udGV4dC5yZXNvbHZlID0gd2VicGFja0NvbnRleHRSZXNvbHZlO1xubW9kdWxlLmV4cG9ydHMgPSB3ZWJwYWNrQ29udGV4dDtcbndlYnBhY2tDb250ZXh0LmlkID0gXCIuLyBzeW5jIHJlY3Vyc2l2ZSAoPzwhXFxcXGJBcHBfUmVzb3VyY2VzXFxcXGIuKikoPzwhXFxcXC5cXFxcL1xcXFxidGVzdHNcXFxcYlxcXFwvLio/KVxcXFwuKHhtbHxjc3N8anN8a3R8KD88IVxcXFwuZFxcXFwuKXRzfCg/PCFcXFxcYl9bXFxcXHctXSpcXFxcLilzY3NzKSRcIjsiLCJcbm1vZHVsZS5leHBvcnRzID0gXCI8RnJhbWUgZGVmYXVsdFBhZ2U9XFxcIi92aWV3cy9tYWluL21haW4tcGFnZVxcXCI+XFxuPC9GcmFtZT5cXG5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9hcHAtcm9vdC54bWxcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwibWFya3VwXCIsIHBhdGg6IFwiLi9hcHAtcm9vdC54bWxcIiB9KTtcbiAgICB9KTtcbn0gIiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwifkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2NvcmUuY3NzXCIsICgpID0+IHJlcXVpcmUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiKSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvY29yZS5jc3NcIikpO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwifkBuYXRpdmVzY3JpcHQvdGhlbWUvY3NzL2RlZmF1bHQuY3NzXCIsICgpID0+IHJlcXVpcmUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9kZWZhdWx0LmNzc1wiKSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJAbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9kZWZhdWx0LmNzc1wiLCAoKSA9PiByZXF1aXJlKFwiQG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcIikpO21vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIlxcbkluIE5hdGl2ZVNjcmlwdCwgdGhlIGFwcC5jc3MgZmlsZSBpcyB3aGVyZSB5b3UgcGxhY2UgQ1NTIHJ1bGVzIHRoYXRcXG55b3Ugd291bGQgbGlrZSB0byBhcHBseSB0byB5b3VyIGVudGlyZSBhcHBsaWNhdGlvbi4gQ2hlY2sgb3V0XFxuaHR0cDovL2RvY3MubmF0aXZlc2NyaXB0Lm9yZy91aS9zdHlsaW5nIGZvciBhIGZ1bGwgbGlzdCBvZiB0aGUgQ1NTXFxuc2VsZWN0b3JzIGFuZCBwcm9wZXJ0aWVzIHlvdSBjYW4gdXNlIHRvIHN0eWxlIFVJIGNvbXBvbmVudHMuXFxuXFxuLypcXG5JbiBtYW55IGNhc2VzIHlvdSBtYXkgd2FudCB0byB1c2UgdGhlIE5hdGl2ZVNjcmlwdCBjb3JlIHRoZW1lIGluc3RlYWRcXG5vZiB3cml0aW5nIHlvdXIgb3duIENTUyBydWxlcy4gWW91IGNhbiBsZWFybiBtb3JlIGFib3V0IHRoZSBcXG5OYXRpdmVTY3JpcHQgY29yZSB0aGVtZSBhdCBodHRwczovL2dpdGh1Yi5jb20vbmF0aXZlc2NyaXB0L3RoZW1lXFxuVGhlIGltcG9ydGVkIENTUyBydWxlcyBtdXN0IHByZWNlZGUgYWxsIG90aGVyIHR5cGVzIG9mIHJ1bGVzLlxcblwifSx7XCJ0eXBlXCI6XCJpbXBvcnRcIixcImltcG9ydFwiOlwiXFxcIn5AbmF0aXZlc2NyaXB0L3RoZW1lL2Nzcy9jb3JlLmNzc1xcXCJcIn0se1widHlwZVwiOlwiaW1wb3J0XCIsXCJpbXBvcnRcIjpcIlxcXCJ+QG5hdGl2ZXNjcmlwdC90aGVtZS9jc3MvZGVmYXVsdC5jc3NcXFwiXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBQbGFjZSBhbnkgQ1NTIHJ1bGVzIHlvdSB3YW50IHRvIGFwcGx5IG9uIGJvdGggaU9TIGFuZCBBbmRyb2lkIGhlcmUuXFxuVGhpcyBpcyB3aGVyZSB0aGUgdmFzdCBtYWpvcml0eSBvZiB5b3VyIENTUyBjb2RlIGdvZXMuIFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCJcXG5UaGUgZm9sbG93aW5nIENTUyBydWxlIGNoYW5nZXMgdGhlIGZvbnQgc2l6ZSBvZiBhbGwgQnV0dG9ucyB0aGF0IGhhdmUgdGhlXFxuXFxcIi1wcmltYXJ5XFxcIiBjbGFzcyBtb2RpZmllci5cXG5cIn0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmJ0blwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE4XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnRleHQtZm9udFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtZmFtaWx5XCIsXCJ2YWx1ZVwiOlwiXFxcIkthbml0XFxcIiwgXFxcIkthbml0LU1lZGl1bVxcXCJcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuZmFzXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC1mYW1pbHlcIixcInZhbHVlXCI6XCJcXFwiRm9udCBBd2Vzb21lIDUgRnJlZVxcXCIsIFxcXCJmYS1zb2xpZC05MDBcXFwiXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiZm9udC13ZWlnaHRcIixcInZhbHVlXCI6XCI5MDBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGZvbnQtc2l6ZTogMTA7IFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5mYXJcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJGb250IEF3ZXNvbWUgNSBGcmVlXFxcIiwgXFxcImZhLXJlZ3VsYXItNDAwXFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtd2VpZ2h0XCIsXCJ2YWx1ZVwiOlwiNDAwXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBmb250LXNpemU6IDEwOyBcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIuaGVhZGVyXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInRleHQtYWxpZ25cIixcInZhbHVlXCI6XCJjZW50ZXJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxOFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5ub2RhdGEtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIyMlwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9hcHAuY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi9hcHAuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsIlxuICAgICAgICBsZXQgYXBwbGljYXRpb25DaGVja1BsYXRmb3JtID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb25cIik7XG4gICAgICAgIGlmIChhcHBsaWNhdGlvbkNoZWNrUGxhdGZvcm0uYW5kcm9pZCAmJiAhZ2xvYmFsW1wiX19zbmFwc2hvdFwiXSkge1xuICAgICAgICAgICAgcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvZnJhbWVcIik7XG5yZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy91aS9mcmFtZS9hY3Rpdml0eVwiKTtcbiAgICAgICAgfVxuXG4gICAgICAgIFxuICAgICAgICAgICAgcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1kZXYtd2VicGFjay9sb2FkLWFwcGxpY2F0aW9uLWNzcy1yZWd1bGFyXCIpKCk7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIFxuICAgICAgICBpZiAobW9kdWxlLmhvdCkge1xuICAgICAgICAgICAgY29uc3QgaG1yVXBkYXRlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1kZXYtd2VicGFjay9obXJcIikuaG1yVXBkYXRlO1xuICAgICAgICAgICAgZ2xvYmFsLl9fY29yZU1vZHVsZXNMaXZlU3luYyA9IGdsb2JhbC5fX29uTGl2ZVN5bmM7XG5cbiAgICAgICAgICAgIGdsb2JhbC5fX29uTGl2ZVN5bmMgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgLy8gaGFuZGxlIGhvdCB1cGRhdGVkIG9uIExpdmVTeW5jXG4gICAgICAgICAgICAgICAgaG1yVXBkYXRlKCk7XG4gICAgICAgICAgICB9O1xuXG4gICAgICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCA9IGZ1bmN0aW9uKHsgdHlwZSwgcGF0aCB9ID0ge30pIHtcbiAgICAgICAgICAgICAgICAvLyB0aGUgaG90IHVwZGF0ZXMgYXJlIGFwcGxpZWQsIGFzayB0aGUgbW9kdWxlcyB0byBhcHBseSB0aGUgY2hhbmdlc1xuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBnbG9iYWwuX19jb3JlTW9kdWxlc0xpdmVTeW5jKHsgdHlwZSwgcGF0aCB9KTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH07XG5cbiAgICAgICAgICAgIC8vIGhhbmRsZSBob3QgdXBkYXRlZCBvbiBpbml0aWFsIGFwcCBzdGFydFxuICAgICAgICAgICAgaG1yVXBkYXRlKCk7XG4gICAgICAgIH1cbiAgICAgICAgXG4gICAgICAgICAgICBjb25zdCBjb250ZXh0ID0gcmVxdWlyZS5jb250ZXh0KFwifi9cIiwgdHJ1ZSwgLyg/PCFcXGJBcHBfUmVzb3VyY2VzXFxiLiopKD88IVxcLlxcL1xcYnRlc3RzXFxiXFwvLio/KVxcLih4bWx8Y3NzfGpzfGt0fCg/PCFcXC5kXFwuKXRzfCg/PCFcXGJfW1xcdy1dKlxcLilzY3NzKSQvKTtcbiAgICAgICAgICAgIGdsb2JhbC5yZWdpc3RlcldlYnBhY2tNb2R1bGVzKGNvbnRleHQpO1xuICAgICAgICAgICAgaWYgKG1vZHVsZS5ob3QpIHtcbiAgICAgICAgICAgICAgICBtb2R1bGUuaG90LmFjY2VwdChjb250ZXh0LmlkLCAoKSA9PiB7IFxuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIkhNUjogQWNjZXB0IG1vZHVsZSAnXCIgKyBjb250ZXh0LmlkICsgXCInIGZyb20gJ1wiICsgbW9kdWxlLmlkICsgXCInXCIpOyBcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIFxuICAgICAgICByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9idW5kbGUtZW50cnktcG9pbnRzXCIpO1xuICAgICAgICBsZXQgYXBwbGljYXRpb24gPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9hcHBsaWNhdGlvblwiKTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCBMaXZlVXBkYXRlID0gcmVxdWlyZShcIi4vbGl2ZVVwZGF0ZVwiKTtcbmdsb2JhbC5jb25mID0gcmVxdWlyZShcIi4vY29uZmlnc1wiKTtcblxubmV3IExpdmVVcGRhdGUoZ2xvYmFsLmNvbmYudXBkYXRlS2V5KTtcblxubGV0IHRva2VuID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwidG9rZW5cIiwgXCJ0b2tlbmV4cGlyZWRcIik7XG4vLyBjb25zb2xlLmxvZyhcIlJvb3QtVXNlciBUb2tlbjogXCIgKyB0b2tlbik7XG5pZiAodG9rZW4gIT0gXCJ0b2tlbmV4cGlyZWRcIikge1xuICAgIGFwcGxpY2F0aW9uLnJ1bih7IG1vZHVsZU5hbWU6IFwiYXBwLXJvb3QtbWFpblwiIH0pO1xufSBlbHNlIHtcbiAgICBhcHBsaWNhdGlvbi5ydW4oeyBtb2R1bGVOYW1lOiBcImFwcC1yb290XCIgfSk7XG59XG47IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vYXBwLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vYXBwLmpzXCIgfSk7XG4gICAgfSk7XG59IFxuICAgIFxuICAgICAgICBcbiAgICAgICAgXG4gICAgIiwiY29uZmlnID0ge1xuICAgIG5hbWU6IFwiQ2xpbmljIFF1ZXVlXCIsXG4gICAgdmVyOiBcIlZlcnNpb24gMS4wIChidWlsZCAwMTA3MjApXCIsXG5cbiAgICBhcGk6IHtcbiAgICAgICAgaXNkb2N0b3I6IFwiaHR0cDovLzYxLjE5LjIwMS4yMDoxOTUzOS9jbGluaWNxdWV1ZS9kb2N0b3IvaXNkb2N0b3JcIixcbiAgICAgICAgZG9jdG9yY2xpbmljOiBcImh0dHA6Ly82MS4xOS4yMDEuMjA6MTk1MzkvY2xpbmljcXVldWUvZG9jdG9yL2NsaW5pY1wiLFxuICAgICAgICBkb2N0b3JxdWV1ZTogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL2RvY3Rvci9xdWV1ZVwiLFxuICAgICAgICBwYXRpZW50aW5mbzogXCJodHRwOi8vNjEuMTkuMjAxLjIwOjE5NTM5L2NsaW5pY3F1ZXVlL2RvY3Rvci9wYXRpZW50aW5mb1wiLFxuICAgIH0sXG5cbiAgICB1cGRhdGVLZXk6IHtcbiAgICAgICAgYW5kcm9pZDogXCJjMHB4c2E0REJsMWlwWkpIRzZoamh6OTRMU3dacGJWWWljMHc1XCJcbiAgICB9XG59O1xuXG5tb2R1bGUuZXhwb3J0cyA9IGNvbmZpZztcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9jb25maWdzLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vY29uZmlncy5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBhcHBsaWNhdGlvbiA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uXCIpO1xuY29uc3QgQXBwU3luYyA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtYXBwLXN5bmNcIikuQXBwU3luYztcbmNvbnN0IEluc3RhbGxNb2RlID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hcHAtc3luY1wiKS5JbnN0YWxsTW9kZTtcbmNvbnN0IFN5bmNTdGF0dXMgID0gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC1hcHAtc3luY1wiKS5TeW5jU3RhdHVzIDtcbmNvbnN0IHBsYXRmb3JtID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvcGxhdGZvcm1cIik7XG5cbmZ1bmN0aW9uIGxpdmVVcGRhdGUoY29uZmlnKSB7XG4gICAgaWYgKHBsYXRmb3JtLmlzSU9TKSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiSU9TIGRldmljZSB0b2tlbjogXCIgKyBjb25maWcuaW9zKTtcbiAgICB9IGVsc2Uge1xuICAgICAgICBjb25zb2xlLmxvZyhcIkFuZHJvaWQgZGV2aWNlIHRva2VuOiBcIiArIGNvbmZpZy5hbmRyb2lkKTtcbiAgICB9XG4gICAgYXBwbGljYXRpb24ub24oYXBwbGljYXRpb24ucmVzdW1lRXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICBBcHBTeW5jLnN5bmMoe1xuICAgICAgICAgICAgZW5hYmxlZFdoZW5Vc2luZ0htcjogZmFsc2UsXG4gICAgICAgICAgICBkZXBsb3ltZW50S2V5OiBwbGF0Zm9ybS5pc0lPUyA/IGNvbmZpZy5pb3MgOiBjb25maWcuYW5kcm9pZCxcbiAgICAgICAgICAgIGluc3RhbGxNb2RlOiBJbnN0YWxsTW9kZS5JTU1FRElBVEUsXG4gICAgICAgICAgICBtYW5kYXRvcnlJbnN0YWxsTW9kZTogcGxhdGZvcm0uaXNJT1NcbiAgICAgICAgICAgICAgICA/IEluc3RhbGxNb2RlLk9OX05FWFRfUkVTVU1FXG4gICAgICAgICAgICAgICAgOiBJbnN0YWxsTW9kZS5JTU1FRElBVEUsXG4gICAgICAgICAgICB1cGRhdGVEaWFsb2c6IHtcbiAgICAgICAgICAgICAgICBvcHRpb25hbFVwZGF0ZU1lc3NhZ2U6IFwi4Lij4Li14Liq4LiV4Liy4Lij4LmM4LiX4LmA4Lie4Li34LmI4Lit4Lit4Lix4Lie4LmA4LiU4LiXXCIsXG4gICAgICAgICAgICAgICAgdXBkYXRlVGl0bGU6IFwi4Lit4Lix4Lie4LmA4LiU4LiX4LmA4Lin4Lit4Lij4LmM4LiK4Lix4LiZ4LmD4Lir4Lih4LmIXCIsXG4gICAgICAgICAgICAgICAgbWFuZGF0b3J5VXBkYXRlTWVzc2FnZTogXCLguKPguLXguKrguJXguLLguKPguYzguJfguYDguJ7guLfguYjguK3guK3guLHguJ7guYDguJTguJdcIixcbiAgICAgICAgICAgICAgICBvcHRpb25hbElnbm9yZUJ1dHRvbkxhYmVsOiBcIuC4ouC4geC5gOC4peC4tOC4gVwiLFxuICAgICAgICAgICAgICAgIG1hbmRhdG9yeUNvbnRpbnVlQnV0dG9uTGFiZWw6IHBsYXRmb3JtLmlzSU9TXG4gICAgICAgICAgICAgICAgICAgID8gXCLguJvguLTguJRcIlxuICAgICAgICAgICAgICAgICAgICA6IFwi4Lij4Li14Liq4LiV4Liy4Lij4LmM4LiXXCIsXG4gICAgICAgICAgICAgICAgYXBwZW5kUmVsZWFzZURlc2NyaXB0aW9uOiB0cnVlXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgICAoc3luY1N0YXR1cykgPT4ge1xuICAgICAgICAgICAgaWYgKHN5bmNTdGF0dXMgPT09IFN5bmNTdGF0dXMuVVBfVE9fREFURSkge1xuICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIk5vIHBlbmRpbmcgdXBkYXRlczsgeW914oCZcmUgcnVubmluZyB0aGUgbGF0ZXN0IHZlcnNpb24uXCIpO1xuICAgICAgICAgICAgfSBlbHNlIGlmIChzeW5jU3RhdHVzID09PSBTeW5jU3RhdHVzLlVQREFURV9JTlNUQUxMRUQpIHtcbiAgICAgICAgICAgICAgY29uc29sZS5sb2coXCJVcGRhdGUgZm91bmQgYW5kIGluc3RhbGxlZC4gSXQgd2lsbCBiZSBhY3RpdmF0ZWQgdXBvbiBuZXh0IGNvbGQgcmVib290XCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgfSk7XG4gICAgfSk7XG59XG5cbm1vZHVsZS5leHBvcnRzID0gbGl2ZVVwZGF0ZTtcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi9saXZlVXBkYXRlLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vbGl2ZVVwZGF0ZS5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBPYnNlcnZhYmxlID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCIpLk9ic2VydmFibGU7XG5jb25zdCBodHRwID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvaHR0cFwiKTtcblxuZnVuY3Rpb24gRG9jdG9yRGF0YSgpIHtcbiAgICBsZXQgdmlld01vZGVsID0gbmV3IE9ic2VydmFibGUoKTtcbiAgICBsZXQgcmVzdWx0ID0gbmV3IEFycmF5KCk7XG4gICAgLy8gY29uc29sZS5sb2coZ2xvYmFsLmNvbmYuYXBpLmRvY3RvcmNsaW5pYyk7XG4gICAgdmlld01vZGVsLmdldERvY3RvckluZm8gPSAocGVyaWQpID0+IHtcbiAgICAgICAgcmV0dXJuIGh0dHBcbiAgICAgICAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5pc2RvY3RvcixcbiAgICAgICAgICAgICAgICBtZXRob2Q6IFwiUE9TVFwiLFxuICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgXCJDb250ZW50LVR5cGVcIjogXCJhcHBsaWNhdGlvbi9qc29uXCIsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBjb250ZW50OiBKU09OLnN0cmluZ2lmeSh7XG4gICAgICAgICAgICAgICAgICAgIHBlcmlkOiBwZXJpZCxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLmRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAoZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICB9O1xuICAgIHZpZXdNb2RlbC5nZXRDbGluaWMgPSAocGVyaWQsIGN1cmRhdGUpID0+IHtcbiAgICAgICAgcmV0dXJuIGh0dHBcbiAgICAgICAgICAgIC5yZXF1ZXN0KHtcbiAgICAgICAgICAgICAgICB1cmw6IGdsb2JhbC5jb25mLmFwaS5kb2N0b3JjbGluaWMsXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICBwZXJpZDogcGVyaWQsXG4gICAgICAgICAgICAgICAgICAgIGRhdGVfZDogY3VyZGF0ZSxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLmRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAoZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgdmlld01vZGVsLmdldERvY3RvclF1ZXVlID0gKGN1cmRhdGUsIGNsaW5pY2NvZGUsIGRvY3RvcnBlcmlkKSA9PiB7XG4gICAgICAgIHJldHVybiBodHRwXG4gICAgICAgICAgICAucmVxdWVzdCh7XG4gICAgICAgICAgICAgICAgdXJsOiBnbG9iYWwuY29uZi5hcGkuZG9jdG9ycXVldWUsXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICBkYXRlX2Q6IGN1cmRhdGUsXG4gICAgICAgICAgICAgICAgICAgIGNvZGVfYjogY2xpbmljY29kZSxcbiAgICAgICAgICAgICAgICAgICAgY19kb2N0OiBkb2N0b3JwZXJpZCxcbiAgICAgICAgICAgICAgICB9KSxcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAudGhlbihcbiAgICAgICAgICAgICAgICAocmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgbGV0IHJlcyA9IHJlc3BvbnNlLmNvbnRlbnQudG9KU09OKCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMubGVuZ3RoID09IDApIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlOiAyMDAsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVzLmRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAoZSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhlKTtcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAxLFxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YTogSlNPTi5zdHJpbmdpZnkoZSksXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXN1bHQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgKTtcbiAgICB9O1xuXG4gICAgdmlld01vZGVsLmdldFBhdGllbnRJbmZvID0gKGhuKSA9PiB7XG4gICAgICAgIHJldHVybiBodHRwXG4gICAgICAgICAgICAucmVxdWVzdCh7XG4gICAgICAgICAgICAgICAgdXJsOiBnbG9iYWwuY29uZi5hcGkucGF0aWVudGluZm8sXG4gICAgICAgICAgICAgICAgbWV0aG9kOiBcIlBPU1RcIixcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgIFwiQ29udGVudC1UeXBlXCI6IFwiYXBwbGljYXRpb24vanNvblwiLFxuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgY29udGVudDogSlNPTi5zdHJpbmdpZnkoe1xuICAgICAgICAgICAgICAgICAgICBobjogaG4sXG4gICAgICAgICAgICAgICAgfSksXG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oXG4gICAgICAgICAgICAgICAgKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIGxldCByZXMgPSByZXNwb25zZS5jb250ZW50LnRvSlNPTigpO1xuICAgICAgICAgICAgICAgICAgICBpZiAocmVzLmxlbmd0aCA9PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogNDAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHJlcyxcbiAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZTogMjAwLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHJlcy5kYXRhLFxuICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgKGUpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coZSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1c0NvZGU6IDQwMSxcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IEpTT04uc3RyaW5naWZ5KGUpLFxuICAgICAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICk7XG4gICAgfTtcbiAgICByZXR1cm4gdmlld01vZGVsO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgICBEb2N0b3JEYXRhOiBEb2N0b3JEYXRhLFxufTtcbjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluL21haW4tcGFnZS1tb2RlbC5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLW1vZGVsLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIlRhYlN0cmlwXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJkYXJrZ3JheVwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIlRhYlN0cmlwSXRlbS50YWJzdHJpcGl0ZW1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcImRhcmtncmF5XCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIndoaXRlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwibWFyZ2luLXRvcFwiLFwidmFsdWVcIjpcIjRcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCJUYWJTdHJpcEl0ZW0udGFic3RyaXBpdGVtOmFjdGl2ZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImNvbG9yXCIsXCJ2YWx1ZVwiOlwiIzFjODQ0NVwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJtaW50Y3JlYW1cIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi50ZXh0LW5hbWUtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1Cb2xkXFxcIlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImZvbnQtc2l6ZVwiLFwidmFsdWVcIjpcIjE4XCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLnRleHQtbGFiZWxcIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LWZhbWlseVwiLFwidmFsdWVcIjpcIlxcXCJLYW5pdFxcXCIsIFxcXCJLYW5pdC1MaWdodFxcXCJcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJmb250LXNpemVcIixcInZhbHVlXCI6XCIxNFwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5jYXJkLWRvY3Rvci1wcm9maWxlXCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjIwXCJ9LHtcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTsgXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1pbWFnZVwiLFwidmFsdWVcIjpcInVybChcXFwifi9zcmMvaW1nL2RvY3Rvci1jYXJkLnBuZ1xcXCIpXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1yZXBlYXRcIixcInZhbHVlXCI6XCJuby1yZXBlYXRcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLXNpemVcIixcInZhbHVlXCI6XCJjb3ZlclwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtcG9zaXRpb25cIixcInZhbHVlXCI6XCJ0b3BcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJjb2xvclwiLFwidmFsdWVcIjpcIiM0NTQwNThcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIubGlzdHZpZXctcGF0aWVudFwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgbWFyZ2luLWxlZnQ6IDg7XFxuICBtYXJnaW4tcmlnaHQ6IDg7XFxuICBtYXJnaW4tYm90dG9tOiA4OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMzAgMzAgMCAwXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYmFja2dyb3VuZC1jb2xvclwiLFwidmFsdWVcIjpcIiNmZWU2ZDJcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGNvbG9yOiB3aGl0ZTsgXCJ9XX0se1widHlwZVwiOlwicnVsZVwiLFwic2VsZWN0b3JzXCI6W1wiLmxpc3R2aWV3LWl0ZW1cIl0sXCJkZWNsYXJhdGlvbnNcIjpbe1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi10b3A6IDE2O1xcbiAgbWFyZ2luLWxlZnQ6IDE2O1xcbiAgbWFyZ2luLXJpZ2h0OiAxNjtcXG4gIG1hcmdpbi1ib3R0b206IDE2OyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi10b3A6OCA7XFxuICBtYXJnaW4tbGVmdDogMTY7XFxuICBtYXJnaW4tcmlnaHQ6IDE2OyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJib3JkZXItcmFkaXVzXCIsXCJ2YWx1ZVwiOlwiMjBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn1dfSx7XCJ0eXBlXCI6XCJydWxlXCIsXCJzZWxlY3RvcnNcIjpbXCIucGF0aWVudC1pbWFnZVwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1sZWZ0XCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCI1MCVcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwiY3JpbXNvblwifV19LHtcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5hZGQtZG9jdG9yLWJ0blwiXSxcImRlY2xhcmF0aW9uc1wiOlt7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJvcmRlci1yYWRpdXNcIixcInZhbHVlXCI6XCIyMFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcInBhZGRpbmdcIixcInZhbHVlXCI6XCIxNlwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcImJhY2tncm91bmQtY29sb3JcIixcInZhbHVlXCI6XCJkYXJrb3JhbmdlXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiY29sb3JcIixcInZhbHVlXCI6XCJ3aGl0ZVwifV19XSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9tYWluL21haW4tcGFnZS5jc3NcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic3R5bGVcIiwgcGF0aDogXCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCB2aWV3ID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvdWkvY29yZS92aWV3XCIpO1xuY29uc3QgTWFpblBhZ2VNb2RlbCA9IHJlcXVpcmUoXCIuL21haW4tcGFnZS1tb2RlbFwiKTtcbmNvbnN0IGZyb21PYmplY3QgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIikuZnJvbU9iamVjdDtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5jb25zdCBwbGF0Zm9ybSA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3BsYXRmb3JtXCIpO1xuY29uc3QgZGlhbG9ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL3VpL2RpYWxvZ3NcIik7XG5jb25zdCBuc3RvYXN0cyA9IHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdG9hc3RzXCIpO1xuXG5jb25zdCBtb2RhbFZpZXdNb2R1bGUgPSBcIi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmpzXCI7XG5jb25zdCBwYXRpZW50Vmlld01vZHVsZSA9IFwiL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qc1wiO1xuXG5sZXQgZG9jdG9yID0gTWFpblBhZ2VNb2RlbC5Eb2N0b3JEYXRhKCk7XG5sZXQgcGFnZTtcbmxldCBtb2RhbFZpZXc7XG5sZXQgbmV3RG9jdG9yQnRuO1xuXG4vLyBEb2N0b3IxXG5sZXQgZGF0YTEgPSBbXTtcbmxldCBidG5Eb2N0b3IxO1xubGV0IGRvY3RvcjE7XG5cbi8vIERvY3RvcjJcbmxldCBkYXRhMiA9IFtdO1xubGV0IGJ0bkRvY3RvcjI7XG5sZXQgZG9jdG9yMjtcblxuLy8gRG9jdG9yM1xubGV0IGRhdGEzID0gW107XG5sZXQgYnRuRG9jdG9yMztcbmxldCBkb2N0b3IzO1xuXG5leHBvcnRzLm9uUGFnZUxvYWRlZCA9IChhcmdzKSA9PiB7XG4gICAgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgbW9kYWxWaWV3ID0gYXJncy5vYmplY3Q7XG4gICAgbmV3RG9jdG9yQnRuID0gcGFnZS5nZXRWaWV3QnlJZChcIm5ld2RvY3RvcmJ0blwiKTtcbiAgICBpZiAocGxhdGZvcm0uaXNJT1MpIHtcbiAgICAgICAgbmV3RG9jdG9yQnRuLnN0eWxlID0gXCJmb250LXNpemU6IDE4ZW07XCI7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgbmV3RG9jdG9yQnRuLnN0eWxlID0gXCJmb250LXNpemU6IDE0ZW07XCI7XG4gICAgfVxufTtcblxuZXhwb3J0cy5hZGREb2N0b3IxID0gKGFyZ3MpID0+IHtcbiAgICBidG5Eb2N0b3IxID0gcGFnZS5nZXRWaWV3QnlJZChcImJ0bkRvY3RvcjFcIik7XG4gICAgZG9jdG9yMSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJkb2N0b3IxXCIpO1xuXG4gICAgZGlhbG9nc1xuICAgICAgICAucHJvbXB0KHtcbiAgICAgICAgICAgIHRpdGxlOiBcIuC4geC4o+C4reC4geC4o+C4q+C4seC4quC5geC4nuC4l+C4ouC5jFwiLFxuICAgICAgICAgICAgbWVzc2FnZTogXCJcIixcbiAgICAgICAgICAgIG9rQnV0dG9uVGV4dDogXCLguJXguIHguKXguIdcIixcbiAgICAgICAgICAgIGNhbmNlbEJ1dHRvblRleHQ6IFwi4Lii4LiB4LmA4Lil4Li04LiBXCIsXG4gICAgICAgICAgICBjYW5jZWxhYmxlOiBmYWxzZSxcbiAgICAgICAgICAgIGlucHV0VHlwZTogZGlhbG9ncy5pbnB1dFR5cGUubnVtYmVyLFxuICAgICAgICB9KVxuICAgICAgICAudGhlbigocikgPT4ge1xuICAgICAgICAgICAgaWYgKHIucmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgbGV0IHBlcmlkID0gci50ZXh0O1xuICAgICAgICAgICAgICAgIGxldCBjdXJkYXRlID0gZ2V0RGF0ZVN0cmluZyhuZXcgRGF0ZSgpKTtcbiAgICAgICAgICAgICAgICBsZXQgY3VyRGF0ZVN0ciA9XG4gICAgICAgICAgICAgICAgICAgIGN1cmRhdGUueWVhciArIFwiLVwiICsgY3VyZGF0ZS5tb250aCArIFwiLVwiICsgY3VyZGF0ZS5kYXk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJQZXJpZDogXCIgKyBwZXJpZCk7XG4gICAgICAgICAgICAgICAgLy8gY29uc29sZS5sb2coXCJDdXJkYXRlOiBcIiArIGN1ckRhdGVTdHIpO1xuICAgICAgICAgICAgICAgIGdldERvY3RvckluZm8oXCJuZXdcIiwgXCIxXCIsIFwiMTM0OVwiKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLguKLguIHguYDguKXguLTguIFcIik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xufTtcblxuZXhwb3J0cy5vblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQxID0gKGFyZ3MpID0+IHtcbiAgICBsZXQgcGVyaWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJkb2N0b3IxXCIpO1xuICAgIGxldCBsaXN0VmlldzEgPSBwYWdlLmdldFZpZXdCeUlkKFwibGlzdHZpZXcxXCIpO1xuICAgIGdldERvY3RvckluZm8oXCJyZWxvYWRcIiwgXCIxXCIsIHBlcmlkKTtcbiAgICBsZXQgb3B0aW9ucyA9IHtcbiAgICAgICAgdGV4dDogXCLguK3guLHguJ7guYDguJTguJfguILguYnguK3guKHguLnguKXguKrguLPguYDguKPguYfguIhcIixcbiAgICAgICAgZHVyYXRpb246IG5zdG9hc3RzLkRVUkFUSU9OLlNIT1JULFxuICAgICAgICBwb3NpdGlvbjogbnN0b2FzdHMuUE9TSVRJT04uQk9UVE9NLCAvL29wdGlvbmFsXG4gICAgfTtcbiAgICBuc3RvYXN0cy5zaG93KG9wdGlvbnMpO1xuICAgIHNldFRpbWVvdXQoKCkgPT4ge1xuICAgICAgICBsaXN0VmlldzEubm90aWZ5UHVsbFRvUmVmcmVzaEZpbmlzaGVkKCk7XG4gICAgfSwgNTAwKTtcbn07XG5cbmV4cG9ydHMub25JdGVtU2VsZWN0ZWQgPSAoYXJncykgPT4ge1xuICAgIGNvbnN0IHBhZ2UgPSBhcmdzLm9iamVjdC5wYWdlO1xuICAgIGxldCBsaXN0dmlld0luZGV4ID0gYXJncy5vYmplY3QuaW5kZXg7XG4gICAgbGV0IGxpc3RWaWV3ID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3R2aWV3XCIgKyBsaXN0dmlld0luZGV4KTtcbiAgICBsZXQgc2VsZWN0ZWRJdGVtcyA9IGxpc3RWaWV3LmdldFNlbGVjdGVkSXRlbXMoKTtcbiAgICAvLyBjb25zb2xlLmxvZyhzZWxlY3RlZEl0ZW1zKTtcbiAgICBsZXQgcGF0aWVudGRhdGEgPSBzZWxlY3RlZEl0ZW1zWzBdO1xuICAgIGxldCBwTmFtZSA9IHBhdGllbnRkYXRhW1wibmFtZVwiICsgbGlzdHZpZXdJbmRleF07XG4gICAgbGV0IHBITiA9IHBhdGllbnRkYXRhW1wiaG5cIiArIGxpc3R2aWV3SW5kZXhdO1xuICAgIGxldCBxVGltZSA9IHBhdGllbnRkYXRhW1wicXRpbWVcIiArIGxpc3R2aWV3SW5kZXhdO1xuICAgIGNvbnN0IG9wdGlvbiA9IHtcbiAgICAgICAgY29udGV4dDoge1xuICAgICAgICAgICAgbmFtZTogcE5hbWUsXG4gICAgICAgICAgICBobjogcEhOLFxuICAgICAgICAgICAgdGltZTogcVRpbWUsXG4gICAgICAgIH0sXG4gICAgICAgIGNsb3NlQ2FsbGJhY2s6IChwYXRpZW50aW5mbykgPT4ge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBwYXRpZW50aW5mbyA9PSBcIm9iamVjdFwiKSB7XG4gICAgICAgICAgICAgICAgY29uc29sZS5sb2cocGF0aWVudGluZm8pO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImNsb3NlIG1vZGFsXCIpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LFxuICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcbiAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgfTtcbiAgICBtb2RhbFZpZXcuc2hvd01vZGFsKHBhdGllbnRWaWV3TW9kdWxlLCBvcHRpb24pO1xuICAgIC8vIHN3aXRjaCAobGlzdHZpZXdJbmRleCkge1xuICAgIC8vICAgICBjYXNlIFwiMVwiOlxuICAgIC8vICAgICAgICAgY29uc29sZS5sb2coXCJMaXN0VmlldyAxXCIpO1xuICAgIC8vICAgICAgICAgdmFyIHNlbGVjdGVkSXRlbXMgPSBsaXN0Vmlldy5nZXRTZWxlY3RlZEl0ZW1zKCk7XG4gICAgLy8gICAgICAgICBjb25zb2xlLmxvZyhzZWxlY3RlZEl0ZW1zKTtcbiAgICAvLyAgICAgICAgIGJyZWFrO1xuICAgIC8vICAgICBjYXNlIFwiMlwiOlxuICAgIC8vICAgICAgICAgY29uc29sZS5sb2coXCJMaXN0VmlldyAyXCIpO1xuICAgIC8vICAgICAgICAgYnJlYWs7XG4gICAgLy8gICAgIGRlZmF1bHQ6XG4gICAgLy8gICAgICAgICBicmVhaztcbiAgICAvLyB9XG59O1xuXG5leHBvcnRzLmFkZERvY3RvcjIgPSAoKSA9PiB7XG4gICAgYnRuRG9jdG9yMiA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJidG5Eb2N0b3IyXCIpO1xuICAgIGRvY3RvcjIgPSBwYWdlLmdldFZpZXdCeUlkKFwiZG9jdG9yMlwiKTtcblxuICAgIGRpYWxvZ3NcbiAgICAgICAgLnByb21wdCh7XG4gICAgICAgICAgICB0aXRsZTogXCLguIHguKPguK3guIHguKPguKvguLHguKrguYHguJ7guJfguKLguYxcIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiXCIsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwi4LiV4LiB4Lil4LiHXCIsXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIuC4ouC4geC5gOC4peC4tOC4gVwiLFxuICAgICAgICAgICAgY2FuY2VsYWJsZTogZmFsc2UsXG4gICAgICAgICAgICBpbnB1dFR5cGU6IGRpYWxvZ3MuaW5wdXRUeXBlLm51bWJlcixcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4oKHIpID0+IHtcbiAgICAgICAgICAgIGlmIChyLnJlc3VsdCkge1xuICAgICAgICAgICAgICAgIGxldCBwZXJpZCA9IHIudGV4dDtcbiAgICAgICAgICAgICAgICBsZXQgY3VyZGF0ZSA9IGdldERhdGVTdHJpbmcobmV3IERhdGUoKSk7XG4gICAgICAgICAgICAgICAgbGV0IGN1ckRhdGVTdHIgPVxuICAgICAgICAgICAgICAgICAgICBjdXJkYXRlLnllYXIgKyBcIi1cIiArIGN1cmRhdGUubW9udGggKyBcIi1cIiArIGN1cmRhdGUuZGF5O1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiUGVyaWQ6IFwiICsgcGVyaWQpO1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiQ3VyZGF0ZTogXCIgKyBjdXJEYXRlU3RyKTtcbiAgICAgICAgICAgICAgICBnZXREb2N0b3JJbmZvKFwibmV3XCIsIFwiMlwiLCBcIjExNDAwXCIpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuC4ouC4geC5gOC4peC4tOC4gVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG59O1xuXG5leHBvcnRzLm9uUHVsbFRvUmVmcmVzaEluaXRpYXRlZDIgPSAoKSA9PiB7XG4gICAgbGV0IHBlcmlkID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yMlwiKTtcbiAgICBsZXQgbGlzdFZpZXcyID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3R2aWV3MlwiKTtcbiAgICBnZXREb2N0b3JJbmZvKFwicmVsb2FkXCIsIFwiMlwiLCBwZXJpZCk7XG4gICAgbGV0IG9wdGlvbnMgPSB7XG4gICAgICAgIHRleHQ6IFwi4Lit4Lix4Lie4LmA4LiU4LiX4LiC4LmJ4Lit4Lih4Li54Lil4Liq4Liz4LmA4Lij4LmH4LiIXCIsXG4gICAgICAgIGR1cmF0aW9uOiBuc3RvYXN0cy5EVVJBVElPTi5TSE9SVCxcbiAgICAgICAgcG9zaXRpb246IG5zdG9hc3RzLlBPU0lUSU9OLkJPVFRPTSwgLy9vcHRpb25hbFxuICAgIH07XG4gICAgbnN0b2FzdHMuc2hvdyhvcHRpb25zKTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgbGlzdFZpZXcyLm5vdGlmeVB1bGxUb1JlZnJlc2hGaW5pc2hlZCgpO1xuICAgIH0sIDUwMCk7XG59O1xuXG5leHBvcnRzLmFkZERvY3RvcjMgPSAoKSA9PiB7XG4gICAgYnRuRG9jdG9yMyA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJidG5Eb2N0b3IzXCIpO1xuICAgIGRvY3RvcjMgPSBwYWdlLmdldFZpZXdCeUlkKFwiZG9jdG9yM1wiKTtcblxuICAgIGRpYWxvZ3NcbiAgICAgICAgLnByb21wdCh7XG4gICAgICAgICAgICB0aXRsZTogXCLguIHguKPguK3guIHguKPguKvguLHguKrguYHguJ7guJfguKLguYxcIixcbiAgICAgICAgICAgIG1lc3NhZ2U6IFwiXCIsXG4gICAgICAgICAgICBva0J1dHRvblRleHQ6IFwi4LiV4LiB4Lil4LiHXCIsXG4gICAgICAgICAgICBjYW5jZWxCdXR0b25UZXh0OiBcIuC4ouC4geC5gOC4peC4tOC4gVwiLFxuICAgICAgICAgICAgY2FuY2VsYWJsZTogZmFsc2UsXG4gICAgICAgICAgICBpbnB1dFR5cGU6IGRpYWxvZ3MuaW5wdXRUeXBlLm51bWJlcixcbiAgICAgICAgfSlcbiAgICAgICAgLnRoZW4oKHIpID0+IHtcbiAgICAgICAgICAgIGlmIChyLnJlc3VsdCkge1xuICAgICAgICAgICAgICAgIGxldCBwZXJpZCA9IHIudGV4dDtcbiAgICAgICAgICAgICAgICBsZXQgY3VyZGF0ZSA9IGdldERhdGVTdHJpbmcobmV3IERhdGUoKSk7XG4gICAgICAgICAgICAgICAgbGV0IGN1ckRhdGVTdHIgPVxuICAgICAgICAgICAgICAgICAgICBjdXJkYXRlLnllYXIgKyBcIi1cIiArIGN1cmRhdGUubW9udGggKyBcIi1cIiArIGN1cmRhdGUuZGF5O1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiUGVyaWQ6IFwiICsgcGVyaWQpO1xuICAgICAgICAgICAgICAgIC8vIGNvbnNvbGUubG9nKFwiQ3VyZGF0ZTogXCIgKyBjdXJEYXRlU3RyKTtcbiAgICAgICAgICAgICAgICBnZXREb2N0b3JJbmZvKFwibmV3XCIsIFwiM1wiLCBcIjExNDAwXCIpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcIuC4ouC4geC5gOC4peC4tOC4gVwiKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG59O1xuXG5leHBvcnRzLm9uUHVsbFRvUmVmcmVzaEluaXRpYXRlZDMgPSAoKSA9PiB7XG4gICAgbGV0IHBlcmlkID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiZG9jdG9yM1wiKTtcbiAgICBsZXQgbGlzdFZpZXczID0gcGFnZS5nZXRWaWV3QnlJZChcImxpc3R2aWV3M1wiKTtcbiAgICBnZXREb2N0b3JJbmZvKFwicmVsb2FkXCIsIFwiM1wiLCBwZXJpZCk7XG4gICAgbGV0IG9wdGlvbnMgPSB7XG4gICAgICAgIHRleHQ6IFwi4Lit4Lix4Lie4LmA4LiU4LiX4LiC4LmJ4Lit4Lih4Li54Lil4Liq4Liz4LmA4Lij4LmH4LiIXCIsXG4gICAgICAgIGR1cmF0aW9uOiBuc3RvYXN0cy5EVVJBVElPTi5TSE9SVCxcbiAgICAgICAgcG9zaXRpb246IG5zdG9hc3RzLlBPU0lUSU9OLkJPVFRPTSwgLy9vcHRpb25hbFxuICAgIH07XG4gICAgbnN0b2FzdHMuc2hvdyhvcHRpb25zKTtcbiAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgbGlzdFZpZXczLm5vdGlmeVB1bGxUb1JlZnJlc2hGaW5pc2hlZCgpO1xuICAgIH0sIDUwMCk7XG59O1xuXG4vLyBTdGVwIDEg4LiV4Lij4Lin4LiI4Liq4Lit4Lia4LiC4LmJ4Lit4Lih4Li54Lil4LmB4Lie4LiX4Lii4LmMXG5mdW5jdGlvbiBnZXREb2N0b3JJbmZvKGFjdGlvbiwgdGFiLCBwZXJpZCkge1xuICAgIGxldCBkb2N0b3JJbmZvUHJvbXMgPSBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgIGRvY3RvclxuICAgICAgICAgICAgLmdldERvY3RvckluZm8ocGVyaWQpXG4gICAgICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oKGluZm9yZXNwb25zZSkgPT4ge1xuICAgICAgICAgICAgICAgIGxldCBpbmZvID0gaW5mb3Jlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgaWYgKGluZm8ubGVuZ3RoICE9IDApIHtcbiAgICAgICAgICAgICAgICAgICAgYXBwU2V0dGluZ3MucmVtb3ZlKFwiZG9jdG9yXCIgKyB0YWIpO1xuICAgICAgICAgICAgICAgICAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJkb2N0b3JcIiArIHRhYiwgcGVyaWQpO1xuICAgICAgICAgICAgICAgICAgICAvLyBsZXQgZG9jdG9ybmFtZSA9IGRvY3RvcmluZm9bMF0uZG9jdG9ybmFtZTtcbiAgICAgICAgICAgICAgICAgICAgLy8gbGV0IGRvY3RvcnNleCA9IGRvY3RvcmluZm9bMF0uc2V4O1xuICAgICAgICAgICAgICAgICAgICBsZXQgZG9jdG9yaW5mbyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRvY3Rvcm5hbWU6IGluZm9bMF0uZG9jdG9ybmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgIHNleDogaW5mb1swXS5zZXgsXG4gICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoZG9jdG9yaW5mbyk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coXCLguYTguKHguYjguJ7guJrguILguYnguK3guKHguLnguKXguYHguJ7guJfguKLguYxcIik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgfSk7XG4gICAgZG9jdG9ySW5mb1Byb21zLnRoZW4oKGRvY3RvcmluZm8pID0+IHtcbiAgICAgICAgZ2V0RG9jdG9yQ2xpbmljSW5mbyhhY3Rpb24sIHRhYiwgcGVyaWQsIFwiMjAyMC0wNy0wNVwiLCBkb2N0b3JpbmZvKTtcbiAgICB9KTtcbn1cblxuLy8gU3RlcCAyIOC4leC4o+C4p+C4iOC4quC4reC4muC4guC5ieC4reC4oeC4ueC4peC4hOC4peC4tOC4meC4tOC4geC4l+C4teC5iOC5gOC4peC4t+C4reC4gVxuZnVuY3Rpb24gZ2V0RG9jdG9yQ2xpbmljSW5mbyhhY3Rpb24sIHRhYiwgcGVyaWQsIGN1cmRhdGUsIGRvY3RvcmluZm8pIHtcbiAgICBsZXQgY2xpbmljSW5mb1Byb21zID0gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICBkb2N0b3JcbiAgICAgICAgICAgIC5nZXRDbGluaWMocGVyaWQsIGN1cmRhdGUpXG4gICAgICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVqZWN0KCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLnRoZW4oKHJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICAgICAgbGV0IGNsaW5pY0xpc3QgPSByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgIGlmIChjbGluaWNMaXN0Lmxlbmd0aCAhPSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJlc29sdmUoY2xpbmljTGlzdCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZShbXSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwi4LmE4Lih4LmI4Lih4Li14LiE4Lil4Li04LiZ4Li04LiBXCIpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgIH0pO1xuICAgIGNsaW5pY0luZm9Qcm9tcy50aGVuKChjbGluaWNpbmZvKSA9PiB7XG4gICAgICAgIGdldERvY3RvclF1ZXVlKGFjdGlvbiwgdGFiLCBjbGluaWNpbmZvLCBkb2N0b3JpbmZvKTtcbiAgICB9KTtcbn1cblxuLy8gU3RlcCAzIOC4lOC4tuC4h+C4guC5ieC4reC4oeC4ueC4peC4hOC4tOC4p+C4leC4o+C4p+C4iOC4guC4reC4h+C5geC4nuC4l+C4ouC5jOC5g+C4meC4hOC4peC4tOC4meC4tOC4geC4meC4seC5ieC4meC5hlxuZnVuY3Rpb24gZ2V0RG9jdG9yUXVldWUoYWN0aW9uLCB0YWIsIGNsaW5pY2luZm8sIGRvY3RvcmluZm8pIHtcbiAgICBpZiAoY2xpbmljaW5mby5sZW5ndGggIT0gMCkge1xuICAgICAgICAvLyBjb25zdCBtYWluVmlldyA9IGFyZ3Mub2JqZWN0O1xuICAgICAgICBzd2l0Y2ggKGFjdGlvbikge1xuICAgICAgICAgICAgY2FzZSBcIm5ld1wiOlxuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbiA9IHtcbiAgICAgICAgICAgICAgICAgICAgY29udGV4dDoge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiB0YWIsXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGluaWNsaXN0OiBjbGluaWNpbmZvLFxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBjbG9zZUNhbGxiYWNrOiAoZG9jdG9ycXVldWVpbmZvKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGRvY3RvcnF1ZXVlaW5mbyA9PSBcIm9iamVjdFwiKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IHF1ZXVlbGlzdCA9IGRvY3RvcnF1ZXVlaW5mby5xdWV1ZUxpc3Q7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc2V0UXVldWVMaXN0KFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY3Rpb24sXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jdG9yaW5mbyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jdG9ycXVldWVpbmZvLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWV1ZWxpc3RcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhcImNsb3NlIG1vZGFsXCIpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICBmdWxsc2NyZWVuOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgYW5pbWF0ZWQ6IHRydWUsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICBtb2RhbFZpZXcuc2hvd01vZGFsKG1vZGFsVmlld01vZHVsZSwgb3B0aW9uKTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCJyZWxvYWRcIjpcbiAgICAgICAgICAgICAgICBsZXQgZG9jdG9yUGVyaWQgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJkb2N0b3JcIiArIHRhYik7XG4gICAgICAgICAgICAgICAgbGV0IGNsaW5pY0NvZGUgPSBhcHBTZXR0aW5ncy5nZXRTdHJpbmcoXCJjbGluaWNjb2RlXCIgKyB0YWIpO1xuICAgICAgICAgICAgICAgIGxldCBjbGluaWNOYW1lID0gYXBwU2V0dGluZ3MuZ2V0U3RyaW5nKFwiY2xpbmljbmFtZVwiICsgdGFiKTtcbiAgICAgICAgICAgICAgICBkb2N0b3JcbiAgICAgICAgICAgICAgICAgICAgLmdldERvY3RvclF1ZXVlKFwiMjAyMC0wNy0xMlwiLCBjbGluaWNDb2RlLCBkb2N0b3JQZXJpZClcbiAgICAgICAgICAgICAgICAgICAgLmNhdGNoKChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKGVycik7XG4gICAgICAgICAgICAgICAgICAgICAgICByZWplY3QoZXJyKTtcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICAgICAgLnRoZW4oKGNsaW5pY3F1ZXVlcmVzcG9uc2UpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCBxdWV1ZWxpc3QgPSBjbGluaWNxdWV1ZXJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgZG9jdG9ycXVldWVpbmZvO1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHF1ZXVlbGlzdC5sZW5ndGggIT0gMCkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcnF1ZXVlaW5mbyA9IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogY2xpbmljTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBxdWV1ZWxpc3QsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZG9jdG9ycXVldWVpbmZvID0ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGluaWNDb2RlOiBjbGluaWNDb2RlLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjbGluaWNOYW1lOiBcIlwiLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWV1ZUxpc3Q6IFtdLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICBzZXRRdWV1ZUxpc3QoXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgYWN0aW9uLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRhYixcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBkb2N0b3JpbmZvLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvY3RvcnF1ZXVlaW5mbyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWV1ZWxpc3RcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4LmE4Lih4LmI4Lie4Lia4LiC4LmJ4Lit4Lih4Li54Lil4LiE4Lil4Li04LiZ4Li04LiBXCIpO1xuICAgIH1cbn1cblxuLy8gU3RlcCA0IOC5gOC4iuC5h+C4hCBhY3Rpb24g4Lin4LmI4Liy4LmA4Lib4LmH4LiZIG5ldyDguKvguKPguLfguK0gcmVsb2FkXG5mdW5jdGlvbiBzZXRRdWV1ZUxpc3QoYWN0aW9uLCB0YWIsIGRvY3RvcmluZm8sIGRvY3RvcnF1ZXVlaW5mbywgcXVldWVsaXN0KSB7XG4gICAgbGV0IGRhdGEgPSBbXTtcbiAgICBpZiAocXVldWVsaXN0Lmxlbmd0aCAhPSAwKSB7XG4gICAgICAgIHF1ZXVlbGlzdC5mb3JFYWNoKChlbGVtZW50KSA9PiB7XG4gICAgICAgICAgICBsZXQgbmFtZSA9IFwibmFtZVwiICsgdGFiO1xuICAgICAgICAgICAgbGV0IGhuID0gXCJoblwiICsgdGFiO1xuICAgICAgICAgICAgbGV0IHF0aW1lID0gXCJxdGltZVwiICsgdGFiO1xuICAgICAgICAgICAgbGV0IHBhdGllbnRpbWcgPSBcInBhdGllbnRpbWdcIiArIHRhYjtcbiAgICAgICAgICAgIGxldCBwYXRpZW50aW1nUGF0aCA9IFwiXCI7XG4gICAgICAgICAgICBzd2l0Y2ggKGVsZW1lbnQuc2V4KSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjFcIjpcbiAgICAgICAgICAgICAgICAgICAgcGF0aWVudGltZ1BhdGggPSBcIn4vc3JjL2ltZy9tYW4tcGF0aWVudC5wbmdcIjtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjJcIjpcbiAgICAgICAgICAgICAgICAgICAgcGF0aWVudGltZ1BhdGggPSBcIn4vc3JjL2ltZy93b21hbi1wYXRpZW50LnBuZ1wiO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGRhdGEucHVzaCh7XG4gICAgICAgICAgICAgICAgW25hbWVdOiBlbGVtZW50LnBhdGllbnRuYW1lLFxuICAgICAgICAgICAgICAgIFtobl06IFwiSE5cIiArIGVsZW1lbnQuaG4sXG4gICAgICAgICAgICAgICAgW3F0aW1lXTogZWxlbWVudC5xX3RpbWUsXG4gICAgICAgICAgICAgICAgW3BhdGllbnRpbWddOiBwYXRpZW50aW1nUGF0aCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICAgICAgaWYgKGFjdGlvbiA9PSBcIm5ld1wiKSB7XG4gICAgICAgICAgICBsZXQgY05hbWUgPSBkb2N0b3JxdWV1ZWluZm8uY2xpbmljTmFtZTtcbiAgICAgICAgICAgIGxldCBwcm9maWxlaW1nID0gcGFnZS5nZXRWaWV3QnlJZChcInByb2ZpbGVpbWdcIiArIHRhYik7XG4gICAgICAgICAgICBsZXQgZG9jdG9ybmFtZSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJkb2N0b3JuYW1lXCIgKyB0YWIpO1xuICAgICAgICAgICAgbGV0IGNsaW5pY25hbWUgPSBwYWdlLmdldFZpZXdCeUlkKFwiY2xpbmljbmFtZVwiICsgdGFiKTtcblxuICAgICAgICAgICAgbGV0IGROYW1lID0gZG9jdG9yaW5mby5kb2N0b3JuYW1lO1xuICAgICAgICAgICAgbGV0IGRTZXggPSBkb2N0b3JpbmZvLnNleDtcbiAgICAgICAgICAgIC8vIENoZWNrIGRvY3RvciBzZXggdG8gc2V0IGRlZmF1bHQgcHJvZmlsZSBpbWFnZVxuICAgICAgICAgICAgc3dpdGNoIChkU2V4KSB7XG4gICAgICAgICAgICAgICAgY2FzZSBcIjFcIjpcbiAgICAgICAgICAgICAgICAgICAgcHJvZmlsZWltZy5zcmMgPSBcIn4vc3JjL2ltZy93b21hbi1kb2N0b3IucG5nXCI7XG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgXCIyXCI6XG4gICAgICAgICAgICAgICAgICAgIHByb2ZpbGVpbWcuc3JjID0gXCJ+L3NyYy9pbWcvbWFuLWRvY3Rvci5wbmdcIjtcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGRvY3Rvcm5hbWUudGV4dCA9IGROYW1lO1xuICAgICAgICAgICAgY2xpbmljbmFtZS50ZXh0ID0gXCLguITguKXguLTguJnguLTguIFcIiArIGNOYW1lLnRyaW0oKTtcbiAgICAgICAgfVxuICAgICAgICBzd2l0Y2ggKHRhYikge1xuICAgICAgICAgICAgY2FzZSBcIjFcIjpcbiAgICAgICAgICAgICAgICBkYXRhMSA9IGRhdGE7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBjYXNlIFwiMlwiOlxuICAgICAgICAgICAgICAgIGRhdGEyID0gZGF0YTtcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgIGNhc2UgXCIzXCI6XG4gICAgICAgICAgICAgICAgZGF0YTMgPSBkYXRhO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgfVxuICAgICAgICBzZXRSYWRMaXN0Vmlldyh0YWIpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbnNvbGUubG9nKFwi4LmE4Lih4LmI4Lie4Lia4LiC4LmJ4Lit4Lih4Li54Lil4LiE4Li04Lin4LiV4Lij4Lin4LiIXCIpO1xuICAgIH1cbn1cblxuLy8gU3RlcCA1IOC4meC4s+C4guC5ieC4reC4oeC4ueC4peC4hOC4tOC4p+C5g+C4quC5iOC5g+C4mSBsaXN0XG5mdW5jdGlvbiBzZXRSYWRMaXN0Vmlldyh0YWIpIHtcbiAgICBjb25zdCB2bSA9IGZyb21PYmplY3Qoe1xuICAgICAgICBkYXRhSXRlbXMxOiBkYXRhMSxcbiAgICAgICAgZGF0YUl0ZW1zMjogZGF0YTIsXG4gICAgICAgIGRhdGFJdGVtczM6IGRhdGEzLFxuICAgIH0pO1xuICAgIHBhZ2UuYmluZGluZ0NvbnRleHQgPSB2bTtcblxuICAgIHN3aXRjaCAodGFiKSB7XG4gICAgICAgIGNhc2UgXCIxXCI6XG4gICAgICAgICAgICBidG5Eb2N0b3IxLnZpc2liaWxpdHkgPSBcImNvbGxhcHNlXCI7XG4gICAgICAgICAgICBidG5Eb2N0b3IxLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDAsXG4gICAgICAgICAgICAgICAgZHVyYXRpb246IDEwMDAsXG4gICAgICAgICAgICB9KTtcblxuICAgICAgICAgICAgZG9jdG9yMS52aXNpYmlsaXR5ID0gXCJ2aXNpYmxlXCI7XG4gICAgICAgICAgICBkb2N0b3IxLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICAgICAgZHVyYXRpb246IDEwMDAsXG4gICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICBjYXNlIFwiMlwiOlxuICAgICAgICAgICAgYnRuRG9jdG9yMi52aXNpYmlsaXR5ID0gXCJjb2xsYXBzZVwiO1xuICAgICAgICAgICAgYnRuRG9jdG9yMi5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAxMDAwLFxuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgICAgIGRvY3RvcjIudmlzaWJpbGl0eSA9IFwidmlzaWJsZVwiO1xuICAgICAgICAgICAgZG9jdG9yMi5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiAxMDAwLFxuICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICBicmVhaztcbiAgICAgICAgY2FzZSBcIjNcIjpcbiAgICAgICAgICAgIGJ0bkRvY3RvcjMudmlzaWJpbGl0eSA9IFwiY29sbGFwc2VcIjtcbiAgICAgICAgICAgIGJ0bkRvY3RvcjMuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogMTAwMCxcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICBkb2N0b3IzLnZpc2liaWxpdHkgPSBcInZpc2libGVcIjtcbiAgICAgICAgICAgIGRvY3RvcjMuYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogMTAwMCxcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICBicmVhaztcbiAgICB9XG59XG5cbmZ1bmN0aW9uIGdldERhdGVTdHJpbmcoZGF0ZSkge1xuICAgIGxldCBtb250aFN0cjtcbiAgICBsZXQgZGF5U3RyO1xuICAgIGlmIChwYXJzZUludChkYXRlLmdldE1vbnRoKCkgKyAxKSA8PSA5KSB7XG4gICAgICAgIG1vbnRoU3RyID0gXCIwXCIgKyAoZGF0ZS5nZXRNb250aCgpICsgMSk7XG4gICAgfSBlbHNlIHtcbiAgICAgICAgbW9udGhTdHIgPSBkYXRlLmdldE1vbnRoKCkgKyAxO1xuICAgIH1cblxuICAgIGlmIChwYXJzZUludChkYXRlLmdldERhdGUoKSkgPD0gOSkge1xuICAgICAgICBkYXlTdHIgPSBcIjBcIiArIGRhdGUuZ2V0RGF0ZSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICAgIGRheVN0ciA9IGRhdGUuZ2V0RGF0ZSgpO1xuICAgIH1cblxuICAgIGxldCBuZXdEYXRlRm9ybWF0ID0ge1xuICAgICAgICB5ZWFyOiBkYXRlLmdldEZ1bGxZZWFyKCksXG4gICAgICAgIG1vbnRoOiBtb250aFN0cixcbiAgICAgICAgZGF5OiBkYXlTdHIsXG4gICAgfTtcblxuICAgIHJldHVybiBuZXdEYXRlRm9ybWF0O1xufVxuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21haW4vbWFpbi1wYWdlLmpzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInNjcmlwdFwiLCBwYXRoOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UuanNcIiB9KTtcbiAgICB9KTtcbn0gIiwiZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXdcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcbmdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlldy9SYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGVcIiwgZnVuY3Rpb24oKSB7IHJldHVybiByZXF1aXJlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XCIpOyB9KTtcblxubW9kdWxlLmV4cG9ydHMgPSBcIjxQYWdlIHhtbG5zPVxcXCJodHRwOi8vc2NoZW1hcy5uYXRpdmVzY3JpcHQub3JnL3Rucy54c2RcXFwiIFxcbiAgICB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIiBsb2FkZWQ9XFxcIm9uUGFnZUxvYWRlZFxcXCI+XFxuXFxuICAgIDxQYWdlLmFjdGlvbkJhcj5cXG4gICAgICAgIDxBY3Rpb25CYXIgYmFja2dyb3VuZENvbG9yPVxcXCIjMWU0YjI1XFxcIiBjb2xvcj1cXFwid2hpdGVcXFwiPlxcbiAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwiaGVhZGVyXFxcIiBpZD1cXFwiaGVhZGVyXFxcIiB0ZXh0PVxcXCLguITguLTguKfguJXguKPguKfguIjguJfguLHguYnguIfguKvguKHguJRcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPEFjdGlvbkl0ZW0gY2xhc3M9XFxcImZhc1xcXCIgaWQ9XFxcIm5ld2RvY3RvcmJ0blxcXCIgaWNvbj1cXFwiZm9udDovLyYjeGYwNTU7XFxcIiBpb3MucG9zaXRpb249XFxcInJpZ2h0XFxcIiAvPlxcbiAgICAgICAgPC9BY3Rpb25CYXI+XFxuICAgIDwvUGFnZS5hY3Rpb25CYXI+XFxuXFxuICAgIDxCb3R0b21OYXZpZ2F0aW9uIGlkPVxcXCJib3R0b21OYXZcXFwiIHNlbGVjdGVkSW5kZXg9XFxcIjBcXFwiPlxcbiAgICAgICAgPFRhYlN0cmlwPlxcbiAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0gY2xhc3M9XFxcImZhc1xcXCIgdGFwPVxcXCJkb2N0b3JxdWV1ZVxcXCI+XFxuICAgICAgICAgICAgICAgIDxJbWFnZSBzcmM9XFxcImZvbnQ6Ly8mI3hmMGYwO1xcXCI+PC9JbWFnZT5cXG4gICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnRcXFwiIHRleHQ9XFxcIuC5geC4nuC4l+C4ouC5jFxcXCIgZm9udFNpemU9XFxcIjE0XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcblxcbiAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0gY2xhc3M9XFxcInRhYnN0cmlwaXRlbSBmYXNcXFwiIHRhcD1cXFwic2V0dGluZ3NcXFwiPlxcbiAgICAgICAgICAgICAgICA8SW1hZ2Ugc3JjPVxcXCJmb250Oi8vJiN4ZjA4NTtcXFwiPjwvSW1hZ2U+XFxuICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250XFxcIiB0ZXh0PVxcXCLguJXguLHguYnguIfguITguYjguLJcXFwiIGZvbnRTaXplPVxcXCIxNFxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgIDwvVGFiU3RyaXA+XFxuXFxuICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgPEdyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgIDxUYWJzIGlkPVxcXCJkb2N0b3J0YWJzXFxcIiBzZWxlY3RlZEluZGV4PVxcXCIwXFxcIiBzZWxlY3RlZEluZGV4Q2hhbmdlZD1cXFwib25JbmRleFNlbGVjdGVkXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxUYWJTdHJpcD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8VGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWxcXFwiIHRleHQ9XFxcIuC5geC4nuC4l+C4ouC5jCAxXFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvVGFiU3RyaXBJdGVtPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxUYWJTdHJpcEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbFxcXCIgdGV4dD1cXFwi4LmB4Lie4LiX4Lii4LmMIDJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9UYWJTdHJpcEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFRhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsXFxcIiB0ZXh0PVxcXCLguYHguJ7guJfguKLguYwgM1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1RhYlN0cmlwSXRlbT5cXG4gICAgICAgICAgICAgICAgICAgIDwvVGFiU3RyaXA+XFxuXFxuICAgICAgICAgICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBpZD1cXFwiYnRuRG9jdG9yMVxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcInZpc2libGVcXFwiIG9wYWNpdHk9XFxcIjFcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwidGV4dC1sYWJlbCBhZGQtZG9jdG9yLWJ0blxcXCIgdGV4dD1cXFwi4LmA4Lie4Li04LmI4Lih4LmB4Lie4LiX4Lii4LmMIDFcXFwiIHRhcD1cXFwiYWRkRG9jdG9yMVxcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGlkPVxcXCJkb2N0b3IxXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNlZWVcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwiY29sbGFwc2VcXFwiIG9wYWNpdHk9XFxcIjBcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJjYXJkLWRvY3Rvci1wcm9maWxlIG0tMTYgcC04IHRleHQtY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBpZD1cXFwicHJvZmlsZWltZzFcXFwiIHdpZHRoPVxcXCIxMjBcXFwiIGhlaWdodD1cXFwiMTIwXFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcIm0tMTZcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTFcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiY2xpbmljbmFtZTFcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguKvguYnguK3guIfguJXguKPguKfguIhcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguIjguLPguJnguKfguJnguJzguLnguYnguJvguYjguKfguKJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXcxXFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczF9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQxXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIGluZGV4PVxcXCIxXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKiwgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJwYXRpZW50LWltYWdlXFxcIiBzcmM9XFxcInt7cGF0aWVudGltZzF9fVxcXCIgd2lkdGg9XFxcIjYwXFxcIiBoZWlnaHQ9XFxcIjYwXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIG9yaWVudGF0aW9uPVxcXCJ2ZXJ0aWNhbFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLTggcC1iLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lMX19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IHAtbC04IHAtci04IHAtdC04XFxcIiBmb250U2l6ZT1cXFwiMTZcXFwiIHRleHQ9XFxcInt7aG4xfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IHRleHQtcmlnaHQgbS1yLTE2XFxcIiBmb250U2l6ZT1cXFwiMTZcXFwiIHRleHQ9XFxcInt7cXRpbWUxfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2x2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXc+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxuXFxuICAgICAgICAgICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBpZD1cXFwiYnRuRG9jdG9yMlxcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcInZpc2libGVcXFwiIG9wYWNpdHk9XFxcIjFcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwidGV4dC1sYWJlbCBhZGQtZG9jdG9yLWJ0blxcXCIgdGV4dD1cXFwi4LmA4Lie4Li04LmI4Lih4LmB4Lie4LiX4Lii4LmMIDJcXFwiIHRhcD1cXFwiYWRkRG9jdG9yMlxcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGlkPVxcXCJkb2N0b3IyXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNlZWVcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwiY29sbGFwc2VcXFwiIG9wYWNpdHk9XFxcIjBcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJjYXJkLWRvY3Rvci1wcm9maWxlIG0tMTYgcC04IHRleHQtY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBpZD1cXFwicHJvZmlsZWltZzJcXFwiIHdpZHRoPVxcXCIxMjBcXFwiIGhlaWdodD1cXFwiMTIwXFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcIm0tMTZcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiY2xpbmljbmFtZTJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguKvguYnguK3guIfguJXguKPguKfguIhcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguIjguLPguJnguKfguJnguJzguLnguYnguJvguYjguKfguKJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXcyXFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczJ9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiBwdWxsVG9SZWZyZXNoPVxcXCJ0cnVlXFxcIiBwdWxsVG9SZWZyZXNoSW5pdGlhdGVkPVxcXCJvblB1bGxUb1JlZnJlc2hJbml0aWF0ZWQyXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIGluZGV4PVxcXCIyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8bHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjbGFzcz1cXFwibGlzdHZpZXctaXRlbSBtLXQtMTYgbS1yLTE2IG0tbC0xNlxcXCIgY29sdW1ucz1cXFwiYXV0bywgKiwgYXV0b1xcXCIgcm93cz1cXFwiYXV0b1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJwYXRpZW50LWltYWdlXFxcIiBzcmM9XFxcInt7cGF0aWVudGltZzJ9fVxcXCIgd2lkdGg9XFxcIjYwXFxcIiBoZWlnaHQ9XFxcIjYwXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8U3RhY2tMYXlvdXQgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIG9yaWVudGF0aW9uPVxcXCJ2ZXJ0aWNhbFxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLTggcC1iLThcXFwiIGZvbnRTaXplPVxcXCIyMFxcXCIgdGV4dD1cXFwie3tuYW1lMn19XFxcIj48L0xhYmVsPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IHAtbC04IHAtci04IHAtdC04XFxcIiBmb250U2l6ZT1cXFwiMTZcXFwiIHRleHQ9XFxcInt7aG4yfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IHRleHQtcmlnaHQgbS1yLTE2XFxcIiBmb250U2l6ZT1cXFwiMTZcXFwiIHRleHQ9XFxcInt7cXRpbWUyfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L2x2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXc+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxuXFxuICAgICAgICAgICAgICAgICAgICA8VGFiQ29udGVudEl0ZW0+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBpZD1cXFwiYnRuRG9jdG9yM1xcXCIgaG9yaXpvbnRhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIiBoZWlnaHQ9XFxcIjEwMCVcXFwiIHZpc2liaWxpdHk9XFxcInZpc2libGVcXFwiIG9wYWNpdHk9XFxcIjFcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgcm93cz1cXFwiKlxcXCIgY29sdW1ucz1cXFwiYXV0b1xcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEJ1dHRvbiBjbGFzcz1cXFwidGV4dC1sYWJlbCBhZGQtZG9jdG9yLWJ0blxcXCIgdGV4dD1cXFwi4LmA4Lie4Li04LmI4Lih4LmB4Lie4LiX4Lii4LmMIDNcXFwiIHRhcD1cXFwiYWRkRG9jdG9yM1xcXCI+PC9CdXR0b24+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvRmxleGJveExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGlkPVxcXCJkb2N0b3IzXFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiNlZWVcXFwiIGhlaWdodD1cXFwiMTAwJVxcXCIgdmlzaWJpbGl0eT1cXFwiY29sbGFwc2VcXFwiIG9wYWNpdHk9XFxcIjBcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IGNsYXNzPVxcXCJjYXJkLWRvY3Rvci1wcm9maWxlIG0tMTYgcC04IHRleHQtY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBpZD1cXFwicHJvZmlsZWltZzNcXFwiIHdpZHRoPVxcXCIxMjBcXFwiIGhlaWdodD1cXFwiMTIwXFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcIm0tMTZcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiZG9jdG9ybmFtZTNcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiBpZD1cXFwiY2xpbmljbmFtZTNcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguKvguYnguK3guIfguJXguKPguKfguIhcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtbGFiZWwgdGV4dC1sZWZ0XFxcIiB0ZXh0PVxcXCLguIjguLPguJnguKfguJnguJzguLnguYnguJvguYjguKfguKJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxsdjpSYWRMaXN0VmlldyBpZD1cXFwibGlzdHZpZXczXFxcIiBjbGFzcz1cXFwibGlzdHZpZXctcGF0aWVudCBwLWItMTZcXFwiIGl0ZW1zPVxcXCJ7e2RhdGFJdGVtczN9fVxcXCIgaGVpZ2h0PVxcXCIxMDAlXFxcIiAgcHVsbFRvUmVmcmVzaD1cXFwidHJ1ZVxcXCIgcHVsbFRvUmVmcmVzaEluaXRpYXRlZD1cXFwib25QdWxsVG9SZWZyZXNoSW5pdGlhdGVkM1xcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEdyaWRMYXlvdXQgY2xhc3M9XFxcImxpc3R2aWV3LWl0ZW0gbS10LTE2IG0tci0xNiBtLWwtMTZcXFwiIGNvbHVtbnM9XFxcImF1dG8sICosIGF1dG9cXFwiIHJvd3M9XFxcImF1dG9cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBjbGFzcz1cXFwicGF0aWVudC1pbWFnZVxcXCIgc3JjPVxcXCJ7e3BhdGllbnRpbWczfX1cXFwiIHdpZHRoPVxcXCI2MFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBwLWwtOCBwLXItOCBwLWItOFxcXCIgZm9udFNpemU9XFxcIjIwXFxcIiB0ZXh0PVxcXCJ7e25hbWUzfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTggcC1yLTggcC10LThcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3tobjN9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8R3JpZExheW91dCBjb2w9XFxcIjJcXFwiIHJvdz1cXFwiMFxcXCIgdmVydGljYWxBbGlnbm1lbnQ9XFxcImNlbnRlclxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgdGV4dC1yaWdodCBtLXItMTZcXFwiIGZvbnRTaXplPVxcXCIxNlxcXCIgdGV4dD1cXFwie3txdGltZTN9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlPlxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9sdjpSYWRMaXN0Vmlldz5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8L1N0YWNrTGF5b3V0PlxcbiAgICAgICAgICAgICAgICAgICAgPC9UYWJDb250ZW50SXRlbT5cXG4gICAgICAgICAgICAgICAgPC9UYWJzPlxcbiAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxuXFxuICAgICAgICA8VGFiQ29udGVudEl0ZW0gYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIj5cXG4gICAgICAgICAgICA8U2Nyb2xsVmlldyBvcmllbnRhdGlvbj1cXFwidmVydGljYWxcXFwiPlxcbiAgICAgICAgICAgICAgICA8RmxleGJveExheW91dCBmbGV4RGlyZWN0aW9uPVxcXCJjb2x1bW5cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgPCEtLSA8Ukw6UmlwcGxlIGJhY2tncm91bmRDb2xvcj1cXFwiI2ZmZlxcXCIgaGVpZ2h0PVxcXCIxMDBcXFwiIGJvcmRlckJvdHRvbUNvbG9yPVxcXCJsaWdodGdyYXlcXFwiPiAtLT5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGJhY2tncm91bmRDb2xvcj1cXFwiI2ZmZlxcXCIgY29sdW1ucz1cXFwiYXV0byxhdXRvXFxcIiByb3dzPVxcXCIqXFxcIiBib3JkZXJCb3R0b21Db2xvcj1cXFwibGlnaHRncmF5XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8SW1hZ2UgaWQ9XFxcInNldHRpbmdwcm9maWxlaW1nXFxcIiBjbGFzcz1cXFwibS04XFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgc3JjPVxcXCJ7e3Byb2ZpbGV9fVxcXCIgd2lkdGg9XFxcIjY1XFxcIiBoZWlnaHQ9XFxcIjY1XFxcIiBzdHJldGNoPVxcXCJhc3BlY3RGaWxsXFxcIiBob3Jpem9udGFsQWxpZ25tZW50PVxcXCJsZWZ0XFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPFN0YWNrTGF5b3V0IHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiB2ZXJ0aWNhbEFsaWdubWVudD1cXFwiY2VudGVyXFxcIj5cXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LW5hbWUtbGFiZWxcXFwiIHRleHQ9XFxcIuC4iuC4t+C5iOC4rS3guJnguLLguKHguKrguIHguLjguKVcXFwiIGNvbFNwYW49XFxcIjJcXFwiIC8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1sYWJlbFxcXCIgdGV4dD1cXFwi4Lir4LiZ4LmI4Lin4Lii4LiH4Liy4LiZXFxcIiBjb2xTcGFuPVxcXCIyXFxcIiAvPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDwvU3RhY2tMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8IS0tIDwvUkw6UmlwcGxlPiAtLT5cXG5cXG4gICAgICAgICAgICAgICAgICAgIDwhLS0gPFJMOlJpcHBsZSBjbGFzcz1cXFwibS10LThcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiI2ZmZlxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIgYm9yZGVyQm90dG9tQ29sb3I9XFxcImxpZ2h0Z3JheVxcXCIgdGFwPVxcXCJsb2dvdXRcXFwiPiAtLT5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNsYXNzPVxcXCJtLXQtOFxcXCIgaGVpZ2h0PVxcXCI2MFxcXCIgY29sdW1ucz1cXFwiYXV0byxhdXRvLCpcXFwiIHJvd3M9XFxcIipcXFwiIGJhY2tncm91bmRDb2xvcj1cXFwiI2ZmZlxcXCI+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS1sLTI4XFxcIiBjb2w9XFxcIjBcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYyZjU7XFxcIiB3aWR0aD1cXFwiMjJcXFwiIGhlaWdodD1cXFwiMjJcXFwiIGNvbG9yPVxcXCIjNzU3NTc1XFxcIi8+XFxuICAgICAgICAgICAgICAgICAgICAgICAgPExhYmVsIGNsYXNzPVxcXCJ0ZXh0LWxhYmVsIG0tbC0xNlxcXCIgY29sPVxcXCIxXFxcIiB0ZXh0PVxcXCLguK3guK3guIHguIjguLLguIHguKPguLDguJrguJpcXFwiIHZlcnRpY2FsQWxpZ25tZW50PVxcXCJjZW50ZXJcXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgICAgICAgICAgICAgICAgICA8IS0tIDwvUkw6UmlwcGxlPiAtLT5cXG4gICAgICAgICAgICAgICAgPC9GbGV4Ym94TGF5b3V0PlxcbiAgICAgICAgICAgIDwvU2Nyb2xsVmlldz5cXG4gICAgICAgIDwvVGFiQ29udGVudEl0ZW0+XFxuICAgIDwvQm90dG9tTmF2aWdhdGlvbj5cXG48L1BhZ2U+XFxuXCI7IFxuaWYgKG1vZHVsZS5ob3QgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSShcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vdmlld3MvbWFpbi9tYWluLXBhZ2UueG1sXCIgfSk7XG4gICAgfSk7XG59ICIsIm1vZHVsZS5leHBvcnRzID0ge1widHlwZVwiOlwic3R5bGVzaGVldFwiLFwic3R5bGVzaGVldFwiOntcInJ1bGVzXCI6W3tcInR5cGVcIjpcInJ1bGVcIixcInNlbGVjdG9yc1wiOltcIi5saXN0dmlldy1jb250ZW50XCJdLFwiZGVjbGFyYXRpb25zXCI6W3tcInR5cGVcIjpcImNvbW1lbnRcIixcImNvbW1lbnRcIjpcIiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXFxcImFwcC92aWV3cy9tYWluLXBhZ2Uvc3JjL2NoZWNraW4uanBnXFxcIikgbm8tcmVwZWF0OyBcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIHdpZHRoOiA5NSU7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgaGVpZ2h0OiAxMDAlOyBcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJtYXJnaW4tdG9wXCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1sZWZ0XCIsXCJ2YWx1ZVwiOlwiOFwifSx7XCJ0eXBlXCI6XCJkZWNsYXJhdGlvblwiLFwicHJvcGVydHlcIjpcIm1hcmdpbi1yaWdodFwiLFwidmFsdWVcIjpcIjhcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIG1hcmdpbi1sZWZ0OiA4O1xcbiAgICBtYXJnaW4tcmlnaHQ6IDg7IFwifSx7XCJ0eXBlXCI6XCJjb21tZW50XCIsXCJjb21tZW50XCI6XCIgbWFyZ2luLWJvdHRvbTogODsgXCJ9LHtcInR5cGVcIjpcImRlY2xhcmF0aW9uXCIsXCJwcm9wZXJ0eVwiOlwiYm9yZGVyLXJhZGl1c1wiLFwidmFsdWVcIjpcIjhcIn0se1widHlwZVwiOlwiZGVjbGFyYXRpb25cIixcInByb3BlcnR5XCI6XCJiYWNrZ3JvdW5kLWNvbG9yXCIsXCJ2YWx1ZVwiOlwid2hpdGVcIn0se1widHlwZVwiOlwiY29tbWVudFwiLFwiY29tbWVudFwiOlwiIGNvbG9yOiB3aGl0ZTsgXCJ9XX1dLFwicGFyc2luZ0Vycm9yc1wiOltdfX07OyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmNzc1wiIH0pO1xuICAgIH0pO1xufSAiLCJjb25zdCBvYnNlcnZhYmxlTW9kdWxlID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlXCIpO1xuY29uc3QgTWFpblBhZ2VNb2RlbCA9IHJlcXVpcmUoXCIuLi9tYWluL21haW4tcGFnZS1tb2RlbFwiKTtcbmNvbnN0IGFwcFNldHRpbmdzID0gcmVxdWlyZShcInRucy1jb3JlLW1vZHVsZXMvYXBwbGljYXRpb24tc2V0dGluZ3NcIik7XG5cbmxldCBkb2N0b3IgPSBNYWluUGFnZU1vZGVsLkRvY3RvckRhdGEoKTtcbmxldCBjbG9zZUNhbGxiYWNrO1xubGV0IHRhYjtcblxuZXhwb3J0cy5vblNob3duTW9kYWxseSA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgY2xpbmljQm9keSA9IGFyZ3MuY29udGV4dDtcbiAgICB0YWIgPSBjbGluaWNCb2R5LnRhYjtcbiAgICBjbG9zZUNhbGxiYWNrID0gYXJncy5jbG9zZUNhbGxiYWNrO1xuICAgIGNvbnN0IHBhZ2UgPSBhcmdzLm9iamVjdDtcbiAgICAvLyBjb25zb2xlLmxvZyhjbGluaWNCb2R5KTtcbiAgICBsZXQgY2xpbmljZGF0YSA9IFtdO1xuICAgIGxldCBkb2N0b3JQZXJpZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5jX2RvY3Q7XG4gICAgbGV0IGRhdGVfZCA9IGNsaW5pY0JvZHkuY2xpbmljbGlzdFswXS5kYXRlX2Q7XG4gICAgY2xpbmljQm9keS5jbGluaWNsaXN0LmZvckVhY2goKGVsZW1lbnQpID0+IHtcbiAgICAgICAgY2xpbmljZGF0YS5wdXNoKHtcbiAgICAgICAgICAgIGNsaW5pY2NvZGU6IGVsZW1lbnQuY2xpbmljX2NvZGUsXG4gICAgICAgICAgICBjbGluaWNuYW1lOiBlbGVtZW50LmNsaW5pY19uYW1lLFxuICAgICAgICB9KTtcbiAgICB9KTtcbiAgICBwYWdlLmJpbmRpbmdDb250ZXh0ID0gb2JzZXJ2YWJsZU1vZHVsZS5mcm9tT2JqZWN0KHtcbiAgICAgICAgZG9jdG9ycGVyaWQ6IGRvY3RvclBlcmlkLFxuICAgICAgICBjdXJyZW50ZGF0ZTogZGF0ZV9kLFxuICAgICAgICBjbGluaWNkYXRhOiBjbGluaWNkYXRhLFxuICAgIH0pO1xuXG4gICAgLy8gY29uc3QgYmluZGluZ0NvbnRleHQgPSBwYWdlLnBhZ2UuYmluZGluZ0NvbnRleHQ7XG4gICAgLy8gbGV0IGlkID0gYmluZGluZ0NvbnRleHQuZ2V0KFwiaWRcIik7XG4gICAgLy8gbGV0IG5hbWUgPSBiaW5kaW5nQ29udGV4dC5nZXQoXCJuYW1lXCIpO1xuICAgIC8vIGNsb3NlQ2FsbGJhY2soaWQsIG5hbWUpO1xufTtcblxuZXhwb3J0cy5vbkl0ZW1TZWxlY3RlZCA9IChhcmdzKSA9PiB7XG4gICAgY29uc3QgcGFnZSA9IGFyZ3Mub2JqZWN0LnBhZ2U7XG4gICAgY29uc3QgbmFtZXBhZ2UgPSBhcmdzLm9iamVjdC50ZXh0O1xuICAgIGxldCBkb2N0b3JQZXJpZCA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJkb2N0b3JwZXJpZFwiKS50ZXh0O1xuICAgIGxldCBjdXJyZW50ZGF0ZSA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJjdXJyZW50ZGF0ZVwiKS50ZXh0O1xuICAgIGxldCBsaXN0VmlldyA9IHBhZ2UuZ2V0Vmlld0J5SWQoXCJsaXN0Vmlld1wiKTtcbiAgICB2YXIgc2VsZWN0ZWRJdGVtcyA9IGxpc3RWaWV3LmdldFNlbGVjdGVkSXRlbXMoKTtcbiAgICBsZXQgY2xpbmljQ29kZSA9IHNlbGVjdGVkSXRlbXNbMF0uY2xpbmljY29kZTtcbiAgICBsZXQgY2xpbmljTmFtZSA9IHNlbGVjdGVkSXRlbXNbMF0uY2xpbmljbmFtZTtcbiAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJjbGluaWNjb2RlXCIrdGFiLCBjbGluaWNDb2RlKTtcbiAgICBhcHBTZXR0aW5ncy5zZXRTdHJpbmcoXCJjbGluaWNuYW1lXCIrdGFiLCBjbGluaWNOYW1lKTtcbiAgICAvLyBjb25zb2xlLmxvZyhcIkRvY3RvciBQZXJpZDogXCIgKyBkb2N0b3JQZXJpZC50ZXh0KTtcbiAgICAvLyBjb25zb2xlLmxvZyhcIkRhdGU6IFwiICsgY3VycmVudGRhdGUudGV4dCk7XG4gICAgLy8gY29uc29sZS5sb2coc2VsZWN0ZWRJdGVtc1swXSk7XG4gICAgZG9jdG9yXG4gICAgICAgIC5nZXREb2N0b3JRdWV1ZShjdXJyZW50ZGF0ZSwgY2xpbmljQ29kZSwgZG9jdG9yUGVyaWQpXG4gICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhlcnIpO1xuICAgICAgICAgICAgcmVqZWN0KGVycik7XG4gICAgICAgIH0pXG4gICAgICAgIC50aGVuKChjbGluaWNxdWV1ZXJlc3BvbnNlKSA9PiB7XG4gICAgICAgICAgICBsZXQgY2xpbmljcXVldWVsaXN0ID0gY2xpbmljcXVldWVyZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgbGV0IGNsaW5pY0RhdGE7XG4gICAgICAgICAgICBpZiAoY2xpbmljcXVldWVsaXN0Lmxlbmd0aCAhPSAwKSB7XG4gICAgICAgICAgICAgICAgY2xpbmljRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogY2xpbmljTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBjbGluaWNxdWV1ZWxpc3QsXG4gICAgICAgICAgICAgICAgfTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY2xpbmljRGF0YSA9IHtcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljQ29kZTogY2xpbmljQ29kZSxcbiAgICAgICAgICAgICAgICAgICAgY2xpbmljTmFtZTogY2xpbmljTmFtZSxcbiAgICAgICAgICAgICAgICAgICAgcXVldWVMaXN0OiBbXSxcbiAgICAgICAgICAgICAgICB9O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY2xvc2VDYWxsYmFjayhjbGluaWNEYXRhKTtcbiAgICAgICAgfSk7XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UuanNcIikgKSB7XG4gICAgXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcbiAgICBtb2R1bGUuaG90LmRpc3Bvc2UoKCkgPT4ge1xuICAgICAgICBnbG9iYWwuaG1yUmVmcmVzaCh7IHR5cGU6IFwic2NyaXB0XCIsIHBhdGg6IFwiLi92aWV3cy9tb2RhbC9tb2RhbC1wYWdlLmpzXCIgfSk7XG4gICAgfSk7XG59ICIsImdsb2JhbC5yZWdpc3Rlck1vZHVsZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiLCBmdW5jdGlvbigpIHsgcmV0dXJuIHJlcXVpcmUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXdcIik7IH0pO1xuZ2xvYmFsLnJlZ2lzdGVyTW9kdWxlKFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3L1JhZExpc3RWaWV3XCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcuaXRlbVRlbXBsYXRlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUHVsbFRvUmVmcmVzaFN0eWxlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5nbG9iYWwucmVnaXN0ZXJNb2R1bGUoXCJuYXRpdmVzY3JpcHQtdWktbGlzdHZpZXcvUmFkTGlzdFZpZXcucHVsbFRvUmVmcmVzaFN0eWxlXCIsIGZ1bmN0aW9uKCkgeyByZXR1cm4gcmVxdWlyZShcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1wiKTsgfSk7XG5cbm1vZHVsZS5leHBvcnRzID0gXCI8UGFnZSBzaG93bk1vZGFsbHk9XFxcIm9uU2hvd25Nb2RhbGx5XFxcIiBcXG4gICAgeG1sbnM6bHY9XFxcIm5hdGl2ZXNjcmlwdC11aS1saXN0dmlld1xcXCI+XFxuICAgIDxTdGFja0xheW91dD5cXG4gICAgICAgIDwhLS0gPEdyaWRMYXlvdXQgcm93cz1cXFwiMTAwIDEwMFxcXCIgY29sdW1ucz1cXFwiKiAyKlxcXCI+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJ7e2lkfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiB0ZXh0PVxcXCJ7e25hbWV9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgIDwvR3JpZExheW91dD4gLS0+XFxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvLCBhdXRvXFxcIj5cXG4gICAgICAgICAgICA8IS0tIDxMYWJlbCBjbGFzcz1cXFwidGV4dC1mb250IHAtOFxcXCIgdGV4dD1cXFwi4Lij4Lir4Lix4Liq4LmB4Lie4LiX4Lii4LmMOiBcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOiAxNlxcXCI+PC9MYWJlbD4gLS0+XFxuICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS04XFxcIiBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMFxcXCIgc3JjPVxcXCJmb250Oi8vJiN4ZjBmMDtcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgY29sb3I9XFxcIiM0YTc3NGVcXFwiLz5cXG4gICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC04XFxcIiBpZD1cXFwiZG9jdG9ycGVyaWRcXFwiIHRleHQ9XFxcInt7ZG9jdG9ycGVyaWR9fVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuXFxuICAgICAgICAgICAgPCEtLSA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBwLThcXFwiIHRleHQ9XFxcIuC4p+C4seC4meC4l+C4teC5iDogXFxcIiBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMTZcXFwiPjwvTGFiZWw+IC0tPlxcbiAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwiZmFzIG0tOFxcXCIgY29sPVxcXCIwXFxcIiByb3c9XFxcIjFcXFwiIHNyYz1cXFwiZm9udDovLyYjeGYwNzM7XFxcIiB3aWR0aD1cXFwiMjRcXFwiIGhlaWdodD1cXFwiMjRcXFwiIGNvbG9yPVxcXCIjNGE3NzRlXFxcIi8+XFxuICAgICAgICAgICAgPExhYmVsIGNvbD1cXFwiMVxcXCIgcm93PVxcXCIxXFxcIiBjbGFzcz1cXFwidGV4dC1mb250IHAtOFxcXCIgaWQ9XFxcImN1cnJlbnRkYXRlXFxcIiB0ZXh0PVxcXCJ7e2N1cnJlbnRkYXRlfX1cXFwiIHN0eWxlPVxcXCJmb250LXNpemU6IDIwXFxcIj48L0xhYmVsPlxcbiAgICAgICAgPC9HcmlkTGF5b3V0PlxcbiAgICAgICAgPGx2OlJhZExpc3RWaWV3IGlkPVxcXCJsaXN0Vmlld1xcXCIgY2xhc3M9XFxcIm0tbC04IG0tci04XFxcIiBpdGVtcz1cXFwie3sgY2xpbmljZGF0YSB9fVxcXCIgYmFja2dyb3VuZENvbG9yPVxcXCIjZWVlXFxcIiBzZWxlY3Rpb25CZWhhdmlvcj1cXFwiUHJlc3NcXFwiIGl0ZW1TZWxlY3RlZD1cXFwib25JdGVtU2VsZWN0ZWRcXFwiIHRleHQ9XFxcInRyYW5zZmVyZGV0YWlsXFxcIiBzdHlsZT1cXFwiaGVpZ2h0OjUwJTsgd2lkdGg6MTAwJVxcXCI+XFxuICAgICAgICAgICAgPGx2OlJhZExpc3RWaWV3LnB1bGxUb1JlZnJlc2hTdHlsZT5cXG4gICAgICAgICAgICAgICAgPGx2OlB1bGxUb1JlZnJlc2hTdHlsZSBpbmRpY2F0b3JDb2xvcj1cXFwiIzRhNzc0ZVxcXCIgaW5kaWNhdG9yQmFja2dyb3VuZENvbG9yPVxcXCJ3aGl0ZVxcXCIvPlxcbiAgICAgICAgICAgIDwvbHY6UmFkTGlzdFZpZXcucHVsbFRvUmVmcmVzaFN0eWxlPlxcbiAgICAgICAgICAgIDxsdjpSYWRMaXN0Vmlldy5pdGVtVGVtcGxhdGU+XFxuICAgICAgICAgICAgICAgIDxTdGFja0xheW91dCBjbGFzcz1cXFwibGlzdHZpZXctY29udGVudFxcXCIgb3JpZW50YXRpb249XFxcInZlcnRpY2FsXFxcIiBwYWRkaW5nPVxcXCI4XFxcIj5cXG4gICAgICAgICAgICAgICAgICAgIDxHcmlkTGF5b3V0IGNvbHVtbnM9XFxcImF1dG8sIGF1dG8sIGF1dG9cXFwiIHJvd3M9XFxcImF1dG8sIGF1dG9cXFwiPlxcbiAgICAgICAgICAgICAgICAgICAgICAgIDxJbWFnZSBjbGFzcz1cXFwiZmFzXFxcIiByb3c9XFxcIjBcXFwiIGNvbD1cXFwiMFxcXCIgc3JjPVxcXCJmb250Oi8vJiN4ZjBmMTtcXFwiIHdpZHRoPVxcXCIyMFxcXCIgaGVpZ2h0PVxcXCIyMFxcXCIgY29sb3I9XFxcIiM0YTc3NGVcXFwiLz5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udCBtLWwtOFxcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjFcXFwiIHRleHQ9XFxcInt7Y2xpbmljY29kZX19XFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOjIwO1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgICAgICA8TGFiZWwgY2xhc3M9XFxcInRleHQtZm9udFxcXCIgcm93PVxcXCIwXFxcIiBjb2w9XFxcIjJcXFwiIHRleHQ9XFxcInt7Y2xpbmljbmFtZX19XFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOjE2O1xcXCI+PC9MYWJlbD5cXG4gICAgICAgICAgICAgICAgICAgIDwvR3JpZExheW91dD5cXG4gICAgICAgICAgICAgICAgPC9TdGFja0xheW91dD5cXG4gICAgICAgICAgICA8L2x2OlJhZExpc3RWaWV3Lml0ZW1UZW1wbGF0ZT5cXG4gICAgICAgIDwvbHY6UmFkTGlzdFZpZXc+XFxuICAgIDwvU3RhY2tMYXlvdXQ+XFxuPC9QYWdlPlwiOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL21vZGFsL21vZGFsLXBhZ2UueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vdmlld3MvbW9kYWwvbW9kYWwtcGFnZS54bWxcIiB9KTtcbiAgICB9KTtcbn0gIiwibW9kdWxlLmV4cG9ydHMgPSB7XCJ0eXBlXCI6XCJzdHlsZXNoZWV0XCIsXCJzdHlsZXNoZWV0XCI6e1wicnVsZXNcIjpbXSxcInBhcnNpbmdFcnJvcnNcIjpbXX19OzsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuY3NzXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcInN0eWxlXCIsIHBhdGg6IFwiLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwuY3NzXCIgfSk7XG4gICAgfSk7XG59ICIsImNvbnN0IG9ic2VydmFibGVNb2R1bGUgPSByZXF1aXJlKFwidG5zLWNvcmUtbW9kdWxlcy9kYXRhL29ic2VydmFibGVcIik7XG5jb25zdCBhcHBTZXR0aW5ncyA9IHJlcXVpcmUoXCJ0bnMtY29yZS1tb2R1bGVzL2FwcGxpY2F0aW9uLXNldHRpbmdzXCIpO1xuXG5sZXQgY2xvc2VDYWxsYmFjaztcbmxldCB0YWI7XG5cbmV4cG9ydHMub25TaG93bk1vZGFsbHkgPSAoYXJncykgPT4ge1xuICAgIGxldCBwYWdlID0gYXJncy5vYmplY3QucGFnZTtcbiAgICBjb25zdCBwYXRpZW50Qm9keSA9IGFyZ3MuY29udGV4dDtcbiAgICBjb25zb2xlLmxvZyhwYXRpZW50Qm9keSk7XG4gICAgLy8gdGFiID0gY2xpbmljQm9keS50YWI7XG4gICAgLy8gY2xvc2VDYWxsYmFjayA9IGFyZ3MuY2xvc2VDYWxsYmFjaztcbiAgICAvLyBjb25zdCBwYWdlID0gYXJncy5vYmplY3Q7XG4gICAgLy8gY29uc29sZS5sb2coY2xpbmljQm9keSk7XG4gICAgLy8gbGV0IGNsaW5pY2RhdGEgPSBbXTtcbiAgICAvLyBsZXQgZG9jdG9yUGVyaWQgPSBjbGluaWNCb2R5LmNsaW5pY2xpc3RbMF0uY19kb2N0O1xuICAgIC8vIGxldCBkYXRlX2QgPSBjbGluaWNCb2R5LmNsaW5pY2xpc3RbMF0uZGF0ZV9kO1xuXG4gICAgcGFnZS5iaW5kaW5nQ29udGV4dCA9IG9ic2VydmFibGVNb2R1bGUuZnJvbU9iamVjdCh7XG4gICAgICAgIHBhdGllbnRuYW1lOiBwYXRpZW50Qm9keS5uYW1lLFxuICAgICAgICBwYXRpZW50aG46IHBhdGllbnRCb2R5LmhuLCBcbiAgICAgICAgcXVldWV0aW1lOiBwYXRpZW50Qm9keS50aW1lXG4gICAgfSk7XG59O1xuOyBcbmlmIChtb2R1bGUuaG90ICYmIGdsb2JhbC5faXNNb2R1bGVMb2FkZWRGb3JVSSAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkoXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qc1wiKSApIHtcbiAgICBcbiAgICBtb2R1bGUuaG90LmFjY2VwdCgpO1xuICAgIG1vZHVsZS5ob3QuZGlzcG9zZSgoKSA9PiB7XG4gICAgICAgIGdsb2JhbC5obXJSZWZyZXNoKHsgdHlwZTogXCJzY3JpcHRcIiwgcGF0aDogXCIuL3ZpZXdzL3BhdGllbnQvcGF0aWVudC1tb2RhbC5qc1wiIH0pO1xuICAgIH0pO1xufSAiLCJcbm1vZHVsZS5leHBvcnRzID0gXCI8UGFnZSBzaG93bk1vZGFsbHk9XFxcIm9uU2hvd25Nb2RhbGx5XFxcIlxcbiAgICB4bWxuczpsdj1cXFwibmF0aXZlc2NyaXB0LXVpLWxpc3R2aWV3XFxcIj5cXG4gICAgPFN0YWNrTGF5b3V0IHdpZHRoPVxcXCIxMDAlXFxcIj5cXG4gICAgICAgIDwhLS0gPEdyaWRMYXlvdXQgcm93cz1cXFwiMTAwIDEwMFxcXCIgY29sdW1ucz1cXFwiKiAyKlxcXCI+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIwXFxcIiB0ZXh0PVxcXCJ7e2lkfX1cXFwiPjwvTGFiZWw+XFxuICAgICAgICAgICAgPExhYmVsIHJvdz1cXFwiMFxcXCIgY29sPVxcXCIxXFxcIiB0ZXh0PVxcXCJ7e25hbWV9fVxcXCI+PC9MYWJlbD5cXG4gICAgICAgIDwvR3JpZExheW91dD4gLS0+XFxuICAgICAgICA8R3JpZExheW91dCBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvLCBhdXRvLCBhdXRvXFxcIj5cXG4gICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImZhcyBtLXQtOCBtLWwtMTYgbS1yLThcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIwXFxcIiBzcmM9XFxcImZvbnQ6Ly8mI3hmMmMyO1xcXCIgd2lkdGg9XFxcIjI0XFxcIiBoZWlnaHQ9XFxcIjI0XFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCIvPlxcbiAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMFxcXCIgY2xhc3M9XFxcInRleHQtZm9udCBwLThcXFwiIGlkPVxcXCJkb2N0b3JwZXJpZFxcXCIgdGV4dD1cXFwie3twYXRpZW50bmFtZX19XFxcIiBzdHlsZT1cXFwiZm9udC1zaXplOiAyMFxcXCI+PC9MYWJlbD5cXG5cXG4gICAgICAgICAgICA8SW1hZ2UgY2xhc3M9XFxcImZhcyBtLXQtOCBtLWwtMTYgbS1yLThcXFwiIGNvbD1cXFwiMFxcXCIgcm93PVxcXCIxXFxcIiBzcmM9XFxcImZvbnQ6Ly8mI3hmMGY4O1xcXCIgd2lkdGg9XFxcIjI0XFxcIiBoZWlnaHQ9XFxcIjI0XFxcIiBjb2xvcj1cXFwiIzRhNzc0ZVxcXCIvPlxcbiAgICAgICAgICAgIDxMYWJlbCBjb2w9XFxcIjFcXFwiIHJvdz1cXFwiMVxcXCIgY2xhc3M9XFxcInRleHQtZm9udCBwLThcXFwiIGlkPVxcXCJjdXJyZW50ZGF0ZVxcXCIgdGV4dD1cXFwie3twYXRpZW50aG59fVxcXCIgc3R5bGU9XFxcImZvbnQtc2l6ZTogMjBcXFwiPjwvTGFiZWw+XFxuXFxuICAgICAgICAgICAgPEltYWdlIGNsYXNzPVxcXCJmYXMgbS10LTggbS1sLTE2IG0tci04IG0tYi04XFxcIiBjb2w9XFxcIjBcXFwiIHJvdz1cXFwiMlxcXCIgc3JjPVxcXCJmb250Oi8vJiN4ZjAxNztcXFwiIHdpZHRoPVxcXCIyNFxcXCIgaGVpZ2h0PVxcXCIyNFxcXCIgY29sb3I9XFxcIiM0YTc3NGVcXFwiLz5cXG4gICAgICAgICAgICA8TGFiZWwgY29sPVxcXCIxXFxcIiByb3c9XFxcIjJcXFwiIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC04XFxcIiBpZD1cXFwiY3VycmVudGRhdGVcXFwiIHRleHQ9XFxcInt7cXVldWV0aW1lfX1cXFwiIHN0eWxlPVxcXCJmb250LXNpemU6IDIwXFxcIj48L0xhYmVsPlxcbiAgICAgICAgPC9HcmlkTGF5b3V0PlxcblxcbiAgICAgICAgPEdyaWRMYXlvdXQgY2xhc3M9XFxcIm0tYi04XFxcIiBjb2x1bW5zPVxcXCJhdXRvLCBhdXRvLCBhdXRvXFxcIiByb3dzPVxcXCJhdXRvXFxcIj5cXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTE2IHAtci0xNlxcXCIgY29sPVxcXCIwXFxcIiByb3c9XFxcIjBcXFwiIHRleHQ9XFxcIuC4o+C4reC4nuC4muC5geC4nuC4l+C4ouC5jFxcXCIgYm9yZGVyUmFkaXVzPVxcXCI4XFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTE2IHAtci0xNlxcXCIgY29sPVxcXCIxXFxcIiByb3c9XFxcIjBcXFwiIHRleHQ9XFxcIuC4o+C4reC4nuC4muC4nuC4ouC4suC4muC4suC4pVxcXCIgYm9yZGVyUmFkaXVzPVxcXCI4XFxcIj48L2J1dHRvbj5cXG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzPVxcXCJ0ZXh0LWZvbnQgcC1sLTE2IHAtci0xNlxcXCIgY29sPVxcXCIyXFxcIiByb3c9XFxcIjBcXFwiIHRleHQ9XFxcIuC4leC4o+C4p+C4iOC5gOC4quC4o+C5h+C4iOC5geC4peC5ieC4p1xcXCIgYm9yZGVyUmFkaXVzPVxcXCI4XFxcIiBiYWNrZ3JvdW5kQ29sb3I9XFxcIiM0YTc3NGVcXFwiIGNvbG9yPVxcXCJ3aGl0ZVxcXCI+PC9idXR0b24+XFxuICAgICAgICA8L0dyaWRMYXlvdXQ+XFxuICAgIDwvU3RhY2tMYXlvdXQ+IFxcbjwvUGFnZT5cIjsgXG5pZiAobW9kdWxlLmhvdCAmJiBnbG9iYWwuX2lzTW9kdWxlTG9hZGVkRm9yVUkgJiYgZ2xvYmFsLl9pc01vZHVsZUxvYWRlZEZvclVJKFwiLi92aWV3cy9wYXRpZW50L3BhdGllbnQtbW9kYWwueG1sXCIpICkge1xuICAgIFxuICAgIG1vZHVsZS5ob3QuYWNjZXB0KCk7XG4gICAgbW9kdWxlLmhvdC5kaXNwb3NlKCgpID0+IHtcbiAgICAgICAgZ2xvYmFsLmhtclJlZnJlc2goeyB0eXBlOiBcIm1hcmt1cFwiLCBwYXRoOiBcIi4vdmlld3MvcGF0aWVudC9wYXRpZW50LW1vZGFsLnhtbFwiIH0pO1xuICAgIH0pO1xufSAiXSwic291cmNlUm9vdCI6IiJ9